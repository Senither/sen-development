<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('groups_id')->unsinged();
            $table->integer('themes_id')->default(1);
            $table->string('email')->uniqid();
            $table->string('password', 64)->nullable();
            $table->string('name');
            $table->string('activation_token', 128)->nullable();
            $table->string('api_token', 128)->nullable();
            $table->smallInteger('beta')->default(0);
            $table->string('minecraft', 32)->nullable();
            $table->string('website')->nullable();
            $table->json('settings');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::drop('users');
    }
}
