<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePluginsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('plugins', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('organization_id')->unsigned();
            $table->integer('user_id')->default(0);
            $table->integer('availability')->default(0);
            $table->string('version');
            $table->string('name');
            $table->string('slug');
            $table->integer('progress')->default(0);

            // 0 - Pending
            // 1 - Being worked on
            // 2 - Fixing bugs/Updating plugin
            // 9 - Finished
            $table->integer('status')->default(0);
            $table->text('description');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::drop('plugins');
    }
}
