<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoiceTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('developer_id')->unsinged();
            $table->integer('user_id')->unsinged();
            $table->integer('status');
            $table->string('receipt')->uniqid();
            $table->string('token')->nullable();
            $table->json('items');
            $table->double('total');
            $table->timestamp('paid_at');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::drop('invoices');
    }
}
