<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('logger', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_id')->default(0);
            $table->string('action');
            $table->string('message');
            $table->text('new')->nullable();
            $table->text('old')->nullable();
            $table->string('address');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::drop('logger');
    }
}
