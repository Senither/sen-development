<?php

use Illuminate\Database\Seeder;
use App\Models\SuggestionCategory;

class SuggestionCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        DB::table('suggestion_categories')->truncate();

        SuggestionCategory::create(['name' => 'Website']);
        SuggestionCategory::create(['name' => 'Server']);
    }
}
