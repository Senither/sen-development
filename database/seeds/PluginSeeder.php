<?php

use App\Models\Plugin;
use Illuminate\Database\Seeder;

class PluginSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        DB::table('plugins')->truncate();

        // $plugins = [
        //     'Earth Games' => [
        //         'name'  => 'Earth Games',
        //         'version' => '1.0 R1.0',
        //         'description' => 'This is a sample plugin for the website.',
        //         'organization_id' => 1
        //     ],
        //     'Lucky Block Brawl' => [
        //         'name'  => 'Empty Plugin',
        //         'version' => '1.0 R1.0',
        //         'description' => 'This is a sample plugin for the website.',
        //         'organization_id' => 2
        //     ],
        //     'Backup Management' => [
        //         'name'  => 'Backup Management',
        //         'version' => '1.1 R3.0',
        //         'description' => 'This is a sample plugin for the website.',
        //         'organization_id' => 3
        //     ],
        //     'Example Plugin' => [
        //         'name'  => 'Empty Plugin',
        //         'version' => '0.1 Beta',
        //         'description' => 'This is just a sample plugin that is empty to help see what happens if the plugin doesn\'t have any files attached to it.',
        //         'organization_id' => 1
        //     ]
        // ];

        // $index = 1;

        // foreach ($plugins as $key => $plugin) {
        //     $plugin['slug'] = str_slug($plugin['name']);

        //     $instance = Plugin::create($plugin);
        //     $instance->organization_id = $plugin['organization_id'];
        //     $instance->progress = rand(1, 99);
        //     $instance->status = rand(0, 2);

        //     $instance->save();
        // }
    }
}
