<?php

use App\Models\FaqCategory;
use Illuminate\Database\Seeder;

class FaqCategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        DB::table('faq_categories')->truncate();

        FaqCategory::create(['name' => 'Website - Plugins']);
        FaqCategory::create(['name' => 'Website - Organizations']);
        FaqCategory::create(['name' => 'Server']);
    }
}
