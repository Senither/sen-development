<?php

use App\Models\Organization;
use Illuminate\Database\Seeder;

class OrganizationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        DB::table('organization')->truncate();
        DB::table('organization_users')->truncate();

        // $list = ['Example', 'Sample', 'Test'];

        // foreach($list AS $name) {
        //     $organization = Organization::create([
        //         'user_id' => 1,

        //         'name' => $name,
        //         'slug' => str_slug($name)
        //     ]);

        //     foreach(range(1, 16) AS $number) {
        //         if(rand(0, 3) == 0) continue;

        //         DB::table('organization_users')->insert([
        //             'organization_id' => $organization->id,
        //             'users_id'        => $number
        //         ]);
        //     }
        // }
    }
}
