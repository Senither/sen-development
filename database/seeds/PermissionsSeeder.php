<?php

use App\Models\Permission;
use Illuminate\Database\Seeder;

class PermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        DB::table('permissions')->truncate();

        $permissions = [
            // Development Permissions
            [
                'name' => 'Allows the user to see the developer tools menu',
                'node' => 'development.see',
            ], [
                'name' => 'Allows the user to create new organizations',
                'node' => 'development.organization.create',
            ], [
                'name' => 'Allows the user to create new plugins',
                'node' => 'development.plugin.create',
            ], [
                'name' => 'Allows the user to add/remove members to/from a plugin if they\'re not the owner of the plugin',
                'node' => 'development.plugin.members.modify',
            ], [
                'name' => 'Allows the use to see invoices',
                'node' => 'development.invoice.see',
            ],

            // Management Permissions
            [
                'name' => 'Can see the management system',
                'node' => 'management.see',
            ], [
                'name' => 'Allows the user see the users list',
                'node' => 'management.user.see',
            ], [
                'name' => 'Allows the user to send invites to the system',
                'node' => 'management.user.invite',
            ], [
                'name' => 'Allows the user to edit other users',
                'node' => 'management.user.edit',
            ], [
                'name' => 'Allows the user see the groups list',
                'node' => 'management.group.see',
            ], [
                'name' => 'Allows the user edit groups permission, name etc..',
                'node' => 'management.group.edit',
            ], [
                'name' => 'Allows the user to see plugins',
                'node' => 'management.plugin.see',
            ], [
                'name' => 'Allows the user to edit plugins',
                'node' => 'management.plugin.edit',
            ], [
                'name' => 'Allows the user to see all orgaizations',
                'node' => 'management.organization.see',
            ], [
                'name' => 'Allow the user to edit any orgaization',
                'node' => 'management.organization.edit',
            ], [
                'name' => 'Allows the user to see, create and edit news articles',
                'node' => 'management.news.see',
            ], [
                'name' => 'Allows the user to see the logs',
                'node' => 'management.logs.see',
            ], [
                'name' => 'Allows the user to mark payments by other people as confirmed',
                'node' => 'management.invoice.others',
            ], [
                'name' => 'Allows the user to see invoices',
                'node' => 'management.invoice.see',
            ], [
                'name' => 'Allows the user to see FAQ questions',
                'node' => 'management.faq.see',
            ], [
                'name' => 'Allows the user to edit FAQ questions',
                'node' => 'management.faq.edit',
            ], [
                'name' => 'Allows the user to delete FAQ questions',
                'node' => 'management.faq.delete',
            ], [
                'name' => 'Allows the user to see contact messages',
                'node' => 'management.message.see',
            ], [
                'name' => 'Allows the user to see files',
                'node' => 'management.file.see',
            ], [
                'name' => 'Allows the user to edit files',
                'node' => 'management.file.edit',
            ], [
                'name' => 'Allows the user to see the suggestions',
                'node' => 'management.suggestion.see',
            ], [
                'name' => 'Allows the user to delete the suggestions',
                'node' => 'management.suggestion.delete',
            ],
        ];

        foreach ($permissions as $perm) {
            Permission::create($perm);
        }
    }
}
