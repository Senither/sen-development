<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Cache;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        Model::unguard();

        $this->call(UsersSeeder::class);
        $this->call(GroupsSeeder::class);
        $this->call(ThemesSeeder::class);
        $this->call(PermissionsSeeder::class);
        $this->call(GroupsPermissionsSeeder::class);
        $this->call(OrganizationSeeder::class);
        $this->call(PluginSeeder::class);
        $this->call(FilesSeeder::class);
        $this->call(FaqCategoriesSeeder::class);
        $this->call(FaqSeeder::class);
        $this->call(ContactCategorySeeder::class);
        $this->call(SuggestionCategorySeeder::class);
        $this->call(LoggerSeeder::class);
        $this->call(NewsSeeder::class);

        Cache::flush();

        Model::reguard();
    }
}
