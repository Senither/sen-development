<?php

use App\Models\Group;
use Illuminate\Database\Seeder;

class GroupsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        DB::table('groups')->truncate();

        $groups = [
            'admins' => [
                'name' => 'Administrator',
                'level' => 100000,
                'default' => 0,
            ],
            'dev' => [
                'name' => 'Developer',
                'level' => 60000,
                'default' => 0,
            ],
            'jrdev' => [
                'name' => 'Jr. Developer',
                'level' => 45000,
                'default' => 0,
            ],
            'vip-client' => [
                'name' => 'VIP Client',
                'level' => 10,
                'default' => 0,
            ],
            'client' => [
                'name' => 'Client',
                'level' => 1,
                'default' => 1,
            ],
        ];

        foreach ($groups as $key => $group) {
            Group::create($group);
        }
    }
}
