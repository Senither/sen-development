<?php

use App\Models\ContactCategory;
use Illuminate\Database\Seeder;

class ContactCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        DB::table('contact_categories')->truncate();

        ContactCategory::create(['name' => 'Website Issue']);
        ContactCategory::create(['name' => 'Server Issue']);
        ContactCategory::create(['name' => 'Plugin']);
        ContactCategory::create(['name' => 'Organization']);
        ContactCategory::create(['name' => 'Account']);
        ContactCategory::create(['name' => 'Other']);
    }
}
