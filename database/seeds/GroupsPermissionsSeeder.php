<?php

use Illuminate\Database\Seeder;

class GroupsPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        DB::table('groups_permissions')->truncate();
    }
}
