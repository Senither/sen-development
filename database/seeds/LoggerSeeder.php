<?php

use Illuminate\Database\Seeder;

class LoggerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        DB::table('logger')->truncate();
    }
}
