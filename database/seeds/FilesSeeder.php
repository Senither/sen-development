<?php

use App\Models\File;
use Illuminate\Database\Seeder;

class FilesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        DB::table('files')->truncate();

        // $faker    = Faker\Factory::create();
        // $versions = ['1.1 R1.0', '1.2 R1.0', '1.3 R1.0', '1.4 R1.0', '2.0 R0.1'];

        // foreach (range(1, 3) as $plugin) {
        //     foreach (range(0, rand(2, 4)) as $number) {
        //         $version = $versions[(count($versions) - 1) - $number];

        //         File::create([
        //             'plugin_id' => $plugin,
        //             'user_id' => rand(1, 10),
        //             'version' => $version,
        //             'slug'    => str_slug($version),
        //             'changes' => $faker->paragraph(3),
        //             'file'    => 'placeholder'
        //         ]);
        //     }
        // }

        // foreach (range(1, 20) AS $number) {
        //     File::create([
        //         'plugin_id' => 1,
        //         'user_id' => rand(1, 10),
        //         'version' => 'Filler #' . $number,
        //         'slug'    => str_slug('Filler #' . $number),
        //         'changes' => $faker->paragraph(3),
        //         'file'    => 'placeholder'
        //     ]);
        // }
    }
}
