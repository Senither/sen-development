<?php

use App\Models\Faq;
use Illuminate\Database\Seeder;

class FaqSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        DB::table('faq')->truncate();

        // $faker = Faker\Factory::create();

        // foreach(range(1, 30) AS $number) {
        //     Faq::create([
        //         'category_id' => rand(1, 3),
        //         'question'    => $faker->sentence(rand(4, 11)),
        //         'awnser'      => implode(' ', $faker->sentences(rand(4, 32))),
        //     ]);
        // }
    }
}
