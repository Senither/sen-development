<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        DB::table('users')->truncate();

        $settings = [
            'opt_in_plugin' => false,
            'opt_in_change' => false,
        ];

        // Create the admin email
        $admin = new User();
        $admin->email = 'alexis@sen-dev.com';
        $admin->name = 'Alexis Tan';
        $admin->password = 'password';
        $admin->groups_id = 1;
        $admin->settings = $settings;
        $admin->save();

        $faker = Faker\Factory::create();

        // // Generate 5 Developer accounts
        // foreach (range(1, 5) as $numer) {
        //     $user = User::create([
        //         'name' => $faker->name,
        //         'email' => $faker->email,
        //         'password' => 'password',
        //         'settings' => $settings,
        //     ]);
        //     $user->groups_id = 2;
        //     $user->save();
        // }

        // // Generate 10 VIP Client accounts
        // foreach (range(1, 5) as $numer) {
        //     $user = User::create([
        //         'name' => $faker->name,
        //         'email' => $faker->email,
        //         'password' => 'password',
        //         'settings' => $settings,
        //     ]);
        //     $user->groups_id = 3;
        //     $user->save();
        // }

        // // Generate 15 Client accounts
        // foreach (range(1, 5) as $numer) {
        //     $user = User::create([
        //         'name' => $faker->name,
        //         'email' => $faker->email,
        //         'password' => 'password',
        //         'settings' => $settings,
        //     ]);
        //     $user->groups_id = 4;
        //     $user->save();
        // }
    }
}
