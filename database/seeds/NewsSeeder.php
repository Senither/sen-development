<?php

use App\Models\News;
use Illuminate\Database\Seeder;

class NewsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        DB::table('news')->truncate();

            // $faker    = Faker\Factory::create();

            // foreach(range(1, 25) AS $number) {
            //     News::create([
            //         'user_id'     => 1,
            //         'title'       => $faker->sentence(rand(4, 9)),
            //         'description' => $faker->paragraph(3),
            //         'body'        => implode("\n", $faker->paragraphs(rand(6, 15)))
            //     ]);
            // }
    }
}
