<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ThemesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        DB::table('themes')->truncate();

        DB::table('themes')->insert([
            ['name' => 'Blue', 'theme' => 'blue'],
            ['name' => 'Dark', 'theme' => 'dark'],
            ['name' => 'Green', 'theme' => 'green'],
            ['name' => 'Purple', 'theme' => 'purple'],
            ['name' => 'Red', 'theme' => 'red'],
            ['name' => 'White', 'theme' => 'white'],
        ]);
    }
}
