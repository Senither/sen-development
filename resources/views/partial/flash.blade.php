<script src="{{ asset('assets/plugins/toastr/toastr.min.js') }}"></script>

@if(flash()->has())
<script>
    @foreach(flash()->get() AS $message)
        setTimeout(function() {
            toastr.options = {
                closeButton: {{ $message['button'] ? 'true' : 'false' }},
                progressBar: {{ $message['timer'] == 0 ? 'false' : 'true' }},

                @if($message['animation'] == 'slide')
                    showMethod: 'slideDown',
                    hideMethod: 'slideUp',
                @elseif($message['animation'] == 'fade')
                    showMethod: 'fadeIn',
                    hideMethod: 'fadeOut',
                @endif
                timeOut: {{ $message['timer'] }}
            };
            toastr.{{ $message['level'] }}('{{ $message['message'] }}', '{{ $message['title'] }}');
        }, 800);
    @endforeach
</script>
@endif
