@if($errors->any())
    <ul class="alert alert-warning">
        @foreach($errors->all() AS $error)
            <li class="error-message">{{ $error }}</li>
        @endforeach
    </ul>
@endif
