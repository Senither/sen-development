<?php
    $requestPathInfo = explode('/', trim(Request::getPathInfo(), '/'));
    $requestPathInfo[1] = isset($requestPathInfo[1]) ? $requestPathInfo[1] : null;
    $requestPathInfo[2] = isset($requestPathInfo[2]) ? $requestPathInfo[2] : null;
?>
<div class="navbar">
    <div class="navbar-inner">
        <div class="sidebar-pusher">
            <a href="javascript:void(0);" class="waves-effect waves-button waves-classic push-sidebar">
                <i class="fa fa-bars"></i>
            </a>
        </div>
        <div class="logo-box">
            <a href="{{ action('DashboardController@index') }}" class="logo-text"><span>Sen</span>Dev</a>
        </div><!-- Logo Box -->
        <div class="topmenu-outer">
            <div class="top-menu">
                <ul class="nav navbar-nav navbar-left">
                    <li>
                        <a href="javascript:void(0);" class="waves-effect waves-button waves-classic toggle-fullscreen"><i class="fa fa-expand"></i></a>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    @include('partial.menu.messages')

                    @include('partial.menu.tasks')
                    <li>
                        <a href="{{ action('UserController@settings') }}">
                            <span class="user-name">You are logged in as <strong>{{ $globalUser->name }}</strong></span>
                            <img class="img-circle avatar" src="{{ avatar_image($globalUser) }}" width="40" height="40" alt="">
                        </a>
                    </li>
                    <li>
                        <a href="{{ action('UserController@logout') }}" class="log-out waves-effect waves-button waves-classic">
                            <span><i class="fa fa-sign-out m-r-xs"></i>Log out</span>
                        </a>
                    </li>
                </ul><!-- Nav -->
            </div><!-- Top Menu -->
        </div>
    </div>
</div><!-- Navbar -->
<div class="page-sidebar sidebar">
    <div class="page-sidebar-inner slimscroll">
        <div class="sidebar-header">
            <div class="sidebar-profile">
                <a href="{{ action('UserController@settings') }}">
                    <div class="sidebar-profile-image">
                        <img src="{!! avatar_image($globalUser, 128) !!}" class="img-circle img-responsive" alt="">
                    </div>
                    <div class="sidebar-profile-details">
                        <span>{{ $globalUser->name }}<br><small>{{ $globalUser->group->name }}</small></span>
                    </div>
                </a>
            </div>
        </div>
        {{-- Sidebar Menu --}}
        <ul class="menu accordion-menu">

            <li class="{{ isCurrentPage($requestPathInfo, 'dashboard') }}"><a href="{{ action('DashboardController@index') }}" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-dashboard"></span><p>Dashboard</p></a></li>

            <li class="droplink {{ $requestPathInfo[1] == 'plugin' ? 'active open' : null }}"><a href="#" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-fire"></span><p>Plugins</p><span class="arrow"></span></a>
                <ul class="sub-menu" style="display: block;">
                    @if($globalUser->organizations->isEmpty())
                        <li><a>You don't have any plugins assinged to you at this moment.</a></li>
                    @else
                        @foreach($globalUser->organizations AS $organization)
                            @foreach($organization->plugins AS $plugin)
                                <li class="{{ isCurrentPage($requestPathInfo, 'plugin', $organization->slug, $plugin->slug) }}">
                                    <a href="{{ action('PluginController@show', ['organization' => $organization->slug, 'plugin' => $plugin->slug]) }}">{{ $plugin->name }}</a>
                                </li>
                            @endforeach
                        @endforeach
                    @endif
                </ul>
            </li>

            <li class="droplink {{ isCurrentPage($requestPathInfo, 'organization') }}"><a href="#" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-briefcase"></span><p>Organizations</p><span class="arrow"></span></a>
                <ul class="sub-menu" style="display: block;">
                    @if($globalUser->organizations->isEmpty())
                        <li><a>You don't belong to any organization at this moment.</a></li>
                    @else
                        @foreach($globalUser->organizations AS $organization)
                            <li class="{{ isCurrentPage($requestPathInfo, 'organization', $organization->slug) }}"><a href="{{ action('OrganizationController@show', ['id' => $organization->slug]) }}">{{ $organization->name }}</a></li>
                        @endforeach
                    @endif
                </ul>
            </li>

            <li class="{{ isCurrentPage($requestPathInfo, 'user', 'settings') }}"><a href="{{ action('UserController@settings') }}" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-wrench"></span><p>My Settings</p></a></li>

            <li class="{{ isCurrentPage($requestPathInfo, 'invoice') }}"><a href="{{ action('InvoiceController@index') }}" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-credit-card"></span><p>Invoices</p></a></li>

            <li class="droplink {{ isCurrentPage($requestPathInfo, 'support') }} {{ isCurrentPage($requestPathInfo, 'news') }}"><a href="#" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-comment"></span><p>Support</p><span class="arrow"></span></a>
                <ul class="sub-menu" style="display: block;">
                    <li class="{{ isCurrentPage($requestPathInfo, 'news') }}"><a href="{{ action('NewsController@index') }}">News</a></li>
                    <li class="{{ isCurrentPage($requestPathInfo, 'support', 'faq') }}"><a href="{{ action('SupportController@faq') }}">FAQ</a></li>
                    <li class="{{ isCurrentPage($requestPathInfo, 'support', 'suggestion') }}"><a href="{{ action('SupportController@suggestion') }}">Suggestions</a></li>
                    <li class="{{ isCurrentPage($requestPathInfo, 'support', 'contact') }}"><a href="{{ action('SupportController@contact') }}">Contact Support</a></li>
                </ul>
            </li>

            @if($globalUser->hasPermission('development.see'))
                <li class="droplink {{isCurrentPage($requestPathInfo, 'developer-tools') }}"><a href="#" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-briefcase"></span><p>Development Tools</p><span class="arrow"></span></a>
                    <ul class="sub-menu" style="display: block;">
                        <li class="{{ isCurrentPage($requestPathInfo, 'developer-tools', 'plugins') }}"><a href="{{ action('DeveloperController@plugins') }}">Plugins</a></li>
                        <li class="{{ isCurrentPage($requestPathInfo, 'developer-tools', 'organizations') }}"><a href="{{ action('DeveloperController@organizations') }}">Organizations</a></li>
                        <li class="{{ isCurrentPage($requestPathInfo, 'developer-tools', 'invoices') }}"><a href="{{ action('DeveloperController@invoice') }}">Invoices</a></li>
                    </ul>
                </li>
            @endif

            @if($globalUser->hasPermission('management.see'))
                <li class="droplink {{ isCurrentPage($requestPathInfo, 'management') }}"><a href="#" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-compressed"></span><p>Management</p><span class="arrow"></span></a>
                    <ul class="sub-menu" style="display: block;">
                        @if($globalUser->hasPermission('management.news.see'))
                            <li class="{{ isCurrentPage($requestPathInfo, 'management', 'news') }}"><a href="{{ action('ManagementController@news') }}">News</a></li>
                        @endif

                        @if($globalUser->hasPermission('management.user.see'))
                            <li class="{{ isCurrentPage($requestPathInfo, 'management', 'users') }}"><a href="{{ action('ManagementController@users') }}">Users</a></li>
                        @endif

                        @if($globalUser->hasPermission('management.group.see'))
                            <li class="{{ isCurrentPage($requestPathInfo, 'management', 'groups') }}"><a href="{{ action('ManagementController@groups') }}">Groups</a></li>
                        @endif

                        @if($globalUser->hasPermission('management.plugin.see'))
                            <li class="{{ isCurrentPage($requestPathInfo, 'management', 'plugins') }}"><a href="{{ action('ManagementController@plugins') }}">Plugins</a></li>
                        @endif

                        @if($globalUser->hasPermission('management.file.see'))
                            <li class="{{ isCurrentPage($requestPathInfo, 'management', 'files') }}"><a href="{{ action('ManagementController@files') }}">Files</a></li>
                        @endif

                        @if($globalUser->hasPermission('management.organization.see'))
                            <li class="{{ isCurrentPage($requestPathInfo, 'management', 'organizations') }}"><a href="{{ action('ManagementController@organization') }}">Organizations</a></li>
                        @endif

                        @if($globalUser->hasPermission('management.faq.see'))
                            <li class="{{ isCurrentPage($requestPathInfo, 'management', 'questions') }}"><a href="{{ action('ManagementController@questions') }}">Questions</a></li>
                        @endif

                        @if($globalUser->hasPermission('management.message.see'))
                            <li class="{{ isCurrentPage($requestPathInfo, 'management', 'messages') }}"><a href="{{ action('ManagementController@messages') }}">Messages</a></li>
                        @endif

                        @if($globalUser->hasPermission('management.suggestion.see'))
                            <li class="{{ isCurrentPage($requestPathInfo, 'management', 'suggestions') }}"><a href="{{ action('ManagementController@suggestions') }}">Suggestions</a></li>
                        @endif

                        @if($globalUser->hasPermission('management.invoice.see'))
                            <li class="{{ isCurrentPage($requestPathInfo, 'management', 'invoices') }}"><a href="{{ action('ManagementController@invoices') }}">Invoices</a></li>
                        @endif

                        @if($globalUser->hasPermission('management.logs.see'))
                            <li class="{{ isCurrentPage($requestPathInfo, 'management', 'logs') }}"><a href="{{ action('ManagementController@logger') }}">Logs</a></li>
                        @endif
                    </ul>
                </li>
            @endif
        </ul>
    </div><!-- Page Sidebar Inner -->
</div><!-- Page Sidebar -->
