@extends('layout')

@section('page-title', 'Management - Show Message')

@section('breadcrubs')
    <li class="active">Management</li>
    <li class="active">Show Message</li>
    <li class="active">{{ $message->id }}</li>
@stop

@section('extra-button')
    <a class="btn btn-default" href="{{ action('ManagementController@messages') }}">Return to Messages</a>
@stop

@section('content')
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="panel panel-white">
                <div class="panel-heading">
                    <h4 class="panel-title"></h4>
                </div>

                <div class="panel-body">
                    <h2>{{ $message->subject }}</h2>

                    <div class="row">
                        <div class="col-md-6">

                            <table class="table">
                                <tr>
                                    <th class="col-md-2">By</th>
                                    <td class="col-md-10">{{ $message->name }}</td>
                                </tr>
                                <tr>
                                    <th class="col-md-2">Email</th>
                                    <td class="col-md-10">{{ $message->email }}</td>
                                </tr>
                                <tr>
                                    <th class="col-md-2">Address</th>
                                    <td class="col-md-10">{{ $message->address }}</td>
                                </tr>
                                <tr>
                                    <th class="col-md-2">Date</th>
                                    <td class="col-md-10">{{ generateTimestamp($message->created_at) }}</td>
                                </tr>
                            </table>
                        </div>

                        <div class="col-md-6">
                            <p>{{ $message->body }}</p>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@stop
