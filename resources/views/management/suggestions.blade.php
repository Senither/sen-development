@extends('layout')

@section('page-title', 'Management - Suggestions')

@section('breadcrubs')
    <li class="active">Management</li>
    <li class="active">Suggestions</li>
@stop

@section('content')
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="panel panel-white">

                <div class="panel-body">
                    <h2>Suggestions</h2>
                    <p>Suggestions by users will be listed below, when a suggestion is completed it should be deleted.</p>
                </div>

            </div>
        </div>
    </div>

    @foreach($suggestions AS $suggestion)
    <?php $carbon = new \Carbon\Carbon($suggestion->created_at); ?>
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="panel panel-white">
                <div class="panel-body">
                    @if($globalUser->hasPermission('management.suggestion.delete'))
                        <p class="text-right"><a class="label label-danger" href="{{ action('ManagementController@suggestionDelete', ['id' => $suggestion->id]) }}">Delete Suggestion</a></p>
                    @endif

                    <h2 style="float: left; margin-top: 0px;">Suggestion by <strong>Alexis Tan</strong></h2>
                    <p class="text-right"><strong>{{ generateTimestamp($carbon) }}</strong>
                    <br>{{ $carbon->diffForHumans() }}.</p>

                    <div class="clearfix"></div>

                    {!! nl2br($suggestion->suggestion) !!}
                </div>
            </div>
        </div>
    </div>
    @endforeach
@stop
