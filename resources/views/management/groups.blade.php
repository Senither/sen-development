@extends('layout')

@section('page-title', 'Management - groups')

@section('breadcrubs')
    <li><a href="#">Management</a></li>
    <li class="active">Groups</li>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Level</th>
                                <th>Default</th>
                                <th>Options</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($groups AS $group)
                                <tr>
                                    <td>{{ $group->name }}</td>
                                    <td>{{ number_format($group->level) }}</td>
                                    <td>{!! $group->default ? '<span class="label label-success">Yes</span>' : '<span class="label label-danger">No</span>' !!}</td>
                                    <td>
                                        @if($globalUser->hasPermission('management.group.edit'))
                                            <a class="btn btn-default" href="{{ action('ManagementController@group', ['id' => $group->id, 'slug' => str_slug($group->name)]) }}">Edit</a>
                                        @else
                                            <a class="btn btn-default" disabled data-toggle="tooltip" data-original-title="You can't edit this group">Edit</a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop
