@extends('layout')

@section('page-title', 'Management - Users')

@section('breadcrubs')
    <li><a href="#">Management</a></li>
    <li class="active">Users</li>
@stop

@if($globalUser->hasPermission('management.user.invite'))
    @section('extra-button')
        <a class="btn btn-info" href="{{ action('ManagementController@userCreate') }}">Invite a New User</a>
    @stop
@endif

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-body">
                    <table class="table">
                        @foreach(['thead', 'tfoot'] AS $action)
                        {!! '<' . $action . '>' !!}
                            <tr>
                                <th></th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Group</th>
                                <th>Options</th>
                            </tr>
                        {!! '</' . $action . '>' !!}
                        @endforeach
                        <tbody>
                            @foreach($users AS $user)
                            <tr>
                                <td><img class="img-circle" src="{{ avatar_image($user) }}" width="40"></td>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>
                                <td>{{ $user->group->name }}</td>
                                <td>
                                    @if($globalUser->hasPermission('management.user.edit'))
                                        <a class="btn btn-default" href="{{ action('ManagementController@user', ['id' => $user->id, 'name' => str_slug($user->name)]) }}">Edit</a>
                                    @else
                                        <a class="btn btn-default" disabled data-toggle="tooltip" data-original-title="You can't edit this user">Edit</a>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                    {!! $users->render() !!}
                </div>
            </div>
        </div>
    </div>
@stop
