@extends('layout')

@section('page-title', 'Management - Edit File')

@section('breadcrubs')
    <li class="active">Management</li>
    <li class="active">Files</li>
    <li class="active">{{ $file->version }} [ID: {{ $file->id }}]</li>
    <li class="active">Edit</li>
@stop

@section('extra-button')
    <a class="btn btn-default" href="{{ action('ManagementController@files') }}">Return to Files</a>
@stop

@section('content')
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="panel panel-white">
                <div class="panel-body">
                    <p>This file was uploaded by <strong>{{ $file->author->name }}</strong> on <strong>{{ generateTimestamp($file->created_at) }}</strong> for the plugin, <strong>{{ $file->plugin->name }}</strong>, belonging to <strong>{{ $file->plugin->organization->name }}</strong>.</p>
                    <p>If you want to display code with the file changes, you can display it using the nice Markdown code syntax.</p>

                    @include('partial.form-errors')

                    <form action="{{ action('ManagementController@fileEdit', ['id' => $file->id, 'slug' => $file->slug]) }}" method="post">
                        {{ csrf_field() }}
                        <input type="hidden" name="file_id" value="{{ $file->id }}">
                        <input type="hidden" name="user_id" value="{{ $file->user_id }}">
                        <input type="hidden" name="plugin_id" value="{{ $file->plugin->id }}">
                        <input type="hidden" name="organization_id" value="{{ $file->plugin->organization->id }}">

                        <div class="form-group">
                            <label for="version">Version</label>
                            <input type="text" name="version" value="{{ $file->version }}" class="form-control" disabled="true">
                        </div>

                        <div class="form-group">
                            <label for="changes">Changes</label>
                            <textarea name="changes" id="changes" cols="30" rows="10" class="form-control">{{ $file->changes }}</textarea>
                        </div>

                        <input type="submit" class="btn btn-success" style="float: right;" value="Save File Changes">
                    </form>

                </div>
            </div>
        </div>
    </div>
@stop
