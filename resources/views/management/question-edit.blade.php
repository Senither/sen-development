@extends('layout')

@section('page-title', 'Management - Questions')

@section('breadcrubs')
    <li class="active">Management</li>
    <li class="active">Edit</li>
    <li class="active">Question</li>
@stop

@section('extra-button')
    <a class="btn btn-default" href="{{ action('ManagementController@questions') }}">Return to Questions</a>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <form action="{{ action('ManagementController@editQuestion', ['id' => $question->id]) }}" method="post">
                                {{ csrf_field() }}

                                <div class="form-group">
                                    <label for="name">Question</label>
                                    <input class="form-control" type="text" name="question" placeholder="The question that should be awnsered.." value="{{ $question->question }}" required>
                                </div>

                                <div class="form-group">
                                    <label for="name">Category</label>
                                    <select class="form-control" name="category_id" required>
                                        @foreach(App\Models\FaqCategory::all() AS $category)
                                            <option value="{{ $category->id }}"{{ $category->id == $question->category_id ? ' selected' : '' }}>{{ $category->name }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="name">Awnser</label>
                                    <textarea class="form-control" style="min-height: 220px;" name="awnser" placeholder="The awnser to the question above..">{{ $question->awnser }}</textarea>
                                </div>

                                <input style="float: right;" class="btn btn-success" type="submit" value="Save Question">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
