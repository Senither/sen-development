@extends('layout')

@section('page-title', 'Management - Files')

@section('breadcrubs')
    <li class="active">Management</li>
    <li class="active">Files</li>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <p class="lead">Files</p>
            <div class="panel panel-white">
                <div class="panel-body">
                    <table class="table">
                        @foreach(['<thead>', '<tfoot>'] AS $parent)
                        {!! $parent !!}
                            <tr>
                                <th>#</th>
                                <th>Plugin</th>
                                <th>Organizaton</th>
                                <th>Version</th>
                                <th>Date</th>
                                <th>Options</th>
                            </tr>
                        {!! $parent !!}
                        @endforeach
                        <tbody>
                            <?php $index = ($files->currentPage() - 1) * $files->perPage() + 1; ?>
                            @foreach($files->items() AS $file)
                            <tr>
                                <td>{{ $index++ }}</td>
                                <td><a class="btn btn-default" href="{{ action('PluginController@showVersion', ['organization' => $file->plugin->organization->slug, 'plugin' => $file->plugin->slug, str_slug($file->version)]) }}">{{ $file->plugin->name }}</a></td>
                                <td><a class="btn btn-default" href="{{ action('OrganizationController@show', ['organization' => $file->plugin->organization->slug]) }}">{{ $file->plugin->organization->name }}</a></td>
                                <td><a class="btn btn-default" href="{{ action('PluginController@showVersion', ['organization' => $file->plugin->organization->slug, 'plugin' => $file->plugin->slug, str_slug($file->version)]) }}">{{ $file->version }}</a></td>
                                <td>{{ generateTimestamp($file->created_at) }}<br><small>{{ $file->created_at->diffForHumans() }}</small></td>
                                <td>
                                    @if($globalUser->hasPermission('management.file.edit'))
                                        <a class="btn btn-default" href="{{ action('ManagementController@fileEdit', ['id' => $file->id, 'slug' => $file->slug]) }}">Edit</a>
                                    @else
                                        <a class="btn btn-default" disabled data-toggle="tooltip" data-original-title="You can't edit this file">Edit</a>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                    {!! $files->render() !!}
                </div>
            </div>
        </div>
    </div>
@stop
