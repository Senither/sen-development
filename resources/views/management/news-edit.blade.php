@extends('layout')

@section('page-title', 'Management - Edit News Article')

@section('breadcrubs')
    <li class="active">Management</li>
    <li class="active">News</li>
    <li class="active">{{ $article->id }}</li>
    <li class="active">Edit</li>
@stop

@section('extra-button')
    <a class="btn btn-default" href="{{ action('ManagementController@news') }}">Return to the News</a>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-body">
                    <p>The author of this article is <strong>{{ $article->author->name }}</strong>.</p>

                    @include('partial.form-errors')

                    <form action="{{ action('ManagementController@newsSave', ['id' => $article->id, 'slug' => str_slug($article->title)]) }}" method="post">
                        {{ csrf_field() }}
                        <input type="hidden" name="article" value="{{ $article->id }}">

                        <div class="form-group">
                            <label for="title">Title</label>
                            <input type="text" name="title" value="{{ $article->title }}" class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea name="description" id="description" cols="30" rows="5" class="form-control">{{ $article->description }}</textarea>
                        </div>

                        <div class="form-group">
                            <label for="body">News Article Body</label>
                            <textarea name="body" id="body" cols="30" rows="15" class="form-control">{{ $article->body }}</textarea>
                        </div>

                        <input type="submit" value="Save News Article" class="btn btn-success" style="float: right;">

                    </form>
                </div>
            </div>
        </div>
    </div>
@stop
