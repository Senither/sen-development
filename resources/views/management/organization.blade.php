@extends('layout')

@section('page-title', 'Management - Organizations')

@section('breadcrubs')
    <li class="active">Management</li>
    <li class="active">Organizations</li>
@stop

@section('content')
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="panel panel-white">
                <div class="panel-heading">
                    <h4 class="panel-title"></h4>
                </div>

                <div class="panel-body">

                    @if($organizations->count() == 0)
                        <p class="text-info">There are currently no organizations, you can create one by going to the Developer Tools, and using the button at the top right.</p>
                    @else
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Members</th>
                                    <th>Plugins</th>
                                    <th>Options</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>Name</th>
                                    <th>Members</th>
                                    <th>Plugins</th>
                                    <th>Options</th>
                                </tr>
                            </tfoot>
                            <tbody>
                                @foreach($organizations->items() AS $org)
                                    <tr>
                                        <td>{{ $org->name }}</td>
                                        <td><span class="label label-info">{{ $org->users->count() }}</span></td>
                                        <td><span class="label label-info">{{ $org->plugins->count() }}</span></td>
                                        <td>
                                            @if($globalUser->hasPermission('management.organization.edit'))
                                                <a class="btn btn-default" href="{{ action('ManagementController@organizationsMembers', ['organization' => $org->slug]) }}">Members</a>
                                            @endif

                                            <a class="btn btn-default" href="{{ action('OrganizationController@show', ['slug' => $org->slug]) }}">View</a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>

                        {!! $organizations->render() !!}
                        @endif
                </div>
            </div>
        </div>
    </div>
@stop
