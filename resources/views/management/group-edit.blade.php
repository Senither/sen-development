@extends('layout')

@section('page-title', 'Management - Edit Group')

@section('breadcrubs')
    <li class="active">Management</li>
    <li class="active">Edit</li>
    <li class="active">Group</li>
@stop

@section('extra-button')
    <a class="btn btn-default" href="{{ action('ManagementController@groups') }}">Return to Groups</a>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-body">
                    <form action="{{ action('ManagementController@saveGroup', ['id' => $group->id, 'slug' => str_slug($group->name)]) }}" method="post">
                        {{ csrf_field() }}
                        <input type="hidden" name="group" value="{{ $group->id }}">

                        <div class="row">
                            <div class="col-md-12">
                                <h2>Editing group <small>{{ $group->name }}</small></h2>

                                <br>

                                @include('partial.form-errors')

                                <div class="form-group">
                                    <label for="name">Group name</label>
                                    <input class="form-control" type="text" name="name" value="{{ $group->name }}">
                                </div>

                                <div class="form-group">
                                    <label for="level">Group Level</label>
                                    <input class="form-control" type="number" name="level" value="{{ $group->level }}" min="0" max="9999999">
                                </div>

                                <div class="form-group">
                                    <div class="checkbox">
                                        <label>
                                            <div class="checker">
                                                <span><input name="default" type="checkbox"{{ $group->default ? ' checked' : null }}></span>
                                            </div>
                                            This will act as the default group for the whole website, new members will automaticlly be added to this group!
                                        </label>
                                    </div>
                                </div>

                                <input class="btn btn-info" style="float: right;" type="submit" value="Save group">

                                <hr>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <h2>Permissions</h2>

                                @if($group->level >= 100000)
                                    <p class="text-info">This group has a power level above 100,000! This means that they will automatically have every permission.</p>
                                    <br>
                                @else
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Node</th>
                                            <th>Description</th>
                                            <th>Option</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>Node</th>
                                            <th>Description</th>
                                            <th>Option</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        @foreach($permissions AS $perm)
                                        <tr>
                                            <td>{{ $perm->node }}</td>
                                            <td>{{ $perm->name }}</td>
                                            <td>
                                                @if($group->permissions->contains('node', $perm->node))
                                                    <a class="btn btn-danger" href="{{ action('ManagementController@updateGroupPermission', ['group' => $group->id, 'permission' => $perm->id, 'action' => 'remove']) }}" data-toggle="tooltip" title="Notice: This will remove the permission node!" data-original-title="Notice: This will remove the permission node!">Remove</a>
                                                @else
                                                    <a class="btn btn-success" href="{{ action('ManagementController@updateGroupPermission', ['group' => $group->id, 'permission' => $perm->id, 'action' => 'add']) }}" data-toggle="tooltip" title="Notice: This will add the permission node!" data-original-title="Notice: This will add the permission node!">Add</a>
                                                @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                @endif
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
@stop
