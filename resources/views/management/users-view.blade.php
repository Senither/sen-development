@extends('layout')

@section('page-title', 'Management - Edit User - ' . $user->name)

@section('breadcrubs')
    <li class="active">Edit User</li>
    <li class="active">{{ $user->name }}</li>
@stop

@section('extra-button')
    <a class="btn btn-default" href="{{ action('ManagementController@users') }}">Return to Users</a>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="panel">
                <div class="panel-body">

                    <div class="row">
                        <div class="col-md-12">

                            <div class="row">
                                <div class="col-md-12">
                                    <h2>{{ $user->name }}'s profile <small>{!! $user->activation_token == null ? '<span class="label label-info">Active</span>' : '<span class="label label-danger">Inactive</span>' !!}</small></h2>
                                </div>
                            </div>

                        </div>

                        <div class="col-md-12">
                            <br>

                            @include('partial.form-errors')

                            <form action="{{ action('ManagementController@user', ['id' => $user->id, 'user' => str_slug($user->name)]) }}" method="POST">
                                {{ csrf_field() }}

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="name">Name</label>
                                            <input class="form-control" type="text" name="name" placeholder="The users name.." value="{{ $user->name }}">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="themes_id">Site theme</label>
                                            <select class="form-control" name="themes_id">
                                                @foreach($themes AS $theme)
                                                    <option value="{{ $theme->id }}"{{ $theme->id == $user->theme->id ? ' selected' : null }}>{{ $theme->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="email">Email</label>
                                            <input class="form-control" type="email" name="email" placeholder="The users email.." value="{{ $user->email }}">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="groups_id">Group</label>
                                            <select class="form-control" name="groups_id">
                                                @foreach($groups AS $group)
                                                    <option value="{{ $group->id }}"{{ $group->id == $user->group->id ? ' selected' : null }}>{{ $group->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <h3>General Settings</h3><br>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="minecraft">Minecraft name</label>
                                            <input class="form-control" type="text" name="minecraft" placeholder="Your minecraft name.." value="{{ $user->minecraft }}">
                                            <!-- <p>Adding your minecraft name will automatically whitelist you on the development server.</p> -->
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="website">Minecraft website</label>
                                            <input class="form-control" type="text" name="website" placeholder="Your minecraft website" value="{{ $user->website }}">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <input style="float: right;" class="btn btn-success" type="submit" value="Save User Settings">
                                    </div>
                                </div>

                            </form>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@stop
