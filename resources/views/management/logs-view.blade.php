@extends('layout')

@section('page-title', 'Management - Logs View')

@section('breadcrubs')
    <li class="active">Management</li>
    <li class="active">Logs</li>
    <li class="active">View</li>
    <li class="active">ID: {{ $log->id }}</li>
@stop

@section('extra-button')
    <a class="btn btn-default" href="{{ action('ManagementController@logger') }}">Return to Logs</a>
@stop

@section('content')
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <p class="lead">Action: <span class="label label-info">{{ $log->action }}</span></p>
            <div class="panel panel-white">

                <div class="panel-body">
                    <table class="table">
                        <tr>
                            <th>Message</th>
                            <td>{{ $log->message }}</td>
                        </tr>
                        <tr>
                            <th>User</th>
                            <td>{!! $log->user == null ? '<i class="text-default">Unknown User</i>' : $log->user->name !!}</td>
                        </tr>
                        <tr>
                            <th>User Group</th>
                            <td>{!! $log->user == null ? '<i class="text-default">Unknown Group</i>' : $log->user->group->name !!}</td>
                        </tr>
                        <tr>
                            <th>Address</th>
                            <td>{{ $log->address }}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 col-md-12">
            <p class="lead">Changes</p>
            <div class="panel panel-white">

                <div class="row">

                    <div class="col-md-6">
                        <div class="panel-body">
                            <h4>Before</h4><br>

                            @if($log->old != null)
                            <table class="table">
                                @foreach($log->old AS $key => $value)
                                    <tr>
                                        <th>{{ $key }}</th>
                                        <td>{{ $value }}</td>
                                    </tr>
                                @endforeach
                            </table>
                            @else
                                <p class="text-info"><i>There are no records to be show here..</i></p>
                            @endif
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="panel-body">
                            <h4>After</h4><br>

                            @if($log->new != null)
                            <table class="table">
                                @foreach($log->new AS $key => $value)
                                    <tr>
                                        <th>{{ $key }}</th>
                                        <td>{{ $value }}</td>
                                    </tr>
                                @endforeach
                            </table>
                            @else
                                <p class="text-info"><i>There are no records to be show here..</i></p>
                            @endif
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
@stop
