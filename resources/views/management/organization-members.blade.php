@extends('layout')

@section('page-title', 'Management - Organizations')

@section('breadcrubs')
    <li class="active">Management</li>
    <li class="active">Organizations</li>
    <li class="active">{{ $organization->name }}</li>
    <li class="active">Members</li>
@stop

@section('extra-button')
    <a class="btn btn-default" href="{{ action('ManagementController@organization') }}">Return to Organizations</a>
@stop

@section('content')
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <p class="lead">Users who are apart of this organization.</p>
            <div class="panel panel-white">
                <div class="panel-body">

                    <table class="table">
                        @foreach(['thead', 'tfoot'] AS $action)
                        {!! '<' . $action . '>' !!}
                            <tr>
                                <th class="col-md-2 col-xs-3"></th>
                                <th class="col-md-4 col-xs-5">Name</th>
                                <th class="col-md-3 col-xs-3">Group</th>
                                <th class="col-md-3 col-xs-3">Options</th>
                            </tr>
                        {!! '</' . $action . '>' !!}
                        @endforeach
                        <tbody>
                            @foreach($all AS $member)
                                <?php if (!$members->contains('email', $member->email)) {
    continue;
} ?>
                                <tr>
                                    <td><img src="{!! avatar_image($member, 42) !!}" width="42" class="img-circle img-responsive" alt=""></td>
                                    <td>{{ $member->name }}</td>
                                    <td>{{ $member->group->name }}</td>
                                    <td>
                                        @if($organization->user_id == $member->id)
                                            <a class="btn btn-info" href="#">Organization Owner</a>
                                        @else
                                            <a class="btn btn-danger" href="{{ action('ManagementController@organizationsActionMember', ['organization' => $organization->slug, $member->id, 'remove']) }}">Remove Member</a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="panel panel-white">
                <div class="panel-body">

                    <table class="table">
                        @foreach(['thead', 'tfoot'] AS $action)
                        {!! '<' . $action . '>' !!}
                            <tr>
                                <th class="col-md-2 col-xs-3"></th>
                                <th class="col-md-4 col-xs-5">Name</th>
                                <th class="col-md-3 col-xs-3">Group</th>
                                <th class="col-md-3 col-xs-3">Options</th>
                            </tr>
                        {!! '</' . $action . '>' !!}
                        @endforeach
                        <tbody>
                            @foreach($all AS $member)
                                <?php if ($members->contains('email', $member->email)) {
    continue;
} ?>
                                <tr>
                                    <td><img src="{!! avatar_image($member, 42) !!}" width="42" class="img-circle img-responsive" alt=""></td>
                                    <td>{{ $member->name }}</td>
                                    <td>{{ $member->group->name }}</td>
                                    <td>
                                        <a class="btn btn-success" href="{{ action('ManagementController@organizationsActionMember', ['organization' => $organization->slug, $member->id, 'add']) }}">Add Member</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
@stop
