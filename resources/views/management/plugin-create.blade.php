@extends('layout')

@section('page-title', 'Management - Create new Plugin')

@section('breadcrubs')
    <li><a href="#">Management</a></li>
    <li class="active">Plugins</li>
    <li class="active">Create a new Plugin</li>
@stop

@section('extra-button')
    <a class="btn btn-default" href="{{ action('ManagementController@plugins') }}">Return to Plugins</a>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="panel">
                <div class="panel-body">

                    <div class="row">
                        <div class="col-md-12">

                            <div class="row">
                                <div class="col-md-12">
                                    <h2>Create a new Plugin</h2>
                                </div>
                            </div>

                        </div>

                        <div class="col-md-12">
                            <br>

                            @include('partial.form-errors')

                            <p>Create the form here..</p>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@stop
