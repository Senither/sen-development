@extends('layout')

@section('page-title', 'Management - Plugins')

@section('breadcrubs')
    <li><a href="#">Management</a></li>
    <li class="active">Plugins</li>
    <li class="active">{{ $plugin->name }} [ID: {{ $plugin->id }}]</li>
    <li class="active">Edit</li>
@stop

@section('extra-button')
    <a class="btn btn-default" href="{{ action('ManagementController@plugins') }}">Return to Plugins</a>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-body">
                    <p>This plugin belongs to the <strong>{{ $plugin->organization->name }}</strong> organization, the plugins original authos is <strong>{{ $plugin->author->name }}</strong>.</p>

                    @include('partial.form-errors')

                    <form action="{{ action('ManagementController@pluginSave', ['id' => $plugin->id, 'plugin' => $plugin->slug]) }}" method="post">
                        {{ csrf_field() }}

                        <input type="hidden" name="plugin_id" value="{{ $plugin->id }}">
                        <input type="hidden" name="user_id" value="{{ $plugin->user_id }}">

                        <div class="form-group">
                            <label for="name">Plugin Name</label>
                            <input type="text" class="form-control" name="name" value="{{ $plugin->name }}"{{ $plugin->user_id != $globalUser->id ? ' disabled' : null }}>
                        </div>

                        <div class="form-group">
                            <label for="status">Plugin Status</label>
                            <select name="status" id="status" class="form-control">
                                @foreach($statuses AS $id => $status)
                                    <option value="{{ $id }}"{{ $plugin->status == $id ? ' selected' : null }}>{{ $status }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="availability">Availability</label>
                            <select name="availability" id="availability" class="form-control">
                                <option value="0"{{ $plugin->availability == 0 ? ' selected' : null }}>Private</option>
                                <option value="1"{{ $plugin->availability != 0 ? ' selected' : null }}>Public</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea name="description" id="description" col="30" rows="10" class="form-control">{{ $plugin->description }}</textarea>
                        </div>

                        <button type="submit" class="btn btn-success" style="float: right;">Save Plugin</button>
                    </form>

                </div>
            </div>
        </div>
    </div>
@stop
