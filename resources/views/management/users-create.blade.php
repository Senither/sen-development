@extends('layout')

@section('page-title', 'Management - Invite User')

@section('breadcrubs')
    <li><a href="#">Management</a></li>
    <li class="active">User</li>
    <li class="active">Invite a New User</li>
@stop

@section('extra-button')
    <a class="btn btn-default" href="{{ action('ManagementController@users') }}">Return to Users</a>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="panel">
                <div class="panel-body">

                    <div class="row">
                        <div class="col-md-12">

                            <div class="row">
                                <div class="col-md-12">
                                    <h2>Invite new user</h2>
                                </div>
                            </div>

                        </div>

                        <div class="col-md-12">
                            <br>

                            @include('partial.form-errors')

                            <form action="{{ action('ManagementController@userCreate') }}" method="POST">
                                {{ csrf_field() }}

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="name">Name</label>
                                            <input class="form-control" type="text" name="name" placeholder="The users name.." value="{{ Request::old('name') }}" required>
                                        </div>

                                        <div class="form-group">
                                            <label for="name">Email</label>
                                            <input class="form-control" type="email" name="email" placeholder="The users email.." value="{{ Request::old('email') }}" required>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <p><strong class="text-danger">Notice!</strong>
                                        <br>Once you click the <span class="label label-info">Invite User</span> button, the inviation email will be sent off to the given email, so please do make sure that the name and email is correct before clicking invite.</p>
                                        <input style="float: right;" class="btn btn-info" type="submit" value="Invite User">
                                    </div>
                                </div>

                            </form>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@stop
