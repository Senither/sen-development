@extends('layout')

@section('page-title', 'Management - Creates news Article')

@section('breadcrubs')
    <li class="active">Management</li>
    <li class="active">News</li>
    <li class="active">Create</li>
@stop

@section('extra-button')
    <a class="btn btn-default" href="{{ action('ManagementController@news') }}">Return to the News</a>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-body">

                    @include('partial.form-errors')

                    <form action="{{ action('ManagementController@newsCreate') }}" method="post">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label for="title">Title</label>
                            <input type="text" name="title" value="{{ Request::old('title') }}" class="form-control" placeholder="What is the news about?">
                        </div>

                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea name="description" id="description" cols="30" rows="5" class="form-control" placeholder="Give some basic information about the update, just try to cover the top layer..">{{ Request::old('description') }}</textarea>
                        </div>

                        <div class="form-group">
                            <label for="body">News Article Body</label>
                            <textarea name="body" id="body" cols="30" rows="15" class="form-control" placeholder="Create the main news article information here..">{{ Request::old('body') }}</textarea>
                        </div>

                        <input type="submit" value="Create News Article" class="btn btn-success" style="float: right;">
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop
