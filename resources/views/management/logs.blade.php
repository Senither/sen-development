@extends('layout')

@section('page-title', 'Management - Logs')

@section('breadcrubs')
    <li class="active">Management</li>
    <li class="active">Logs</li>
@stop

@section('extra-button')
    <!--<a class="btn btn-info" href="#">Purge old Logs</a>-->
@stop

@section('content')
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="panel panel-white">
                <div class="panel-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Action</th>
                                <th>User</th>
                                <th>Address</th>
                                <th>Date</th>
                                <th>Options</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>Action</th>
                                <th>User</th>
                                <th>Address</th>
                                <th>Date</th>
                                <th>Options</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            @foreach($logs->items() AS $log)
                                <tr>
                                    <td><span class="label label-info">{{ $log->action }}</span></td>
                                    <td>{!! $log->user == null ? '<i class="text-default">Unknown User</i>' : $log->user->name !!}</td>
                                    <td>{{ $log->address }}</td>
                                    <td>{{ generateTimestamp($log->created_at) }}<br><small>{{ $log->created_at->diffForHumans() }}</small></td>
                                    <td>
                                        <a class="btn btn-default" href="{{ action('ManagementController@loggerShow', ['id' => $log->id]) }}">View Full Log</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                    {!! $logs->render() !!}
                </div>
            </div>
        </div>
    </div>
@stop
