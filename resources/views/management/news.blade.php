@extends('layout')

@section('page-title', 'Management - News')

@section('breadcrubs')
    <li class="active">Management</li>
    <li class="active">News</li>
@stop

@section('extra-button')
    <a class="btn btn-info" href="{{ action('ManagementController@newsCreate') }}">Create new News Article</a>
@stop

@section('content')
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="panel panel-white">
                <div class="panel-heading">
                    <h4 class="panel-title"></h4>
                </div>

                <div class="panel-body">
                    <table class="table">
                        @foreach(['thead', 'tfoot'] AS $action)
                        {!! '<' . $action . '>' !!}
                            <tr>
                                <th>#</th>
                                <td>Author</td>
                                <td>Title</td>
                                <td>Options</td>
                            </tr>
                        {!! '</' . $action . '>' !!}
                        @endforeach
                        <tbody>
                            <?php $index = ($news->currentPage() - 1) * $news->perPage() + 1;  ?>
                            @foreach($news AS $article)
                                <tr>
                                    <td>{{ $index++ }}</td>
                                    <td>{{ $article->author->name }}</td>
                                    <td>{{ $article->title }}</td>
                                    <td>
                                        <a class="btn btn-default" href="{{ action('ManagementController@newsEdit', ['id' => $article->id, 'slug' => str_slug($article->title)]) }}">Edit</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                    {!! $news->render() !!}
                </div>
            </div>
        </div>
    </div>
@stop
