@extends('layout')

@section('page-title', 'Management - Plugins')

@section('breadcrubs')
    <li><a href="#">Management</a></li>
    <li class="active">Plugins</li>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-body">
                    <table class="table">
                        @foreach(['thead', 'tfoot'] AS $action)
                        {!! '<' . $action . '>' !!}
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Version</th>
                                <th>Organization</th>
                                <th>Progress</th>
                                <th>Options</th>
                            </tr>
                        {!! '</' . $action . '>' !!}
                        @endforeach
                        <tbody>
                            <?php $usersCountableIndex = ($plugins->currentPage() - 1) * $plugins->perPage(); ?>
                            <?php $userHasBypassPermission = $globalUser->hasPermission('management.secure-checks.exempt'); ?>
                            @foreach($plugins AS $plugin)
                            <tr>
                                <td>{{ ++$usersCountableIndex }}</td>
                                <td>{{ $plugin->name }}</td>
                                <td>{{ $plugin->version }}</td>
                                <td>{{ $plugin->organization->name }}</td>
                                <td><?php if ($plugin->isFinished): ?>
                                            <span class="label label-success">Finished</span>
                                        <?php elseif ($plugin->isPending): ?>
                                            <span class="label label-primary">Pending</span>
                                        <?php elseif ($plugin->isUnderProgress): ?>
                                            <span class="label label-info">Being worked on</span>
                                        <?php endif; ?></td>
                                <td>
                                    @if($globalUser->hasPermission('management.file.edit'))
                                        <a class="btn btn-default" href="{{ action('ManagementController@plugin', ['id' => $plugin->id, 'slug' => $plugin->slug]) }}">Edit</a>
                                    @else
                                        <a class="btn btn-default" disabled data-toggle="tooltip" data-original-title="You can't edit this plugin">Edit</a>
                                    @endif

                                    @if($userHasBypassPermission)
                                        <a class="btn btn-default" href="{{ action('PluginController@show', ['organization' => $plugin->organization->slug, 'plugin' => $plugin->slug]) }}">View</a>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                    {!! $plugins->render() !!}
                </div>
            </div>
        </div>
    </div>
@stop
