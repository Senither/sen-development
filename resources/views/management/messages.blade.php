@extends('layout')

@section('page-title', 'Management - Contact Messages')

@section('breadcrubs')
    <li class="active">Management</li>
    <li class="active">Contact Messages</li>
@stop

@section('content')
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="panel panel-white">
                <div class="panel-heading">
                    <h4 class="panel-title"></h4>
                </div>

                <div class="panel-body">

                    @if($messages->count() == 0)
                        <p class="text-info">There are currently no messages.</p>
                    @else
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Viewed</th>
                                    <th>Post Date</th>
                                    <th>Options</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Viewed</th>
                                    <th>Post Date</th>
                                    <th>Options</th>
                                </tr>
                            </tfoot>
                            <tbody>
                                @foreach($messages->items() AS $message)
                                    <tr>
                                        <td>{{ $message->id }}</td>
                                        <td>{{ $message->name }}</td>
                                        <td>{{ $message->email }}</td>
                                        <td>{!! in_array($globalUser->id, $message->read) ? '<span class="label label-success">Yes</span>' : '<span class="label label-danger">No</span>' !!}</td>
                                        <td>{{ generateTimestamp($message->created_at) }}<br><small>{{ $message->created_at->diffForHumans() }}</small></td>
                                        <td>
                                            <a class="btn btn-default" href="{{ action('ManagementController@messageShow', ['id' => $message->id]) }}">View</a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>

                        {!! $messages->render() !!}
                        @endif
                </div>
            </div>
        </div>
    </div>
@stop
