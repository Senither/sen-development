@extends('layout')

@section('page-title', 'Management - Questions')

@section('breadcrubs')
    <li class="active">Management</li>
    <li class="active">Questions (FAQ)</li>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <p class="lead">Unawnsered questions</p>
            <div class="panel">
                <div class="panel-body">
                    @if($unawnsered->isEmpty())
                        <p>There are currently no unawnsered questions.</p>
                    @else
                        <table class="table">
                            <thead>
                                <tr>
                                    <th class="col-md-1">ID</th>
                                    <th class="col-md-2">Category</th>
                                    <th class="col-md-2">User</th>
                                    <th class="col-md-4">Question</th>
                                    <th class="col-md-3">Option</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($unawnsered AS $question)
                                    <tr>
                                        <td>{{ $question->id }}</td>
                                        <td>{{ $question->category }}</td>
                                        <td>{{ $question->name == null ? 'Unknown' : $question->name }}</td>
                                        <td>{{ str_finish($question->question, '?') }}</td>
                                        <td>
                                            <a class="btn btn-success" href="{{ action('ManagementController@editQuestion', ['id' => $question->id]) }}">Awnser</a>
                                            <a class="btn btn-danger" href="{{ action('ManagementController@deleteQuestion', ['id' => $question->id]) }}">Delete</a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <p class="lead">Awnsered questions</p>
            <div class="panel">
                <div class="panel-body">
                    <?php
                        $questionSize = 0;
                        foreach ($awnsered as $category) {
                            $questionSize += $category->questions->count();
                        }
                    ?>
                    @if($questionSize == 0)
                        <p>There are currently no awnsered questions.</p>
                    @else
                        <table class="table">
                            <thead>
                                <tr>
                                    <th class="col-md-1">ID</th>
                                    <th class="col-md-2">Category</th>
                                    <th class="col-md-7">Question</th>
                                    <th class="col-md-2">Option</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($awnsered AS $category)
                                    @foreach($category->questions AS $question)
                                        <tr>
                                            <td>{{ $question->id }}</td>
                                            <td>{{ $category->name }}</td>
                                            <td>{{ str_finish($question->question, '?') }}</td>
                                            <td>
                                                <a class="btn btn-info" href="{{ action('ManagementController@editQuestion', ['id' => $question->id]) }}">Edit</a>
                                                <a class="btn btn-danger" href="{{ action('ManagementController@deleteQuestion', ['id' => $question->id]) }}">Delete</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endforeach
                            </tbody>
                        </table>
                    @endif
                </div>
            </div>
        </div>
    </div>
@stop
