@extends('layout')

@section('page-title', 'Invoice - All')

@section('breadcrubs')
    <li><a href="#">Invoice</a></li>
    <li class="active">All</li>
@stop

@if($globalUser->hasPermission('development.invoice.see'))
    @section('extra-button')
        <a class="btn btn-default" href="{{ action('DeveloperController@invoice') }}">Manage Invoices</a>
    @stop
@endif

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-body">
                    @if($invoices->isEmpty())
                        <p>You currently don't have any invoices..</p>
                    @else
                    <table class="table">

                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Receipt</th>
                                <th>Sender</th>
                                <th>total</th>
                                <th>Status</th>
                                <th>Sent Date</th>
                                <th>Options</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $index = ($invoices->currentPage() - 1) * $invoices->perPage() + 1; ?>
                            @foreach($invoices AS $inv)
                                <tr>
                                    <td>{{ $index++ }}</td>
                                    <td>#{{ $inv->receipt }}</td>
                                    <td>{{ $inv->sender->name }}</td>
                                    <td>${{ $inv->total }}</td>
                                    <td>{!! $inv->status == 0 ? '<span class="label label-danger">Pending</span>' : '<span class="label label-success">Paid</span>' !!}</td>
                                    <td>{{ generateTimestamp($inv->created_at) }}<br><small>{{ $inv->created_at->diffForHumans() }}</small></td>
                                    <td>
                                        <a class="btn btn-default" href="{{ action('InvoiceController@show', ['receipt' => $inv->receipt]) }}">
                                            View full Invoice
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>

                        {!! $invoices->render() !!}
                    </table>
                    @endif
                </div>
            </div>
        </div>
    </div>
@stop
