@extends('layout')

@section('page-title', 'Invoice - #' . $invoice->receipt)

@section('breadcrubs')
    <li><a href="#">Invoice</a></li>
    <li class="active">#{{ $invoice->receipt }}</li>
@stop

@section('content')
    @if(Input::has('payment') && Input::get('payment') == 'success')
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-white">
                    <div class="panel-body">
                        <p>Thank you for your payment, the invoice will update as soon as the payment has been confirmed.</p>
                    </div>
                </div>
            </div>
        </div>
    @endif
    <div class="row">
        <div class="invoice col-md-12">
            <div class="panel panel-white">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-4">
                            <h1 class="m-b-md"><b>SenDevelopment</b></h1>
                        </div>
                        <div class="col-md-8 text-right">
                            <h1>INVOICE<br><small>#{{ $invoice->receipt }}</small></h1>
                        </div>
                        <div class="col-md-12">
                            <hr>
                            <p>
                                <strong>Invoice to</strong><br>
                                {{ $invoice->receiver->name }}<br>
                                {{ $invoice->receiver->email }}<br>
                            </p>
                        </div>
                        <div class="col-md-12">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Item</th>
                                        <th>Quantity</th>
                                        <th>Price /each</th>
                                        <th>Sub Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $total = 0; ?>
                                    @foreach($invoice->items AS $item)
                                    <tr>
                                        <td>{{ $item['name'] }}</td>
                                        <td>{{ $item['quantity'] }}</td>
                                        <td>${{ number_format($item['cost'], 2) }}</td>
                                        <td>${{ number_format($item['cost'] * $item['quantity'], 2) }}</td>
                                    </tr>
                                    <?php $total = $total + ($item['quantity'] * $item['cost']); ?>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-12">
                            <div class="text-right">
                                <h4 class="no-m m-t-md text-success">Total</h4>
                                <h1 class="no-m text-success">${{ number_format($total, 2) }}</h1>
                                <hr>
                                @if(!$invoice->status)
                                    @if($globalUser->id == $invoice->user_id)
                                        @if(Input::get('payment', '') != 'success')
                                            <button id="payment-process" class="btn btn-info"><i class="fa fa-paypal"></i> Pay with PayPal</button>
                                        @endif
                                    @else
                                    <form action="{{ action('InvoiceController@confirm', ['receipt' => $invoice->receipt]) }}" method="post">
                                        {{ csrf_field() }}
                                        <button type="submit" class="btn btn-info"><i class="fa fa-check-square-o"></i> Mark Invoice as Paid</button>
                                        <input type="hidden" name="token" value="{{ $invoice->token }}">
                                    </form>
                                    @endif
                                @else
                                    <p>This invoice was paid for on the <i>{{ generateTimestamp($invoice->paid_at) }}</i>.<p>
                                @endif
                            </div>
                        </div>
                    </div><!--row-->
                </div>
            </div>
        </div>
    </div><!-- Row -->

    <div id="payment-form"></div>
@stop

@if(!$invoice->status && $globalUser->id == $invoice->user_id)
    @section('script')
        <script>
        $(function () {
            var $button = $('#payment-process');

            $('#payment-process').on('click', function () {
                $button.addClass('disabled');
                $button.html('<i class="fa fa-spinner fa-spin"></i> Processing Payment')

                $.ajax({
                    type: 'POST',
                    url:  '/client/invoice',
                    data: {
                        invoice: '<?php echo $invoice->receipt; ?>',
                        token:   '<?php echo $invoice->token; ?>',
                        _token:  '<?php echo csrf_token(); ?>'
                    },
                    success: function (data) {
                        if(data.status == 'success') {
                            $('#payment-form').append(data.data[0]);
                            $.each(data.data, function (i, item) {
                                if(i != 0) {
                                    $('#paypal_invoice_form').append(item);
                                }
                            });

                            setTimeout(function() {
                                document.forms['paypal_invoice_form'].submit();

                            }, 150);
                        }
                        console.log(data);
                    },
                    error: function (data) {

                        toastr.options = {
                            closeButton: true,
                            progressBar: true,
                            showMethod: 'slideDown',
                            hideMethod: 'slideUp',
                            timeOut: 7500
                        };
                        toastr.error(data.responseJSON.reason, 'PayPal Processing Error!');
                        $button.removeClass('disabled');
                        $button.html('<i class="fa fa-paypal"></i> Pay with PayPal');
                    }
                });
            });

            $(window).unload(function() {
                $button.removeClass('disabled');
                $button.html('<i class="fa fa-paypal"></i> Pay with PayPal');
            });
        });
        </script>
    @stop
@endif
