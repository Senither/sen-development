@extends('layout')

@section('page-title', 'News Feed')

@section('breadcrubs')
    <li class="active">News Feed</li>
@stop

@section('extra-button')
    <a class="btn btn-default" href="{{ action('NewsController@index') }}">Return to News Feed</a>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-white">
                <div class="panel-body">
                    <h2>{{ $article->title }}</h2>
                    <p style="float: left;">The article was posted {{ $article->created_at->diffForHumans() }}<br><strong>Date:</strong> {{ generateTimestamp($article->created_at) }}</p>
                    <p style="float: right;"><strong>Author: </strong> {{ $article->author->name }}</p>
                    <p class="clearfix"></p>

                    <hr>

                    <p>{!! nl2br(e($article->body)) !!}</p>
                </div>
            </div>

        </div>
    </div>
@stop
