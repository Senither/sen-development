@extends('layout')

@section('page-title', 'News Feed')

@section('breadcrubs')
    <li class="active">News Feed</li>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">

            <section id="cd-timeline" class="cd-container">

                <?php $index = $news->currentPage() * $news->perPage(); ?>
                <?php for ($i = (($news->currentPage() - 1) * $news->perPage()); $i < ($news->currentPage() * $news->perPage()); ++$i): ?>
                <?php if ($news->count() <= $i) {
    break;
} ?>
                <?php $article = $news->items()[$i]; ?>
                <div id="{{ $article->id }}" class="cd-timeline-block">
                    <div class="cd-timeline-img cd-info">
                        <i class="fa fa-tag"></i>
                    </div>

                    <div class="cd-timeline-content">
                        <h2><a href="{{ action('NewsController@show', ['id' => $article->id, 'slug' => str_slug($article->title)]) }}">{{ $article->title }}</a></h2>
                        <p>{{ $article->description }}</p>
                        <span class="cd-date">Updated was posted {{ $article->created_at->diffForHumans() }} | {{ generateTimestamp($article->created_at) }}</span>
                    </div>
                </div>
                <?php endfor; ?>
            </section>

            <div class="panel panel-white">
                <div class="panel-body">
                    {!! $news->render() !!}
                </div>
            </div>

        </div>
    </div>
@stop
