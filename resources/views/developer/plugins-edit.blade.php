@extends('layout')

@section('page-title', 'Developer Tools - Edit Plugin')

@section('breadcrubs')
    <li class="active">Developer Tools</li>
    <li class="active">Plugins</li>
    <li class="active">{{ $plugin->name }}</li>
    <li class="active">Edit</li>
@stop

@section('extra-button')
    <a class="btn btn-default" href="{{ action('DeveloperController@plugins') }}">Return to Plugins</a>
@stop

@section('content')
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="panel panel-white">
                <div class="panel-body">

                    @include('partial.form-errors')

                    <form action="{{ action('DeveloperController@pluginEdit', ['organization' => $organization->slug, 'plugin' => $plugin->slug]) }}" method="post">
                        {{ csrf_field() }}

                        <input type="hidden" name="plugin_id" value="{{ $plugin->id }}">
                        <input type="hidden" name="user_id" value="{{ $plugin->user_id }}">
                        <input type="hidden" name="organization_id" value="{{ $organization->id }}">

                        <div class="form-group">
                            <label for="name">Plugin Name</label>
                            <input type="text" class="form-control" name="name" value="{{ $plugin->name }}"{{ $plugin->user_id != $globalUser->id ? ' disabled' : null }}>
                        </div>

                        @if($plugin->availability == 1 || $plugin->user_id == $globalUser->id)
                            <div class="form-group">
                                <label for="status">Plugin Status</label>
                                <select name="status" id="status" class="form-control">
                                    @foreach($statuses AS $id => $status)
                                        <option value="{{ $id }}"{{ $plugin->status == $id ? ' selected' : null }}>{{ $status }}</option>
                                    @endforeach
                                </select>
                            </div>

                            @if($plugin->user_id == $globalUser->id)
                            <div class="form-group">
                                <label for="availability">Availability</label>
                                <select name="availability" id="availability" class="form-control">
                                    <option value="0"{{ $plugin->availability == 0 ? ' selected' : null }}>Private</option>
                                    <option value="1"{{ $plugin->availability != 0 ? ' selected' : null }}>Public</option>
                                </select>
                            </div>
                            @endif

                            <div class="form-group">
                                <label for="description">Description</label>
                                <textarea name="description" id="description" col="30" rows="10" class="form-control">{{ $plugin->description }}</textarea>
                            </div>

                            <button type="submit" class="btn btn-success" style="float: right;">Save Plugin</button>
                        @else
                            <p>The plugins availabilities status has been set to <strong>Private</strong>, this means that you can't edit this plugins details. If you wish to make any changes to this plugin, please contact the owner/creator/author of the plugin, about changes so they can give you access or make the changes for you.</p>
                        @endif
                    </form>

                </div>
            </div>
        </div>
    </div>
@stop
