@extends('layout')

@section('page-title', 'Create a new Organization')

@section('breadcrubs')
    <li class="active">Developer Tools</li>
    <li class="active">My Organizations</li>
    <li class="active">Create new Organization</li>
@stop

@section('extra-button')
    <a class="btn btn-default" href="{{ action('DeveloperController@organizations') }}">Return to Organizations</a>
@stop

@section('content')
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="panel panel-white">
                <div class="panel-body">

                    @include('partial.form-errors')

                    <form action="{{ action('DeveloperController@organizationCreate') }}" method="post">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label for="name">Organization Name</label>
                            <input type="text" name="name" id="id" class="form-control">
                        </div>

                        <p>When creating a new organiation, please make sure that the name isn't already taken.
                        <br>It is a good idea, when creating a new organization, that the name name follows a few conventions:</p>

                        <ul>
                            <li>The name should represent the client or their company.</li>
                            <li>The name should be longer than four(4) characters.</li>
                            <li>The name should be no more than twenty two(22) characters.</li>
                            <li>If spaces are to be removed in a name, it should be following the camel case convention.</li>
                        </ul>

                        <p>The name can exteed the 22 characters guideline, however that is the recommended limit to prevent names filling more than one line in the menu, ofcourse the character width depents on which characters are actually being used in the name, so some names may require less characters, and some require more, to fill out a line in the menu. Just keep this in mind when making the organization name as it won't be possible to edit the name later on.</p>
                        <p>Once your organization is created, you'll be forwarded to the members page where you'll be able to invite users to the organization, if they don't have an account, you'll be able to send them an invite to join the management system.</p>
                        <hr>

                        <input type="submit" class="btn btn-success" style="float: right;" value="Create Organization">
                        <p class="text-right" style="float: right; padding-top: 7px; padding-right: 10px;"><strong>I have read the nameing conventions and guidelines, </strong></p>
                    </form>

                </div>
            </div>
        </div>
    </div>
@stop
