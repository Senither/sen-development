@extends('layout')

@section('page-title', 'Developer Tools - Edit Plugin')

@section('breadcrubs')
    <li class="active">Developer Tools</li>
    <li class="active">Plugins</li>
    <li class="active">{{ $plugin->name }}</li>
    <li class="active">Edit</li>
@stop

@if($plugin->availability == 1 || $plugin->user_id == $globalUser->id)
    @section('extra-button')
        <a class="btn btn-info" href="{{ action('DeveloperController@pluginUpdate', ['organization' => $plugin->organization->slug, 'plugin' => $plugin->slug]) }}">Add a new File</a>
    @stop
@endif

@section('content')
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="panel panel-white">
                <div class="panel-body">
                    <table class="table">
                        @foreach(['thead', 'tfoot'] AS $action)
                        {!! '<' . $action . '>' !!}
                            <tr>
                                <th>#</th>
                                <th>Version</th>
                                <th>Author</th>
                                <th>Created</th>
                                <th>Last updated</th>
                                <th>Options</th>
                            </tr>
                        {!! '</' . $action . '>' !!}
                        @endforeach
                        <tbody>
                            <?php $index = $files->currentPage() * $files->perPage(); ?>
                            <?php for ($i = (($files->currentPage() - 1) * $files->perPage()); $i < ($files->currentPage() * $files->perPage()); ++$i): ?>
                            <?php if ($files->count() <= $i) {
    break;
} ?>
                            <?php $file = $files->items()[$i]; ?>
                            <tr>
                                <td>{{ $i + 1 }}</td>
                                <td>{{ $file->version }}</td>
                                <td>{{ $file->author->name }}</td>
                                <td>{{ generateTimestamp($file->created_at) }}<br><small>{{ $file->created_at->diffForHumans() }}</small></td>
                                <td>{{ generateTimestamp($file->updated_at) }}<br><small>{{ $file->updated_at->diffForHumans() }}</small></td>
                                <td>
                                    <a class="btn btn-default" href="{{ action('DeveloperController@pluginFileEdit', ['organization' => $organization->slug, 'plugin' => $plugin->slug, 'version' => $file->slug]) }}">Edit</a>
                                </td>
                            </tr>
                            <?php endfor; ?>
                        </tbody>
                    </table>

                    {!! $files->appends(Request::except('page'))->render() !!}
                </div>
            </div>
        </div>
    </div>
@stop
