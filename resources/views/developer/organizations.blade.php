@extends('layout')

@section('page-title', 'Developer Tools - Organizations')

@section('breadcrubs')
    <li class="active">Developer Tools</li>
    <li class="active">My Organizations</li>
@stop

@if($globalUser->hasPermission('development.organization.create'))
    @section('extra-button')
        <a class="btn btn-info" href="{{ action('DeveloperController@organizationCreate') }}">Create a new Organization</a>
    @stop
@endif

@section('content')
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="panel panel-white">
                <div class="panel-body">
                    <p>Organizations with a <span class="label label-primary">Purple</span> action button is organization you can create plugins under.</p>

                    <table class="table">
                        @foreach(['thead', 'tfoot'] AS $action)
                        {!! '<' . $action . '>' !!}
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Plugins</th>
                                <th>Files</th>
                                <th>Members</th>
                                <th>Options</th>
                            </tr>
                        {!! '</' . $action . '>' !!}
                        @endforeach
                        <tbody>
                            <?php $organizationCountableIndex = 1; ?>
                            @foreach($organizations AS $org)
                            <tr>
                                <td>{{ $organizationCountableIndex++ }}</td>
                                <td>{{ $org->name }}</td>
                                <td><span class="label label-info">{{ $org->plugins->count() }}</span></td>
                                <td><span class="label label-info"><?php
                                    $int = 0;
                                    foreach ($org->plugins as $plugin) {
                                        $int += $plugin->files->count();
                                    }
                                    echo $int;
                                ?></span></td>
                                <td><span class="label label-info"><?php
                                    echo Cache::remember('organization-users.'.$org->id, 1, function () use ($org) {
                                            return Illuminate\Support\Collection::make(DB::table('users')->leftJoin('organization_users', 'id', '=', 'users_id')
                                                                        ->where('organization_users.organization_id', $org->id)->get());
                                    })->count();
                                ?></span></td>
                                <td>
                                    <div class="btn-group m-b-sm">
                                        <button type="button" class="btn btn-{{ $globalUser->hasPermission('development.plugin.create') ? 'primary' : 'default' }} dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                            Actions <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="{{ action('OrganizationController@show', ['organization' => $org->slug]) }}">View Organization</a></li>
                                            <li><a href="{{ action('DeveloperController@organizationsMembers', ['organization' => $org->slug]) }}">View Members</a></li>

                                            @if($globalUser->hasPermission('development.plugin.create'))
                                                <li class="divider"></li>
                                                <li><a href="{{ action('DeveloperController@organizationPluginCreate', ['organization' => $org->slug]) }}">Create a Plugin</a></li>
                                            @endif
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop
