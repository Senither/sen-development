@extends('layout')

@section('page-title', 'Developer Tools - Edit Files')

@section('breadcrubs')
    <li class="active">Developer Tools</li>
    <li class="active">Plugins</li>
    <li class="active">{{ $plugin->name }}</li>
    <li class="active">Files</li>
    <li class="active">{{ $version->version }}</li>
    <li class="active">Edit</li>
@stop

@section('extra-button')
    <a class="btn btn-default" href="{{ action('DeveloperController@pluginFiles', ['organization' => $plugin->organization->slug, 'plugin' => $plugin->slug]) }}">Return to files list</a>
@stop

@section('content')
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="panel panel-white">
                <div class="panel-body">

                    @include('partial.form-errors')
                    <form action="{{ action('DeveloperController@pluginFileEdit', ['organization' => $organization->slug, 'plugin' => $plugin->slug, 'version' => $version->slug]) }}" method="post">
                        {{ csrf_field() }}

                        <input type="hidden" name="plugin_id" value="{{ $plugin->id }}">
                        <input type="hidden" name="version_id" value="{{ $version->id }}">
                        <input type="hidden" name="organization_id" value="{{ $organization->id }}">

                        <div class="form-group">
                            <label for="version">Plugin file version</label>
                            <input type="text" class="form-control" name="version" value="{{ $version->version }}" disabled="true">
                        </div>

                        <div class="form-group">
                            <label for="changes">File changes</label>
                            <textarea name="changes" cols="30" rows="10" class="form-control">{{ $version->changes }}</textarea>
                        </div>

                        <button type="submit" class="btn btn-success" style="float: right;" type="button">Save File</button>
                    </form>

                </div>
            </div>
        </div>
    </div>
@stop
