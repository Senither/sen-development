@extends('layout')

@section('page-title', 'Developer Tools - Update plugin')

@section('breadcrubs')
    <li class="active">Developer Tools</li>
    <li class="active">Update Plugin</li>
    <li class="active">{{ $plugin->name }}</li>
@stop

@section('extra-button')
    <a class="btn btn-default" href="{{ action('DeveloperController@pluginFiles', ['organization' => $organization->slug, 'plugin' => $plugin->slug]) }}">Return to Files</a>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <p class="lead">Update {{ $plugin->name }}</p>
            <div class="panel panel-white">
                <div class="panel-body">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fuga molestias neque dolor ullam minima doloribus nobis. Natus quae porro aliquid perspiciatis ipsum, quo minus ut amet non corporis, debitis dignissimos!</p>

                    <hr>
                    @include('partial.form-errors')
                    <form action="{{ action('DeveloperController@pluginFileUpload', ['organization' => $organization->slug, 'plugin' => $plugin->slug]) }}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" name="plugin_id" value="{{ $plugin->id }}">
                        <input type="hidden" name="organization_id" value="{{ $organization->id }}">

                        <div class="form-group">
                            <label for="version">Plugin file version</label>
                            <input type="text" class="form-control" name="version" value="{{ Request::old('version') }}">
                        </div>

                        <div class="form-group">
                            <label for="changes">File changes</label>
                            <textarea name="changes" cols="30" rows="10" class="form-control">{{ Request::old('changes') }}</textarea>
                        </div>

                        <div class="input-group m-b-sm">
                            <label for="fileUpload">Upload a new file to {{ $plugin->name }}</label>
                            <input type="file" class="form-control" id="fileUpload" name="fileUpload">
                            <span class="input-group-btn">
                                <button type="submit" class="btn btn-info" style="margin-top: 23px" type="button">Upload file</button>
                            </span>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop
