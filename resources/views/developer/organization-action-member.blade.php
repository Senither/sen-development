@extends('layout')

@section('page-title', 'Developer Tools - Organizations')

@section('breadcrubs')
    <li class="active">Developer Tools</li>
    <li class="active">My Organizations</li>
    <li class="active">{{ $organization->name }}</li>
    <li class="active">Members</li>
    <li class="active">{{ $user->name }}</li>
    <li class="active">{{ ucfirst($action) }}</li>
@stop

@section('content')
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="panel panel-white">
                <div class="panel-body">

                    <h2 class="text-center">You're about to {{ $action }} <strong>{{ $user->name }}</strong> {{ $action == 'add' ? 'to' : 'from' }} <strong>{{ $organization->name }}</strong>.</h2>
                    <p class="text-center">Are you sure you want to continue?</p>

                    <br>

                    <center>
                        <form action="{{ action('DeveloperController@organizationsActionMember', ['organization' => $organization->slug, 'id' => $user->id, 'action' => $action]) }}" method="post">
                            {{ csrf_field() }}
                            <input type="hidden" name="form_action" value="{{ $action }}">
                            <input type="hidden" name="form_user" value="{{ $user->id }}">
                            <input type="hidden" name="form_organization" value="{{ $organization->slug }}">

                            <input class="btn btn-success" type="submit" value="Yes, Continue"> or <a class="btn btn-danger" href="{{ action('DeveloperController@organizationsMembers', ['organization' => $organization->slug]) }}">No, Go to the Members page</a>
                        </form>
                    </center>

                </div>
            </div>
        </div>
    </div>
@stop
