@extends('layout')

@section('page-title', 'Developer Tools - Plugins')

@section('breadcrubs')
    <li class="active">Developer Tools</li>
    <li class="active">My Plugins</li>
@stop

@section('content')
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="panel panel-white">
                <div class="panel-body">
                    <p>Plugins with a <span class="label label-primary">Purple</span> action button is plugins you have created, or plugins that have their availabilities status set to public.</p>

                    <table class="table">
                        @foreach(['thead', 'tfoot'] AS $action)
                        {!! '<' . $action . '>' !!}
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Version</th>
                                <th>Organization</th>
                                <th>Progress</th>
                                <th>Options</th>
                            </tr>
                        {!! '</' . $action . '>' !!}
                        @endforeach
                        <tbody>
                            <?php $usersCountableIndex = 1; ?>
                            @foreach($plugins AS $plugin)
                            <tr>
                                <td>{{ $usersCountableIndex++ }}</td>
                                <td>{{ $plugin->name }}</td>
                                <td>{{ $plugin->version }}</td>
                                <td><a class="btn btn-default" href="{{ action('OrganizationController@show', ['slug' => $plugin->organization->slug]) }}">{{ $plugin->organization->name }}</a></td>
                                <td>{!! getPluginStatusFrom($plugin) !!}</td>
                                <td>
                                    <div class="btn-group m-b-sm">
                                        <button type="button" class="btn btn-{{ $plugin->availability == 1 || $plugin->user_id == $globalUser->id ? 'primary' : 'default' }} dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                            Actions <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="{{ action('PluginController@show', ['organization' => $plugin->organization->slug, 'plugin' => $plugin->slug]) }}">View Plugin</a></li>
                                            <li><a href="{{ action('DeveloperController@pluginFiles', ['organization' => $plugin->organization->slug, 'plugin' => $plugin->slug]) }}">View Files</a></li>

                                            @if($plugin->availability == 1 || $plugin->user_id == $globalUser->id)
                                                <li class="divider"></li>
                                                <li><a href="{{ action('DeveloperController@pluginEdit', ['organization' => $plugin->organization->slug, 'plugin' => $plugin->slug]) }}">Edit Plugin</a></li>
                                                <li><a href="{{ action('DeveloperController@pluginUpdate', ['organization' => $plugin->organization->slug, 'plugin' => $plugin->slug]) }}">Add new File</a></li>
                                            @endif
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop
