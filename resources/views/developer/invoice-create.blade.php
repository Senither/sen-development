@extends('layout')

@section('page-title', 'Invoice - Create New')

@section('breadcrubs')
    <li><a href="#">Invoice</a></li>
    <li class="active">Create new</li>
@stop

@section('extra-button')
    <a class="btn btn-default" href="{{ action('DeveloperController@invoice') }}">Return to Invoices</a>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-body">

                    @include('partial.form-errors')

                    <form action="{{ action('DeveloperController@invoiceSave') }}" method="post">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label for="user">Invoice receiver</label>
                            <select class="form-control" name="user" id="user">
                                <option selected disabled>-- Select the Invoice receiver --</option>
                                @foreach($users AS $id => $user)
                                    <option value="{{ $id }}">{{ $user }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <h3>Invoice Items</h3>
                            </div>

                            <div class="col-md-6 text-right">
                                <a class="btn btn-info" id="add-new-item" style="margin-top: 22px;" href="#">Add Item</a>
                            </div>
                        </div>

                        <br>

                        <section id="items"></section>

                        <input class="btn btn-success" style="float: right;" type="submit" value="Create & Send Invoice">
                    </form>

                </div>
            </div>
        </div>
    </div>
@stop

@section('script')
    <script>
    $(document).ready(function() {
        var currentIndex = 1;
        $('#items').append(buildItem(currentIndex++));

        $('#add-new-item').click(function(event) {
            event.preventDefault();

            $('#items').append(buildItem(currentIndex++));
        });

        function buildItem(number) {
        return '<div class="row" id="item-'+ number +'">' +
                    '<div class="col-md-6">' +
                        '<div class="form-group">' +
                            '<label for="item_name_'+ number +'">Name</label>' +
                            '<input class="form-control" type="text" name="item_name_'+ number +'" placeholder="The given item name">' +
                        '</div>' +
                    '</div>' +
                    '' +
                    '<div class="col-md-3">' +
                        '<div class="form-group">' +
                            '<label for="item_quantity_'+ number +'">Quantity</label>' +
                            '<input class="form-control" type="number" min="0" name="item_quantity_'+ number +'" placeholder="The given items quantity" value="1">' +
                        '</div>' +
                    '</div>' +
                    '' +
                    '<div class="col-md-3">' +
                        '<div class="form-group">' +
                            '<label for="item_price_'+ number +'">Price</label>' +
                            '<input class="form-control" type="number" step="any" min="0" name="item_price_'+ number +'" placeholder="The given items price" value="1">' +
                        '</div>' +
                    '</div>' +
                '</div>';
    }
    });
    </script>
@stop
