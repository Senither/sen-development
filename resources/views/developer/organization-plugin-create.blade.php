@extends('layout')

@section('page-title', 'Developer Tools - Create new Plugin')

@section('breadcrubs')
    <li class="active">Developer Tools</li>
    <li class="active">My Organizations</li>
    <li class="active">{{ $organization->name }}</li>
    <li class="active">Create new Plugin</li>
@stop

@section('extra-button')
    <a class="btn btn-default" href="{{ action('DeveloperController@organizations') }}">Return to Organizations</a>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-white">
                <div class="panel-body">

                    @include('partial.form-errors')

                    <form action="{{ action('DeveloperController@organizationPluginCreate', ['organization' => $organization->slug]) }}" method="post">
                        {{ csrf_field() }}
                        <input type="hidden" name="organization_id" value="{{ $organization->id }}">

                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" name="name" value="{{ Request::old('name') }}">
                        </div>

                        <div class="form-group">
                            <label for="availability">Availability</label>
                            <select name="availability" id="availability" class="form-control">
                                <option value="0"{{ Request::old('availability', 0) == 0 ? ' selected' : null }}>Private</option>
                                <option value="1"{{ Request::old('availability', 0) != 0 ? ' selected' : null }}>Public</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="status">Plugin Status</label>
                            <select name="status" id="status" class="form-control">
                                @foreach($statuses AS $id => $status)
                                    <option value="{{ $id }}"{{ 0 == $id ? ' selected' : null }}>{{ $status }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="progress">Progress in %</label>
                            <input type="number" class="form-control" name="progress" min="0" max="100" value="{{ Request::old('progress', 0) }}">
                        </div>

                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea name="description" id="description" cols="30" rows="10" class="form-control">{{ Request::old('description') }}</textarea>
                        </div>

                        <input type="submit" class="btn btn-success" style="float: right;" value="Create Plugin">
                    </form>

                </div>
            </div>
        </div>
    </div>
@stop
