@extends('layout')

@section('page-title', 'Support - FAQ')

@section('breadcrubs')
    <li class="active">Support</li>
    <li class="active">FAQ</li>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-white">
                <div class="panel-body">
                    <h2 class="no-m m-b-lg">How can we help you?</h2>

                    <button type="button" class="btn btn-success m-b-xs" data-toggle="modal" data-target="#askQuestionModal">Ask a new question</button>
                    <!-- Modal -->
                    <div class="modal fade" id="askQuestionModal" tabindex="-1" role="dialog" aria-labelledby="askQuestionModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <form action="{{ action('SupportController@formFaq') }}" method="post">
                                    {{ csrf_field() }}

                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel">Ask a new question</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <label for="remember">Question</label>
                                            <input type="text" name="question" class="form-control" id="remember">
                                        </div>

                                        <div class="form-group">
                                            <label for="category">Category</label>
                                            <select name="category" class="form-control">
                                                <option selected disabled> -- Select a Category --</option>
                                                @foreach($categories AS $category)
                                                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-success">Send question</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- ./Modal -->
                    <a href="{{ action('SupportController@contact') }}" class="btn btn-success m-b-xs">Contact us</a>

                    @include('partial.form-errors')

                    <h2 class="m-t-xxl m-b-lg">Recent Questions</h2>
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        @foreach($latest AS $faq)
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="{{ str_slug($faq->question) }}">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#{{ $faq->id }}" aria-expanded="false">
                                        {{ $faq->question }}
                                    </a>
                                </h4>
                            </div>
                            <div id="{{ $faq->id }}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="{{ str_slug($faq->question) }}">
                                <div class="panel-body">
                                    <p>{!! $faq->awnser !!}</p>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    <h2 class="m-t-xxl m-b-lg">All questions</h2>

                    @foreach($categories AS $category)
                    <div class="row">
                        <div class="col-md-12"><h3 id="{{ str_slug($category->name) }}">{{ $category->name }}</h3><hr></div>

                        @if(!$category->questions->isEmpty())
                            @foreach(array_chunk_fixed($category->questions->toArray(), 2) AS $row)
                                <div class="col-md-6">
                                    <ul class="faq-popular list-unstyled">

                                        @foreach($row AS $faq)
                                            <li><a href="#" data-toggle="modal" data-target="#{{ str_slug($faq['question']) }}">{{ $faq['question'] }}</a></li>
                                        @endforeach

                                        @foreach($row AS $faq)
                                        <div class="modal fade" id="{{ str_slug($faq['question']) }}" tabindex="1" role="dialog" aria-labelledby="{{ str_slug($faq['question']) }}" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                        <h4 class="modal-title" id="myModalLabel">{{ $faq['question'] }}</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <p>{{ nl2br($faq['awnser']) }}</p>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach

                                    </ul>
                                </div>
                            @endforeach
                        @else
                            <p><strong class="text-default">There are currently no questions for this category..</strong></p>
                        @endif
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div><!-- Row -->
@stop
