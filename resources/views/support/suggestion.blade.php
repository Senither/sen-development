@extends('layout')

@section('page-title', 'Support - Suggestion')

@section('breadcrubs')
    <li class="active">Support</li>
    <li class="active">Suggestion</li>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-body">
                    <h2 class="no-m m-b-lg">Got some suggestions?</h2>
                    <p>Got anything you would like to added to the control panel? Any ideas, improvements, updates or additions? Great! You can use the form below to send them in.</p>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-body">

                    @include('partial.form-errors')

                    <form action="{{ action('SupportController@formSuggestion') }}" method="post">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label for="category_id">Category</label>
                            <select class="form-control" name="category_id">
                                <option selected disabled> --  Select a suggestion category -- </option>
                                @foreach($categories AS $suggestion)
                                    <option value="{{ $suggestion->id }}"{{ Request::old('category_id') == $suggestion->id ? ' selected' : null }}>{{ $suggestion->name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="suggestion">Suggestion</label>
                            <textarea class="form-control" style="min-height: 260px;" name="suggestion">{{ Request::old('suggestion') }}</textarea>
                        </div>

                        <input style="float: right;" class="btn btn-info" type="submit" value="Send suggestion">
                    </form>

                </div>
            </div>
        </div>
    </div>
@stop
