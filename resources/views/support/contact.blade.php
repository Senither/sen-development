@extends('layout')

@section('page-title', 'Support - Contact')

@section('breadcrubs')
    <li class="active">Support</li>
    <li class="active">Contact</li>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-body">
                    <h2 class="no-m m-b-lg">Send us an email</h2>
                    <p>If you have any issues, got any questions or just wanna talk, feel free to use the form below; If you have a suggestion, please use the <a href="{{ action('SupportController@suggestion') }}">suggestion form</a>. </p>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-body">

                    @include('partial.form-errors')

                    <form action="{{ action('SupportController@formContact') }}" method="post">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label for="subject">Subject</label>
                            <input class="form-control" type="text" name="subject" placeholder="Your subject.." value="{{ Request::old('subject') }}" autofocus>
                        </div>

                        <div class="form-group">
                            <label for="category_id">Category</label>
                            <select class="form-control" name="category_id">
                                <option selected disabled> --  Select a contact category -- </option>
                                @foreach(\App\Models\ContactCategory::all() AS $contact)
                                    <option value="{{ $contact->id }}"{{ Request::old('category_id') == $contact->id ? ' selected' : null }}>{{ $contact->name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="message">Message</label>
                            <textarea class="form-control" style="min-height: 260px;" name="message" placeholder="Your message to us..">{{ Request::old('message') }}</textarea>
                        </div>

                        <input style="float: right;" class="btn btn-info" type="submit" value="Send Email">
                    </form>

                </div>
            </div>
        </div>
    </div>
@stop
