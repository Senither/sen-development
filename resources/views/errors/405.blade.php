@extends('simple-layout')

@section('page-title', '405 - Method not allowed')
@section('body-class', 'page-error')

@section('content')
    <div class="col-md-4 center">
        <h1 class="text-xxl text-primary text-center">405 <small><small>Method not allowed</small></small></h1>
        <div class="details">
            <h3>Oops ! Looks like you're trying to view something you shouldn't.</h3>

            <p>Try returning back to the <a href="{{ action('DashboardController@index') }}">Dashboard</a> or <a href="{{ route('home') }}">Home Page</a>.</p>
        </div>
    </div>
@stop
