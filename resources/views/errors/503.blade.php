@extends('simple-layout')

@section('page-title', '503 - Maintenance mode')
@section('body-class', 'page-error')

@section('content')
    <div class="col-md-4 center">
        <h1 class="text-xxl text-primary text-center">Maintenance</h1>
        <div class="details">
            <h3>We're working on some upgrades at the moment.</h3>
            <p>We should be back online as soon as we got our coffee.</p>

            <div class="bg-slategray lt wrap-reset mt-40 text-center">
                <a href="{{ URL::full() }}"><button class="btn btn-default btn-sm b-0"><i class="fa fa-refresh"></i> Try again</button></a>
                <a href="mailto:support@sen-dev.com"><button class="btn btn-lightred btn-sm b-0"><i class="fa fa-envelope-o"></i> Contact support</button></a>
            </div>
        </div>
    </div>
@stop
