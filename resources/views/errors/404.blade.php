@extends('simple-layout')

@section('page-title', '404 - Not found')
@section('body-class', 'page-error')

@section('content')
    <div class="col-md-4 center">
        <h1 class="text-xxl text-primary text-center">404 <small>Not found</small></h1>
        <div class="details">
            <h3>Oops ! Something went wrong</h3>
            <p>We can't find the page you're looking for, maybe the page took a walk.</p>

            <p>You might be able to find the page from the <a href="{{ action('DashboardController@index') }}">Dashboard</a>.
            <br>If you think this is a mistake you can contact us about it at our <a href="{{ action('SupportController@contact') }}">Support Center</a></p>
        </div>
    </div>
@stop
