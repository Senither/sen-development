@extends('simple-layout')

@section('page-title', '403 - Forbidden')
@section('body-class', 'page-error')

@section('content')
    <div class="col-md-4 center">
        <h1 class="text-xxl text-primary text-center">403 <small>Forbidden</small></h1>
        <div class="details">
            <h3>You don't seem to have access to the page you just requested :(</h3>
            <p>If you think this is error, you can contact us at our support center.</p>

            <p>Return to the <a href="{{ action('DashboardController@index') }}">Dashboard</a>.</p>
        </div>
    </div>
@stop
