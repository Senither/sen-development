@extends('simple-layout')

@section('page-title', '500 - Internal Server Error')
@section('body-class', 'page-error')

@section('content')
    <div class="col-md-4 center">
        <h1 class="text-xxl text-primary text-center">500 <small>Server Error</small></h1>
        <div class="details">
            <h3>Whoops, a real big error here!</h3>
            <p>Looks like something broke on our end.. Maybe trying again will fix it?</p>

            <div class="bg-slategray lt wrap-reset mt-40 text-center">
                <a href="{{ URL::full() }}"><button class="btn btn-default btn-sm b-0"><i class="fa fa-refresh"></i> Try again</button></a>
                <a href="{{ action('DashboardController@index') }}"><button class="btn btn-greensea btn-sm b-0"><i class="fa fa-dashboard"></i> Return to dashboard</button></a>
                <a href="mailto:support@sen-dev.com"><button class="btn btn-lightred btn-sm b-0"><i class="fa fa-envelope-o"></i> Contact support</button></a>
            </div>
        </div>
    </div>
@stop
