
<!DOCTYPE html>
<html>
    <head>

        <!-- Title -->
        <title>SenDevelopment | Home</title>

        @include('partial.meta')

        <!-- Styles -->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Raleway:500,400,300' rel='stylesheet' type='text/css'>
        <link href="{{ asset('assets/plugins/pace-master/themes/black/pace-theme-flash.css') }}" rel="stylesheet"/>
        <link href="{{ asset('assets/plugins/uniform/css/uniform.default.min.css') }}" rel="stylesheet"/>
        <link href="{{ asset('assets/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('assets/plugins/fontawesome/css/font-awesome.css') }}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('assets/plugins/animate/animate.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('assets/plugins/tabstylesinspiration/css/tabs.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('assets/plugins/tabstylesinspiration/css/tabstyles.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('assets/plugins/pricing-tables/css/style.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('assets/css/landing.css') }}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('assets/css/custom-landing.css') }}" rel="stylesheet" type="text/css"/>

        <script src="{{ asset('assets/plugins/pricing-tables/js/modernizr.js') }}"></script>

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>
    <body data-spy="scroll" data-target="#header">
        <nav id="header" class="navbar navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="fa fa-bars"></span>
                    </button>
                    <a class="navbar-brand" href="#">Sen<span>Development</span></a>
                </div>
                <div id="navbar" class="navbar-collapse collapse navbar-right">
                    <ul class="nav navbar-nav">
                        <li><a href="#home">Home</a></li>
                        <li><a href="#section-2">About us</a></li>
                        <li><a href="#section-3">Contact</a></li>
                        <li><a href="{{ Auth::check() ? action('DashboardController@index') : action('UserController@login') }}">Client Area</a></li>
                    </ul>
                </div>
            </div>
        </nav>

        <div class="home" id="home">
            <div class="overlay"></div>
            <div class="container">
                <div class="row">
                    <div class="home-text col-md-8">
                        <h1 class="wow fadeInDown" data-wow-delay="0.5s" data-wow-duration="1.5s" data-wow-offset="10">Sen Development</h1>
                        <p class="lead wow fadeInDown" data-wow-delay="1.0s" data-wow-duration="1.5s" data-wow-offset="10">A simple solution for all of your plugin needs.
                        <br>We are a fast and solid plugin development team that you can rely on.</p>

                        <a href="#section-3" target="_blank" class="btn btn-default btn-rounded btn-lg wow fadeInLeft" data-wow-delay="1.3s" data-wow-duration="1.5s" data-wow-offset="10">Contact Us</a>
                        <a href="{{ Auth::check() ? action('DashboardController@index') : action('UserController@login') }}" class="btn btn-success btn-rounded btn-lg wow fadeInRight" data-wow-delay="1.3s" data-wow-duration="1.5s" data-wow-offset="10">Client Area</a>
                    </div>
                    <div class="scroller">
                        <div class="mouse"><div class="wheel"></div></div>
                    </div>
                </div>
            </div>
        </div>

        <section id="section-2">
            <div class="container">
                <div class="row">
                    <div class="col-sm-8 wow fadeInLeft" data-wow-delay="0.5s" data-wow-duration="1.5s" data-wow-offset="10">
                        <h2>About us</h2>
                        <p>Minecraft servers has changed. We're no longer running around a survival server, on a lan with our friends cutting down trees, and then calling it a day. No, things have changed a little.. Okay - they've changed dramatically!</p>
                        <p>With the release of Bukkit and Spigot, minecraft servers have turned their world upside-down.</p>
                        <p>With Sen Development, not only will you get what you're paying for, but also be sure that the project is using the latests technologies, and are updated to keep up with the latest features, on top of that, our <a href="{{ Auth::check() ? action('DashboardController@index') : action('UserController@login') }}">Control Panel</a> gives you access to your plugin, and any version of the plugins at any time!</p>
                    </div>
                    <div class="col-sm-4 wow fadeInRight" data-wow-delay="0.5s" data-wow-duration="2.0s" data-wow-offset="10">
                        <img src="assets/images/cube.png" class="cube-img" alt="">
                    </div>
                </div>
            </div>
        </section>

        <section id="section-3">
            <div class="overlay"></div>
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-sm-offset-3 wow fadeInUp" data-wow-duration="1.5s" data-wow-offset="10" data-wow-delay="0.5s">
                        <div id="form-container">
                            <a href="#contact" class="btn btn-success btn-lg btn-rounded contact-button"><i class="fa fa-envelope-o"></i></a>
                            <h2>Wanna have a chat?</h2>
                            <p>If you have any questions, concerns, got a proposal for a new project or just wanna chat, feel free to send us a message.</p>
                            <form id="contat-form" class="m-t-md">
                                <div id="form-errors"></div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <input type="text" id="name" class="form-control input-lg contact-name" placeholder="Name" value="{{ Request::old('name') }}" required>
                                        </div>
                                        <div class="col-sm-6">
                                            <input type="email" id="email" class="form-control input-lg" placeholder="Email" value="{{ Request::old('email') }}" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <input type="text" id="subject" class="form-control input-lg" placeholder="Subject" value="{{ Request::old('subject') }}" required>
                                </div>
                                <div class="form-group">
                                    <textarea id="message" class="form-control" rows="4=6" placeholder="Message" required>{{ Request::old('message') }}</textarea>
                                </div>
                                <a id="form-submit" class="btn btn-default btn-lg">Send Message</a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <footer>
            <div class="container">
                <p class="text-center no-s">2015 &copy; SenDevelopment by Alexis Tan.</p>
            </div>
        </footer>

        <!-- Javascripts -->
        <script src="{{ asset('assets/plugins/jquery/jquery-2.1.4.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/pace-master/pace.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/uniform/jquery.uniform.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/wow/wow.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/tabstylesinspiration/js/cbpfwtabs.js') }}"></script>
        <script src="{{ asset('assets/plugins/pricing-tables/js/main.js') }}"></script>
        <script src="{{ asset('assets/js/landing.js') }}"></script>

        @include('partial.flash')

    </body>
</html>
