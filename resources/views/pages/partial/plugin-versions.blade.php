<div class="col-md-4">
    <p class="lead">Versions</p>
    <div class="panel">
        <div class="panel-body">

            @if($plugin->files->isEmpty())
                <p>There are currently no versions ready to download for this plugin.</p>
            @else
            <table class="table">
                <thead>
                    <tr>
                        <th>Version</th>
                        <th>Release date</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $index = $files->currentPage() * $files->perPage(); ?>
                    <?php for ($i = (($files->currentPage() - 1) * $files->perPage()); $i < ($files->currentPage() * $files->perPage()); ++$i): ?>
                    <?php if ($files->count() <= $i) {
    break;
} ?>
                    <?php $file = $files->items()[$i]; ?>
                        <tr class="{{ $file->slug == $version->slug ? 'active' : null }}">
                            <td><a href="{{ action('PluginController@showVersion', ['organization' => $plugin->organization->slug, 'plugin' => $plugin->slug, 'version' => str_slug($file->version)]) }}?page={{ $files->currentPage() }}">{{ $file->version }}</a></td>
                            <td><small>{{ generateTimestamp($file->created_at) }}</small></td>
                        </tr>
                    <?php endfor; ?>
                </tbody>

            </table>

            {!! $files->appends(Request::except('page'))->render() !!}
            @endif
        </div>
    </div>
</div>
