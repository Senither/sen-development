@extends('layout')

@section('page-title', $plugin->organization->name . ' - ' . $plugin->name)

@section('breadcrubs')
    <li><a href="{{ action('OrganizationController@show', ['id' => $plugin->organization->slug]) }}">{{ $plugin->organization->name }}</a></li>
    <li class="active">{{ $plugin->name }}</li>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <p class="lead">Users who have access to this plugin</p>
            <div class="panel">
                <div class="panel-body">
                    @foreach($plugin->organization->users AS $user)
                        <a href="#" data-toggle="modal" data-target="#modal">
                            <img class="img-circle" src="{{ avatar_image($user, 52) }}" width="52" alt="{{ $user->name }} profile image" data-toggle="tooltip" title="{{ $user->name }} ({{ $user->group->name }})">
                        </a>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        @include('pages.partial.plugin-versions')

       <div class="col-md-8">
           <p class="lead">Plugin Information <small>(Version: {{ $version->version }})</small></p>
           <div class="panel">
               <div class="panel-body">
                   <p><strong>Plugin Description</strong></p>
                   <p>{!! createCodeStructure($plugin->description) !!}</p>

                   <hr>

                    <p class="text-center">
                        <a class="text-right btn btn-info btn-addon" href="{{ action('PluginController@download', ['organization' => $plugin->organization->slug, 'plugin' => $plugin->slug, 'version' => $version->slug]) }}">
                          <i class="fa fa-cloud-download"></i>Download {{ str_replace(' ', '', $plugin->name) . '-' . str_replace(' ', '-', $version->version) . '.jar' }}
                        </a>
                    </p>

                    <hr>

                    <p><strong>{{ $version->version }} Changes:</strong></p>
                    <p><small>Changes was posted {{ $version->created_at->diffForHumans() }}.</small></p>
                    <p>{{ $version->changes }}</p>
               </div>
           </div>
       </div>
    </div>
@stop
