@extends('layout')

@section('page-title', $plugin->organization->name . ' - ' . $plugin->name)

@section('breadcrubs')
    <li><a href="{{ action('OrganizationController@show', ['id' => $plugin->organization->slug]) }}">{{ $plugin->organization->name }}</a></li>
    <li class="active">{{ $plugin->name }}</li>
@stop

@if($globalUser->hasPermission('development.see') && ($plugin->availability == 1 || $plugin->user_id == $globalUser->id))
    @section('extra-button')
        <a class="btn btn-default" href="{{ action('DeveloperController@pluginEdit', ['organization' => $plugin->organization->slug, 'plugin' => $plugin->slug]) }}">Manage Plugin</a>
    @stop
@endif

@section('content')
    <div class="row">
        <div class="col-md-12">
            <p style="float: left;" class="lead">Users who have access to this plugin</p>
            <p style="float: right;">Status: {!! getPluginStatusFrom($plugin) !!}</p>
            <p class="clearfix"></p>

            <div class="panel">
                <div class="panel-body">
                    @foreach($plugin->organization->users AS $user)
                        <a href="#" data-toggle="modal" data-target="#modal">
                            <img class="img-circle" src="{{ avatar_image($user, 52) }}" width="52" alt="{{ $user->name }} profile image" data-toggle="tooltip" title="{{ $user->name }} ({{ $user->group->name }})">
                        </a>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        @include('pages.partial.plugin-versions')

       <div class="col-md-8">
           <p class="lead">Plugin Information <small>(Version: Latest)</small></p>
           <div class="panel">
               <div class="panel-body">
                   <p><strong>Plugin Description</strong></p>
                   <p>{!! createCodeStructure($plugin->description) !!}</p>

                    @if($version != null)
                     <hr>

                      <p class="text-center">
                          <a class="text-right btn btn-info btn-addon" href="{{ action('PluginController@download', ['organization' => $plugin->organization->slug, 'plugin' => $plugin->slug, 'version' => 'latest']) }}">
                            <i class="fa fa-cloud-download"></i>Download Latest Version
                          </a>
                      </p>

                      <hr>

                      <p><strong>Latest Changes <small>({{ $version->version }})</small>:</strong></p>
                      <p><small>Changes was posted {{ $version->created_at->diffForHumans() }}.</small></p>
                      <p>{{ $version->changes }}</p>
                    @endif

                    @if($plugin->isUnderProgress)
                      @if($plugin->progress < 100)
                        @if($plugin->progress == 0)
                          <p><strong>Plugin progress so far..</strong></p>
                          <div class="progress-bar progress-bar-default" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
                            No progress so far D:
                          </div>
                        @else
                          <?php
                            $progressBarColour = 'danger';
                            if ($plugin->progress < 15) {
                                $progressBarColour = 'danger';
                            } elseif ($plugin->progress < 35) {
                                $progressBarColour = 'warning';
                            } elseif ($plugin->progress < 95) {
                                $progressBarColour = 'info';
                            } else {
                                $progressBarColour = 'success';
                            }
                          ?>
                          <p><strong>Plugin progress so far..</strong></p>
                          <div class="progress-bar progress-bar-{{ $progressBarColour }}" role="progressbar" aria-valuenow="{{ $plugin->progress }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $plugin->progress }}%;">
                            {{ $plugin->progress }}%
                          </div>
                        @endif
                      @endif
                    @endif
               </div>
           </div>
       </div>
    </div>
@stop
