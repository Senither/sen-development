@extends('layout')

@section('page-title', 'Dashboard')

@section('breadcrubs')
    <li class="active">Dashboard</li>
@stop

@section('content')
    <div class="row">
        <div class="col-md-6">

            <div class="row">
                <div class="col-md-12">
                    <p class="lead">Server Stats</p>
                    <div class="panel panel-white">
                        <div class="panel-body">
                            <p class="text-center">Join at <span class="label label-info">mc.sen-dev.com</span></p>

                            <h2 class="text-center">The development server is currently... <br><br>
                            @if($server != null && $server->max != -1)
                                <span class="label label-success">Online</span>
                            @else
                                <span class="label label-danger">Offline</span>
                            @endif
                            </h2>

                            @if($globalUser->minecraft == null)
                                <br>
                                <p><span class="text-danger"><strong>Notice:</strong></span> You're not whitelisted on the development server! To get whitelisted, head over to the <a href="{{ action('UserController@settings') }}">My Settings</a> page and add your Minecraft usernamne, this will automatically whitelist you on the development server, however it should be noted that it may take a few minutes before it takes affect.</p>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <p class="lead">Quick Access: &nbsp; <small>My Plugins</small></p>
                    <div class="panel panel-white">
                        <div class="panel-body">
                            <table class="table">
                                @foreach(['thead', 'tfoot'] AS $action)
                                {!! '<' . $action . '>' !!}
                                    <tr>
                                        <th>Name</th>
                                        <th>Status</th>
                                        <th>Options</th>
                                    </tr>
                                {!! '</' . $action . '>' !!}
                                @endforeach
                                <tbody>
                                    @foreach($globalUser->organizations AS $organization)
                                        @foreach($organization->plugins AS $plugin)
                                        <tr>
                                            <td>{{ $plugin->name }}</td>
                                            <td>{!! getPluginStatusFrom($plugin) !!}</td>
                                            <td>
                                                <a class="btn btn-default" href="{{ action('PluginController@show', ['organization' => $organization->slug, 'plguin' => $plugin->slug]) }}">View</a>
                                            </td>
                                        </tr>
                                        @endforeach
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="col-md-6">
            <p class="lead">News Feed</p>

            <section id="cd-timeline" class="cd-container">
                @foreach($news AS $article)
                <div class="cd-timeline-block">
                    <div class="cd-timeline-img cd-info">
                        <i class="fa fa-tag"></i>
                    </div>

                    <div class="cd-timeline-content">
                        <h2><a href="{{ action('NewsController@show', ['id' => $article->id, 'slug' => str_slug($article->title)]) }}">{{ $article->title }}</a></h2>
                        <p>{{ $article->description }}</p>
                        <span class="cd-date">{{ generateTimestamp($article->created_at) }} | <a href="{{ action('NewsController@show', ['id' => $article->id, 'slug' => str_slug($article->title)]) }}">Read full update</a></span>
                    </div>
                </div>
                @endforeach

                 <div class="cd-timeline-block">
                    <p><a class="btn btn-info" style="float: right; margin-right: 15px;" href="{{ action('NewsController@index') }}">View older updates..</a></p>
                </div>
            </section>

        </div>
    </div>
@stop
