@extends('layout')

@section('page-title', $organization->name)

@section('breadcrubs')
    <li class="active">{{ $organization->name }}</li>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <p class="lead">Users who are a part of this Organization</p>
            <div class="panel">
                <div class="panel-body">
                    @foreach($organization->users AS $user)
                        <a href="#" data-toggle="modal" data-target="#modal">
                            <img class="img-circle" src="{{ avatar_image($user, 52) }}" width="52" alt="{{ $user->name }} profile image" data-toggle="tooltip" title="{{ $user->name }} ({{ $user->group->name }})">
                        </a>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <p class="lead">Plugins associated with this Organization</p>
            <div class="panel">
                <div class="panel-body">

                    @if(!$organization->plugins->isEmpty())
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Latest Version</th>
                                    <th>Status</th>
                                    <th>Option</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $index = $plugins->currentPage() * $plugins->perPage(); ?>
                                <?php for ($i = (($plugins->currentPage() - 1) * $plugins->perPage()); $i < ($plugins->currentPage() * $plugins->perPage()); ++$i): ?>
                                <?php if ($plugins->count() <= $i) {
    break;
} ?>
                                <?php $plugin = $plugins->items()[$i]; ?>
                                <tr>
                                    <td>{{ $index++ }}</td>
                                    <td>{{ $plugin->name }}</td>
                                    <td>{{ $plugin->version }}</td>
                                    <td>{!! getPluginStatusFrom($plugin) !!}</td>
                                    <td>
                                        <a class="btn btn-default btn-addon" href="{{ action('PluginController@show', ['organization' => $organization->slug, 'plugin' => $plugin->slug]) }}"><i class="fa fa-files-o"></i> View Plugin</a>
                                    </td>
                                </tr>
                                <?php endfor; ?>
                            </tbody>
                        </table>

                        {!! $plugins->render() !!}
                    @else
                        <p>There a currently no plugins assinged to this organization.</p>
                    @endif
                </div>
            </div>
        </div>
    </div>
@stop
