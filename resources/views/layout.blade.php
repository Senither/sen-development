<!DOCTYPE html>
<html>
    <head>

        <!-- Title -->
        <title>SenDev | @yield('page-title')</title>

        @include('partial.meta')

        <!-- Styles -->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600' rel='stylesheet' type='text/css'>
        <link href="{{ asset('assets/plugins/pace-master/themes/blue/pace-theme-flash.css') }}" rel="stylesheet"/>
        <link href="{{ asset('assets/plugins/uniform/css/uniform.default.min.css') }}" rel="stylesheet"/>
        <link href="{{ asset('assets/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('assets/plugins/fontawesome/css/font-awesome.css') }}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('assets/plugins/line-icons/simple-line-icons.css') }}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('assets/plugins/offcanvasmenueffects/css/menu_cornerbox.css') }}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('assets/plugins/waves/waves.min.css') }}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('assets/plugins/switchery/switchery.min.css') }}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('assets/plugins/3d-bold-navigation/css/style.css') }}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('assets/plugins/slidepushmenus/css/component.css') }}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('assets/plugins/metrojs/MetroJs.min.css') }}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('assets/plugins/toastr/toastr.min.css') }}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('assets/plugins/prism/prism.css') }}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('assets/plugins/vertical-timeline/css/style.css') }}" rel="stylesheet" type="text/css">

        <!-- Theme Styles -->
        <link href="{{ asset('assets/css/modern.min.css') }}" rel="stylesheet" type="text/css"/>
        <link href="<?php echo asset("assets/css/themes/{$globalUser->theme->theme}.css"); ?>" class="theme-color" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('assets/css/custom.css') }}" rel="stylesheet" type="text/css"/>

        <script src="{{ asset('assets/plugins/3d-bold-navigation/js/modernizr.js') }}"></script>
        <script src="{{ asset('assets/plugins/offcanvasmenueffects/js/snap.svg-min.js') }}"></script>

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>
    <body class="page-header-fixed">
        <div class="overlay"></div>

        <main class="page-content content-wrap container">
            @include('partial.menu')
            <div class="page-inner">
                <div class="page-title">
                    <h3>@yield('page-title')</h3>
                    <div class="page-breadcrumb">
                        <ol class="breadcrumb" style="float: left;">
                            <li><a href="{{ action('DashboardController@index') }}">Home</a></li>
                            @yield('breadcrubs')
                        </ol>
                    </div>
                    <p class="extra-button">
                        @yield('extra-button')
                    </p>
                </div>
                <div id="main-wrapper">
                    @yield('content')
                </div><!-- Main Wrapper -->
                <div class="page-footer">
                    <p class="no-s" style="float: left;">2015 &copy; SenDevelopment by Alexis Tan.</p>
                    <p class="no-s" style="float: right;">Powered by <a href="http://laravel.com/">Laravel</a>, <a href="http://getbootstrap.com/">Bootstrap</a> & <a href="https://www.digitalocean.com/?refcode=9f589c4101c3">Digital Ocean</a>.</p>
                </div>
            </div><!-- Page Inner -->
        </main><!-- Page Content -->

        <!-- Javascripts -->
        <script src="{{ asset('assets/plugins/jquery/jquery-2.1.4.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/pace-master/pace.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/jquery-blockui/jquery.blockui.js') }}"></script>
        <script src="{{ asset('assets/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/switchery/switchery.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/uniform/jquery.uniform.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/waves/waves.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/waypoints/jquery.waypoints.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/jquery-counterup/jquery.counterup.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/metrojs/MetroJs.min.js') }}"></script>
        <script src="{{ asset('assets/js/modern.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/prism/prism.js') }}"></script>

        @yield('script')

        @include('partial.flash')
    </body>
</html>
