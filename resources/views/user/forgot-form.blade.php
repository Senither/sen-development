@extends('simple-layout')

@section('page-title', 'Reset password')
@section('body-class', 'page-register')

@section('content')
    <div class="row">
        <div class="col-md-6 center">
            <div class="login-box panel panel-white">
                <div class="panel-body">
                    <div class="col-md-12 center">
                        <div class="login-box">
                            <a href="{{ action('DashboardController@index') }}" class="logo-name text-lg text-center"><span>SEN</span>Development</a>
                            <p class="text-center m-t-md">Reset your password</p>

                            @include('partial.form-errors')

                            <form method="post" action="{{ action('UserController@saveForgot', ['token' => $token]) }}" class="m-t-md">
                                {{ csrf_field() }}
                                <input type="hidden" name="name" value="{{ $user->name }}">
                                <input type="hidden" name="email" value="{{ $user->email }}">

                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Name" value="{{ $user->name }}" disabled>
                                </div>
                                <div class="form-group">
                                    <input type="email" class="form-control" placeholder="Email" value="{{ $user->email }}" disabled>
                                </div>
                                <div class="form-group">
                                    <input type="password" name="password" class="form-control" placeholder="Password" required>
                                </div>

                                <div class="form-group">
                                    <input type="password" name="password_again" class="form-control" placeholder="Password again" required>
                                </div>

                                <button type="submit" class="btn btn-success btn-block m-t-xs">Change my Password</button>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
            <p class="text-center m-t-xs text-sm">2015 &copy; SENDevelopment by Alexis Tan.</p>
        </div>
    </div><!-- Row -->
@stop


