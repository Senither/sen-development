@extends('simple-layout')

@section('page-title', 'Login')
@section('body-class', 'page-login')

@section('content')
    <div class="row">
        <div class="col-md-3 center">
            <div class="login-box">
                <a href="{{ action('DashboardController@index') }}" class="logo-name text-lg text-center"><span>SEN</span>Development</a>
                <p class="text-center m-t-md">Please login into your account.</p>

                @include('partial.form-errors')

                @if(session()->has('success_message'))
                    <p class="alert alert-success">
                        {{ session()->get('success_message') }}
                    </p>
                @endif

                <form method="post" action="{{ action('UserController@login') }}">
                    {{ csrf_field() }}

                    <div class="form-group">
                        <input type="email" name="email" class="form-control" placeholder="Email" value="{{ Request::old('email') }}" required autofocus>
                    </div>
                    <div class="form-group">
                        <input type="password" name="password" class="form-control" placeholder="Password" required>
                    </div>
                    <div class="form-group">
                        <label for="remember">Remember me </label>
                        <input type="checkbox" name="remember" class="form-control" id="remember">
                    </div>
                    <button type="submit" class="btn btn-success btn-block">Login</button>
                    <a href="{{ action('UserController@forgot') }}" class="display-block text-center m-t-md text-sm">Forgot Password?</a>
                    <a href="{{ route('home') }}" class="btn btn-default btn-block m-t-md">Return to the Home Page</a>
                </form>
                <p class="text-center m-t-xs text-sm">2015 &copy; SenDevelopment by Alexis Tan.</p>
            </div>
        </div>
    </div><!-- Row -->
    <!-- <p class="login-info">Welcome to SenDevelopment's plugin management system, once logged in you'll be able to easily manage and track projects, as-well as download any version of any project you have hired us for. -->
    <!-- <br><br>If you're a new customer you should have an account activation link in your e-mail, once your account is activated you'll be able to login and use the management system.</p> -->
@stop


