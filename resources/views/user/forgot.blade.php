@extends('simple-layout')

@section('page-title', 'Password reset')
@section('body-class', 'page-forgot')

@section('content')
    <div class="row">
        <div class="col-md-3 center">
            <div class="login-box panel panel-white">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <a href="{{ action('DashboardController@index') }}" class="logo-name text-lg text-center"><span>SEN</span>Development</a>
                            <p class="text-center m-t-md">Enter your e-mail address below to reset your password</p>

                            @include('partial.form-errors')

                            @if(session()->has('success_message'))
                                <p class="alert alert-success">
                                    {{ session()->get('success_message') }}
                                </p>
                            @else

                            <form method="post" action="{{ action('UserController@forgot') }}" class="m-t-md">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <input type="email" name="email" class="form-control" placeholder="Email" required>
                                </div>
                                <button type="submit" class="btn btn-success btn-block">Submit</button>
                                <a href="{{ action('UserController@login') }}" class="btn btn-default btn-block m-t-md">Back</a>
                            </form>

                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <p class="text-center m-t-xs text-sm">2015 &copy; SENDevelopment by Alexis Tan.</p>
        </div>
    </div><!-- Row -->
@stop


