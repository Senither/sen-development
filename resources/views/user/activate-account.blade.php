@extends('simple-layout')

@section('page-title', 'Password reset')
@section('body-class', 'page-forgot')

@section('content')
    <div class="row">
        <div class="col-md-3 center">
            <div class="login-box panel panel-white">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <a href="{{ action('DashboardController@index') }}" class="logo-name text-lg text-center"><span>SEN</span>Development</a>
                            <p class="text-center m-t-md">Welcome, {{ $user->name }}!
                            <br>Your account email is <i>{{ $user->email }}</i>.</p>

                            <p class="text-center m-t-md">Below you will be able to setup some of your account settings, once you have done that you'll be able to use all of our awesome features.</p>

                            @include('partial.form-errors')

                            @if(session()->has('success_message'))
                                <p class="alert alert-success">
                                    {{ session()->get('success_message') }}
                                </p>
                            @else

                            <form method="post" action="{{ action('UserController@activate', ['id' => $token]) }}" class="m-t-md">
                                {{ csrf_field() }}
                                <input type="hidden" name="token_id" value="{{ $token }}">

                                <div class="form-group">
                                    <label for="name"><strong>Name</strong></label>
                                    <input type="text" name="name" class="form-control" value="{{ $user->name }}" placeholder="Your name">
                                </div>

                                <div class="form-group">
                                    <label for="password"><strong>Password</strong></label>
                                    <input type="password" name="password" class="form-control" placeholder="Your password">
                                </div>

                                <div class="form-group">
                                    <label for="password_again"><strong>Password Again</strong></label>
                                    <input type="password" name="password_again" class="form-control" placeholder="Your password again">
                                </div>

                                <button type="submit" class="btn btn-success btn-block">Activate Account & Login</button>
                            </form>

                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <p class="text-center m-t-xs text-sm">2015 &copy; SENDevelopment by Alexis Tan.</p>
        </div>
    </div><!-- Row -->
@stop


