@extends('layout')

@section('page-title', 'User Settings')

@section('breadcrubs')
    <li class="active">User Settings</li>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="panel">
                <div class="panel-body">

                    <div class="row">
                        <div class="col-md-12">

                            <div class="row">
                                <div class="col-md-12">
                                    <h3>Profile picture</h3>
                                    <p><strong>Profile picture</strong></p>
                                    <img style="float: left; margin-right: 10px;" class="img-circle" src="{{ avatar_image($globalUser, 70) }}" height="70" alt="Profile picture">
                                    <p>Profile pictures are loaded from <a target="blank" href="http://www.gravatar.com/">Gravatar.com</a>.
                                    <br>If you already have a Gravatar account, you'll be able to change your profile picture from the <a target="blank" href="https://en.gravatar.com/emails/">My Gravatars</a> page.
                                    Don't have an account? Signup at <a target="blank" href="http://www.gravatar.com/">Gravatar.com</a> with your email, upload a profile picture and your image should be updated automatically!</p>

                                    <div style="clear: both;"></div>

                                    <p>If you want to change your email address, you can contact our via our <a href="{{ action('SupportController@contact') }}">support center</a> and we will help you through the process.</p>
                                </div>
                            </div>

                        </div>

                        <div class="col-md-12">
                            <br>

                            @include('partial.form-errors')

                            <form action="{{ action('UserController@settings') }}" method="POST">
                                {{ csrf_field() }}

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="name">Your name</label>
                                            <input class="form-control" type="text" name="name" placeholder="Your name.." value="{{ $globalUser->name }}">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="theme">Site theme</label>
                                            <select class="form-control" name="theme">
                                                @foreach($themes AS $theme)
                                                    <option value="{{ $theme->id }}"{{ $theme->id == $globalUser->theme->id ? ' selected' : null }}>{{ $theme->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <h3>Minecraft Settings</h3><br>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="minecraft">Your minecraft name</label>
                                            <input class="form-control" type="text" name="minecraft" placeholder="Your minecraft name.." value="{{ $globalUser->minecraft }}">
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <p class="text-muted"><br>Having your minecraft username added will automatically whitelist you on the development server.</p>
                                    </div>
                                </div>

                                @if(1 == 0)
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="website">Your minecraft website</label>
                                            <input class="form-control" type="text" name="website" placeholder="Your minecraft website" value="{{ $globalUser->website }}">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <p class="text-muted"><br>.</p>
                                    </div>
                                </div>
                                @endif

                                <div class="row">
                                    <div class="col-md-12">
                                        <h3>Email Settings & Notifications</h3><br>
                                        <p>I would like to..</p>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="checkbox">
                                            <label>
                                                <div class="checker">
                                                    <span><input name="opt_in_plugin" type="checkbox"{{ $globalUser->settings()->get('opt_in_plugin') ? ' checked' : null }}></span>
                                                </div>
                                                Recive email notifications when a new plugin is added to a organization I am a member of.
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="checkbox">
                                            <label>
                                                <div class="checker">
                                                    <span><input name="opt_in_change" type="checkbox"{{ $globalUser->settings()->get('opt_in_change') ? ' checked' : null }}></span>
                                                </div>
                                                Recive email notifications when any changes or updates are made to plugins I have access to.
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <h3>Change Password</h3>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <p class="text-danger"><strong>You don't have to fill out the password fields below unless you actually want to change your password.</strong></p>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="password">Password</label>
                                            <input class="form-control" type="password" name="password" placeholder="Your password">
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="password_again">Password again</label>
                                            <input class="form-control" type="password" name="password_again" placeholder="Your password">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <input style="float: right;" class="btn btn-success" type="submit" value="Save my Settings">
                                    </div>
                                </div>

                            </form>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@stop
