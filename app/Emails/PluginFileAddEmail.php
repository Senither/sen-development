<?php

namespace App\Emails;

/**
 * This will be run when a new file is added to an already existing plugin.
 */
class PluginFileAddEmail extends Email
{
    protected function getEmailId()
    {
        return '0d0218bf-9e88-4c78-b268-1cca30d14df8';
    }

    protected function getVariables($user, $version, $plugin, $organization)
    {
        return [
            'name' => $user->name,
            'plugin' => $plugin->name,
            'url' => 'http://sen-dev.com/client/plugin/'.$organization->slug.'/'.$plugin->slug.'/'.str_slug($version),
            'version' => $version,
        ];
    }
}
