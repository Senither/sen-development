<?php

namespace App\Emails;

class UserAddToOrganization extends Email
{
    protected function getEmailId()
    {
        return '9ef7eaa4-81c6-49f2-a6a2-4747f8a2b400';
    }

    protected function getVariables($user, $organization)
    {
        return [
            'name' => $user->name,
            'organization' => $organization->name,
            'url' => 'http://sen-dev.com/client/organization/'.$organization->slug,
        ];
    }
}
