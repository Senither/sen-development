<?php

namespace App\Emails;

class PasswordResetEmail extends Email
{
    protected function getEmailId()
    {
        return '9d77c1e3-68e6-4aaa-841d-67f4f7d13a5f';
    }

    protected function getVariables($user, $token)
    {
        return [
            'name' => $user->name,
            'url' => 'http://sen-dev.com/client/user/forgot/'.$token,
        ];
    }
}
