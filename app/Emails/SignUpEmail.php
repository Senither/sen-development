<?php

namespace App\Emails;

class SignUpEmail extends Email
{
    protected function getEmailId()
    {
        return '9080ae70-6f46-49bb-b317-3023a6bcb8bb';
    }

    protected function getVariables($user)
    {
        return [
            'name' => $user->name,
            'url' => 'http://sen-dev.com/client/user/activate-account/'.$user->activation_token,
        ];
    }
}
