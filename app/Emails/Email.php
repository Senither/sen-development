<?php

namespace App\Emails;

use CS_REST_Transactional_SmartEmail;

abstract class Email
{
    protected $apiKey;

    protected $data = [];

    public function __construct($apiKey = null)
    {
        $this->apiKey = $apiKey ?: config('services.campain_monitor.key');
    }

    public function sendTo($user)
    {
        $mailer = $this->newTransaction();

        $data = $this->data;
        if (method_exists($this, 'getVariables')) {
            $data = call_user_func_array(
                [$this, 'getVariables'],
                array_merge(compact('user'), $this->data)
            );
        }

        return $mailer->send([
            'To' => $user->email,
            'Data' => $data,
        ]);
    }

    public function withData($data = [])
    {
        $this->data = $data;

        return $this;
    }

    protected function newTransaction()
    {
        return new CS_REST_Transactional_SmartEmail(
            $this->getEmailId(),
            ['api_key' => $this->apiKey]
        );
    }

    abstract protected function getEmailid();
}
