<?php

namespace App\Emails;

/**
 * This will be run when a new file is added to an already existing plugin.
 */
class InvoiceEmail extends Email
{
    protected function getEmailId()
    {
        return 'a7f8a7cb-cc24-4368-8546-7dbd5eb419ff';
    }

    protected function getVariables($user, $invoice)
    {
        return [
            'name' => $user->name,
            'receipt' => $invoice->receipt,
            'url' => 'http://sen-dev.com/client/invoice/'.$invoice->receipt,
        ];
    }
}
