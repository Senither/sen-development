<?php

/**
 * Displays a flash message to the user using ToastrJS.
 *
 * @param string $title
 * @param string $message
 *
 * @return mixed
 */
function flash($title = null, $message = null)
{
    $flash = app('App\Http\Flash');

    if (func_num_args() == 0) {
        return $flash;
    }

    return $flash->info($title, $message);
}

/**
 * Logs actions to the database.
 *
 * @param String $action
 * @param String $message
 *
 * @return mixed
 */
function logAction($action, $message)
{
    $logger = app('App\Http\Logger');

    return $logger->action($action)->message($message);
}

/**
 * Gets the users settings object, allows you to set new setting
 * values, overwrite old values and update the users settings.
 *
 * @param string $key
 *
 * @return mixed
 */
function settings($key = null)
{
    $settings = app('App\Models\Extensions\UserSettings');

    return $key != null ? $settings->get($key) : $settings;
}

/**
 * Generates a gravatar image from a users email.
 *
 * @param App\Models\User $user
 * @param int             $size
 * @param string          $default
 *
 * @return string $url
 */
function avatar_image($user, $size = 40, $default = 'http://sen-dev.com/assets/images/default-profile.jpg')
{
    $url = 'http://www.gravatar.com/avatar/'.md5(strtolower(trim($user->email)));
    $url .= '?d='.urlencode($default).'&s='.$size;

    return $url;
}

/**
 * Formats a carbon instance to a nice and simply looking timestamp.
 *
 * @param Carbon\Carbon $carbon
 *
 * @return string
 */
function generateTimestamp(Carbon\Carbon $carbon)
{
    return $carbon->format('M j, Y g:i A');
}

/**
 * Splits up an array into the given number.
 *
 * @param array $input
 * @param int   $num
 * @param bool  $preserve_keys
 *
 * @return array
 */
function array_chunk_fixed($input, $num, $preserve_keys = false)
{
    $count = count($input);

    if ($count) {
        $input = array_chunk($input, ceil($count / $num), $preserve_keys);
    }

    $input = array_pad($input, $num, array());

    return $input;
}

/**
 * Checks to see if we're on the current page, if we
 * are we return the open and active css classes.
 *
 * @param array  $request
 * @param string $requiredArgument
 *
 * @return mixed
 */
function isCurrentPage(array $request, $requiredArgument)
{
    $args = func_get_args();

    for ($i = 1; $i < func_num_args(); ++$i) {
        if (!isset($request[$i])) {
            return;
        }

        if ($args[$i] != $request[$i]) {
            return;
        }
    }

    return 'active open';
}

/**
 * Gets the status of a plugin as a label.
 *
 * @param \App\Models\Plugin $plugin
 *
 * @return string
 */
function getPluginStatusFrom(\App\Models\Plugin $plugin)
{
    if ($plugin->isFinished) {
        return '<span class="label label-success">Finished</span>';
    } elseif ($plugin->isPending) {
        return '<span class="label label-primary">Pending</span>';
    } elseif ($plugin->isUnderProgress) {
        switch ($plugin->status) {
            case 1:
                return '<span class="label label-info">Being worked on</span>';
                break;

            case 2:
                return '<span class="label label-info">Being updated</span>';
                break;
        }
    }

    return '<span class="label label-default">Unknown Status</span>';
}

/**
 * Converts the markdown code format into pre and code tags
 * so it can be displayed to the user in a nicer format.
 *
 * @param string $text
 *
 * @return string
 */
function createCodeStructure($text)
{
    $text = htmlentities($text);

    preg_match('/([`]{3}\w+)/', $text, $matches, PREG_OFFSET_CAPTURE);
    foreach ($matches as $parameters) {
        $tag = $parameters[0];
        $language = mb_strtolower(mb_substr($tag, 3));
        $start = "<pre class='language-{$language} line-numbers' rel='{$language}'><code class='language-{$language} language-capture'>";
        $text = str_replace($tag, $start, $text);
    }

    return nl2br(str_replace('```', '</code></pre>', $text));
}
