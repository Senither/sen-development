<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\OrganizationTrait;
use Illuminate\Database\Eloquent\Builder;

class Organization extends Model
{
    use OrganizationTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'organization';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'name', 'slug'];

    /**
     * Converts our timestamps to carbon instances.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * Orders the result of all of the organizations by name.
     *
     * @param Illuminate\Database\Eloquent\Builder $query
     *
     * @return Illuminate\Database\Eloquent\Builder
     */
    public function scopeOrderByName(Builder $query)
    {
        return $query->orderBy('name', 'asc');
    }

    /**
     * Sets up our organizations-user relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany('App\Models\User', 'organization_users', 'organization_id', 'users_id');
    }

    /**
     * Sets up our organizations-plugins relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function plugins()
    {
        return $this->hasMany('App\Models\Plugin', 'organization_id')->orderBy('name', 'asc');
    }
}
