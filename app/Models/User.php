<?php

namespace App\Models;

use Psy\Exception\ErrorException;
use Illuminate\Auth\Authenticatable;
use App\Models\Extensions\UserSettings;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'groups_id',
        'themes_id',
        'name',
        'email',
        'password',
        'activation_token',
        'settings',
        'minecraft',
        'website',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    /**
     * Converts settings from json to an array so it will be usable for us later on.
     *
     * @var array
     */
    protected $casts = [
        'settings' => 'json',
    ];

    /**
     * Get the user settings.
     *
     * @return Settings
     */
    public function settings()
    {
        return new UserSettings($this->settings, $this);
    }

    /**
     * Encrypts our password for us any time we write to the value.
     *
     * @param string $password
     */
    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = bcrypt($password);
    }

    /**
     * Checks to see if a user has the given permission node.
     *
     * @param string $node
     *
     * @return bool
     */
    public function hasPermission($node)
    {
        if ($node == null) {
            return false;
        }

        if ($this->group == null) {
            return false;
        }

        return $this->group->level >= 100000 ? true : $this->group->permissions->contains('node', $node);
    }

    /**
     * Checks to see if the user has access to see/use the given object.
     *
     * @param mixed $instance
     *
     * @throws Psy\Exception\ErrorException
     *
     * @return bool
     */
    public function hasAccesssTo($instance)
    {
        if ($instance == null) {
            throw new ErrorException('Argument 1 pased to App\Models\User::hasAccesssTo() can not be null!', 0, 1, __FILE__, __LINE__);
        }

        // If the user has the exempt permission node, we can just
        // return true as they will be an administrator.
        if ($this->hasPermission('management.secure-checks.exempt')) {
            return true;
        }

        if ($instance instanceof Plugin) {
            return $this->organizations->contains('id', $instance->organization_id);
        } elseif ($instance instanceof Organization) {
            return $this->organizations->contains('id', $instance->id);
        } elseif ($instance instanceof File) {
            return $this->hasAccesssTo(Plugin::find($instance->plugin_id));
        } elseif ($instance instanceof Invoice) {
            if ($this->hasPermission('management.invoice.others')) {
                return true;
            }

            return $this->id == $instance->sender->id || $this->id == $instance->receiver->id;
        }
        throw new ErrorException(
                'Argument 1 pased to App\Models\User::hasAccesssTo() must be an instance App\Models\Plugin or App\Models\Organization or App\Models\File!',
                0, 1, __FILE__, __LINE__
        );
    }

    /**
     * Creates our user object with all of the infomation related to the user.
     *
     * @param Illuminate\Database\Eloquent\Builder $query
     *
     * @return Illuminate\Database\Eloquent\Builder
     */
    public function scopeFullUser(Builder $query)
    {
        return $this->scopeFullUserById($query, \Auth::user()->id);
    }

    /**
     * Creates our user object with all of the information related to the user.
     *
     * @param Illuminate\Database\Eloquent\Builder $query
     * @param int                                  $id
     *
     * @return Illuminate\Database\Eloquent\Builder
     */
    public function scopeFullUserById(Builder $query, $id)
    {
        return $query->with('group.permissions')
                     ->with('organizations.plugins.files.author')
                     ->with('theme')
                     ->where('id', $id)
                     ->first();
    }

    /**
     * Gets all of the active users.
     *
     * @param Illuminate\Database\Eloquent\Builder $query
     *
     * @return Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive(Builder $query)
    {
        return $query->where('password', '!=', 'null');
    }

    /**
     * Gets all of the deactive users.
     *
     * @param Illuminate\Database\Eloquent\Builder $query
     *
     * @return Illuminate\Database\Eloquent\Builder
     */
    public function scopeDeactive(Builder $query)
    {
        return $query->where('password', '=', null);
    }

    /**
     * Sets up our user-group relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function group()
    {
        return $this->belongsTo('App\Models\Group', 'groups_id', 'id');
    }

    /**
     * Sets up our user-theme relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function theme()
    {
        return $this->belongsTo('App\Models\Theme', 'themes_id', 'id');
    }

    /**
     * Sets up our user-organizations relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function organizations()
    {
        return $this->belongsToMany('App\Models\Organization', 'organization_users', 'users_id');
    }
}
