<?php

namespace App\Models;

use App\Models\Traits\FaqTrait;
use Illuminate\Database\Eloquent\Model;

class Faq extends Model
{
    use FaqTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'faq';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['category_id', 'requester', 'question', 'awnser'];

    /**
     * Converts our timestamps to carbon instances.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * Sets up the file-plugin relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo('App\Models\FaqCategory');
    }
}
