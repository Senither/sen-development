<?php

namespace App\Models;

use App\Models\Traits\LoggerTrait;
use Illuminate\Database\Eloquent\Model;

class Logger extends Model
{
    use LoggerTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'logger';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'action', 'message', 'new', 'old', 'address'];

    /**
     * Converts our timestamps to carbon instances.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * Converts settings from json to an array so it will be usable for us later on.
     *
     * @var array
     */
    protected $casts = [
        'new' => 'json',
        'old' => 'json',
    ];

    /**
     * Sets up our logger-user relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }
}
