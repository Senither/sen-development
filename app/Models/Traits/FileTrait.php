<?php

namespace App\Models\Traits;

use App\Models\Scopes\OrderByNewestScope;

trait FileTrait
{
    public static function bootFileTrait()
    {
        static::addGlobalScope(new OrderByNewestScope());
    }
}
