<?php

namespace App\Models\Traits;

use App\Models\Scopes\OrderByNameScope;

trait FaqCategoryTrait
{
    public static function bootFaqCategoryTrait()
    {
        static::addGlobalScope(new OrderByNameScope());
    }
}
