<?php

namespace App\Models\Traits;

use App\Models\Scopes\OrderByNodeScope;

trait PermissionTrait
{
    public static function bootPermissionTrait()
    {
        static::addGlobalScope(new OrderByNodeScope());
    }
}
