<?php

namespace App\Models\Traits;

use App\Models\Scopes\OrderByNewestScope;

trait LoggerTrait
{
    public static function bootLoggerTrait()
    {
        static::addGlobalScope(new OrderByNewestScope());
    }
}
