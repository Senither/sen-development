<?php

namespace App\Models\Traits;

use App\Models\Scopes\OrderByNewestScope;

trait NewsTrait
{
    public static function bootNewsTrait()
    {
        static::addGlobalScope(new OrderByNewestScope());
    }
}
