<?php

namespace App\Models\Traits;

use App\Models\Scopes\OrderByAwnserScope;

trait FaqTrait
{
    public static function bootFaqTrait()
    {
        static::addGlobalScope(new OrderByAwnserScope());
    }
}
