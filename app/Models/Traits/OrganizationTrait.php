<?php

namespace App\Models\Traits;

use App\Models\Scopes\OrderByNameScope;

trait OrganizationTrait
{
    public static function bootOrganizationTrait()
    {
        static::addGlobalScope(new OrderByNameScope());
    }
}
