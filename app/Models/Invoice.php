<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'invoices';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['developer_id', 'user_id', 'status', 'receipt', 'token', 'items', 'total'];

    /**
     * Converts our timestamps to carbon instances.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * Converts items from json to an array so it will be usable for us later on.
     *
     * @var array
     */
    protected $casts = [
        'items' => 'json',
    ];

    /**
     * Converts our custom timestamp fields to Carbon instances.
     *
     * @var [type]
     */
    protected $dates = [
        'paid_at',
    ];

    /**
     * Sets up the Invoice-Developer(Sender) relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function sender()
    {
        return $this->belongsTo('App\Models\User', 'developer_id', 'id');
    }

    /**
     * Sets up the Invoice-Receiver(User) relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function receiver()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }
}
