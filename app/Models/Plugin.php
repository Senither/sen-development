<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Plugin extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'plugins';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'organization_id',
        'user_id',
        'availability',
        'version',
        'name',
        'slug',
        'progress',
        'status',
        'description',
    ];

    /**
     * Converts our timestamps to carbon instances.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * Sorts the query result so it only returns finished plugins.
     *
     * @param Illuminate\Database\Eloquent\Builder $query
     *
     * @return Illuminate\Database\Eloquent\Builder
     */
    public function scopeFinished(Builder $query)
    {
        return $query->where('status', 9);
    }

    /**
     * Sorts the query result so it only returns pending plugins.
     *
     * @param Illuminate\Database\Eloquent\Builder $query
     *
     * @return Illuminate\Database\Eloquent\Builder
     */
    public function scopePending(Builder $query)
    {
        return $query->where('status', 0);
    }

    /**
     * Sorts the query result so it only returns plugins that are still under progress.
     *
     * @param Illuminate\Database\Eloquent\Builder $query
     *
     * @return Illuminate\Database\Eloquent\Builder
     */
    public function scopeProgress(Builder $query)
    {
        return $query->whereBetween('status', [1, 2]);
    }

    /**
     * Will return true if the plugins status is "finished".
     *
     * @return bool
     */
    public function getIsFinishedAttribute()
    {
        return $this->status == 9;
    }

    /**
     * Will return true if the plugins status is "pending".
     *
     * @return bool
     */
    public function getIsPendingAttribute()
    {
        return $this->status == 0;
    }

    /**
     * Will return true if the plugins status is "finished".
     *
     * @return bool
     */
    public function getIsUnderProgressAttribute()
    {
        return $this->status < 9 && $this->status > 0;
    }

    /**
     * Sets up the plugin-file relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function files()
    {
        return $this->hasMany('App\Models\File');
    }

    /**
     * Sets up the plugin-organization relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function organization()
    {
        return $this->belongsTo('App\Models\Organization');
    }

    /**
     * Sets up the plugin-author relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function author()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }
}
