<?php

namespace App\Models;

use App\Models\Traits\FileTrait;
use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    use FileTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'files';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['plugin_id', 'user_id', 'slug', 'version', 'changes', 'file'];

    /**
     * Converts our timestamps to carbon instances.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * Sets up the file-plugin relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function plugin()
    {
        return $this->belongsTo('App\Models\Plugin');
    }

    /**
     * Sets up the file-user relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function author()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }
}
