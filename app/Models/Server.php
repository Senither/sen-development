<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Server extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'sendev_ss_cache';

    /**
     * Gets the latest server data from our ServerStatus table.
     *
     * @param Illuminate\Database\Eloquent\Builder $query
     * @param int                                  $size
     *
     * @return Illuminate\Database\Eloquent\Builder
     */
    public function scopeLatest(Builder $query, $size = 25)
    {
        return $query->orderBy('id', 'desc')->limit($size)->get();
    }
}
