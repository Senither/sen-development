<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'contacts';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['category_id', 'name', 'email', 'address', 'read', 'subject', 'body'];

    /**
     * Converts read from json to an array so it will be usable for us later on.
     *
     * @var array
     */
    protected $casts = [
        'read' => 'json',
    ];

    /**
     * Converts our timestamps to carbon instances.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * Sets up the contact-category relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo('App\Models\ContactCategory');
    }
}
