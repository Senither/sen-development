<?php

namespace App\Models;

use App\Models\Traits\NewsTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class News extends Model
{
    use NewsTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'news';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'title', 'description', 'body'];

    /**
     * Converts our timestamps to carbon instances.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * Gets the latest news data from our ServerStatus table.
     *
     * @param Illuminate\Database\Eloquent\Builder $query
     * @param int                                  $size
     *
     * @return Illuminate\Database\Eloquent\Builder
     */
    public function scopeLatest(Builder $query, $size = 25)
    {
        return $query->orderBy('id', 'desc')->limit($size)->get();
    }

    /**
     * Sets up our news-author relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function author()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }
}
