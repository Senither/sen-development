<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'groups';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'level', 'default'];

    /**
     * Converts our timestamps to carbon instances.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * Returns our default(Client) group.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeClient($query)
    {
        return $query->where('default', '=', 1)->first();
    }

    /*
     * Sets up our group-to-users relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function users()
    {
        return $this->hasMany('App\Models\User', 'groups_id', 'id');
    }

    /**
     * Get the permissions associated with the given permission.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function permissions()
    {
        return $this->belongsToMany('App\Models\Permission', 'groups_permissions');
    }
}
