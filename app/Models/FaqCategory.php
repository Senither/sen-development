<?php

namespace App\Models;

use App\Models\Traits\FaqCategoryTrait;
use Illuminate\Database\Eloquent\Model;

class FaqCategory extends Model
{
    use FaqCategoryTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'faq_categories';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];

    /**
     * Converts our timestamps to carbon instances.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Sets up the file-plugin relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function questions()
    {
        return $this->hasMany('App\Models\Faq', 'category_id', 'id');
    }
}
