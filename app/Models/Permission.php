<?php

namespace App\Models;

use App\Models\Traits\PermissionTrait;
use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    use PermissionTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'permissions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'node'];

    /**
     * Converts our timestamps to carbon instances.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Get the groups associated with the given permission.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function groups()
    {
        return $this->belongsToMany('App\Model\Group', 'groups_permissions');
    }
}
