<?php

namespace App\Console;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \App\Console\Commands\Inspire::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param \Illuminate\Console\Scheduling\Schedule $schedule
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function () {
            DB::table('password_resets')->where('created_at', '<=', Carbon::now()->subHour(3))->delete();
            DB::table('sendev_ss_cache')->where('created_at', '<=', Carbon::now()->subHour(3))->delete();
        })->everyMinute();

        $schedule->call(function () {
            DB::table('logger')->where('created_at', '<=', Carbon::now()->subMonth())->delete();
        })->daily()->at('6:00');
    }
}
