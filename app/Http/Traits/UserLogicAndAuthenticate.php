<?php

namespace App\Http\Traits;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

trait UserLogicAndAuthenticate
{
    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    public function __construct()
    {
        $this->createMiddleware();
    }

    public function createMiddleware()
    {
        $methods = (property_exists($this, 'ignoreMethods')) ? $this->ignoreMethods : [];
        $methods = array_merge($methods, ['login']);

        $this->middleware('auth', ['except' => $methods]);
        $this->middleware('guest', ['only' => $methods]);
    }

    /**
     * Logs in the user if everything is in order.
     *
     * @param Illuminate\Http\Request $request
     *
     * @return
     */
    public function login(Request $request)
    {
        if (!$request->has(['email', 'password'])) {
            return view('user.login');
        }

        // Validate our request
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required|min:3',
        ]);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        $throttles = $this->isUsingThrottlesLoginsTrait();

        if ($throttles && $this->hasTooManyLoginAttempts($request)) {
            return $this->sendLockoutResponse($request);
        }

        $credentials = $this->getCredentials($request);

        if (Auth::attempt($credentials, $request->has('remember'))) {
            flash()->success('Welcome '.Auth::user()->name.'!', 'You have logged in successfully!')
                   ->withCloseButton()
                   ->withTimer(4800);

            return $this->handleUserWasAuthenticated($request, $throttles);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        if ($throttles) {
            $this->incrementLoginAttempts($request);
        }

        return redirect($this->loginPath())
                        ->withInput($request->only($this->loginUsername(), 'remember'))
                        ->withErrors([
                            $this->loginUsername() => $this->getFailedLoginMessage(),
                        ]);
    }

    /**
     * Redirects the user back to the login page once they have been logged out.
     *
     * @return Symfony\Component\HttpFoundation\Redirect
     */
    public function logout()
    {
        Auth::logout();

        return redirect('client/login');
    }

    /**
     * Send the response after the user was authenticated.
     *
     * @param \Illuminate\Http\Request $request
     * @param bool                     $throttles
     *
     * @return \Illuminate\Http\Response
     */
    protected function handleUserWasAuthenticated(Request $request, $throttles)
    {
        if ($throttles) {
            $this->clearLoginAttempts($request);
        }

        if (method_exists($this, 'authenticated')) {
            return $this->authenticated($request, Auth::user());
        }

        return redirect()->intended('client/dashboard');
    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    protected function getCredentials(Request $request)
    {
        return $request->only('email', 'password');
    }

    /**
     * Determine if the class is using the ThrottlesLogins trait.
     *
     * @return bool
     */
    protected function isUsingThrottlesLoginsTrait()
    {
        return in_array(
            ThrottlesLogins::class, class_uses_recursive(get_class($this))
        );
    }

    /**
     * Get the failed login message.
     *
     * @return string
     */
    protected function getFailedLoginMessage()
    {
        return Lang::has('auth.failed')
                ? Lang::get('auth.failed')
                : 'These credentials do not match our records.';
    }

    /**
     * The field to use for the users login name.
     *
     * @return string $name
     */
    protected function loginUsername()
    {
        return (property_exists($this, 'loginUsername')) ? $this->loginUsername : 'username';
    }

    /**
     * The path to send the user to, to login.
     *
     * @return string $loginPath
     */
    protected function loginPath()
    {
        return (property_exists($this, 'loginPath')) ? $this->loginPath : 'client/login';
    }
}
