<?php

namespace App\Http\Controllers;

use App\Models\Organization;
use Illuminate\Support\Facades\Cache;
use App\Http\Controllers\Abstracts\Controller;
use Illuminate\Pagination\LengthAwarePaginator;

class OrganizationController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $organization = Cache::remember('organizations.'.md5($slug), 5, function () use ($slug) {
            return Organization::with('users.group')
                               ->with('plugins')
                               ->where('slug', $slug)
                               ->first();
        });

        if ($organization == null) {
            abort(404);
        }
        if (!$this->user->hasAccesssTo($organization)) {
            abort(403);
        }

        $plugins = new LengthAwarePaginator($organization->plugins, $organization->plugins->count(), 10, null, [
            'path' => action('OrganizationController@show', ['slug' => $slug]),
        ]);

        return view('pages.organization', compact('organization', 'plugins'));
    }
}
