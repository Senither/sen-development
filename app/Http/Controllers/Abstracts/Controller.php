<?php

namespace App\Http\Controllers\Abstracts;

use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Cache;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

abstract class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * The global user object of the user that is
     * currently logged in, this will return
     * null if the user isn't logged in.
     *
     * @var
     */
    protected $user = null;

    protected $pluginStatuses = [
        0 => 'Pending',
        1 => 'Work in Progress',
        2 => 'Fixing Bugs / Updating Plugin',
        9 => 'Finished',
    ];

    /**
     * Generates our global layout files for our views.
     */
    public function __construct()
    {
        // Cache::flush();
        if (Auth::check()) {
            $this->user = Cache::remember('users.'.Auth::user()->id.md5(Auth::user()->email), 5, function () {
                return User::fullUser();
            });

            $theme = $this->user->theme;
            if ($theme == null) {
                $theme = \App\Models\Theme::first();
                $this->user->themes_id = $theme->id;
                $this->user->save();

                Cache::forget('users.'.Auth::user()->id.''.md5(Auth::user()->email));

                flash()->warning('Theme error!', 'The chosen theme seems to be invalid, the theme has therefor been reset back to default.')
                       ->withCloseButton()
                       ->withTimer(7800);

                abort(500);
            }

            $themeColour = '18,175,203';
            switch ($this->user->theme->theme) {
                case 'dark':
                    $themeColour = '52,66,90';
                    break;
                case 'green':
                    $themeColour = '34,186,160';
                    break;
                case 'purple':
                    $themeColour = '122,111,190';
                    break;
                case 'red':
                    $themeColour = '242,86,86';
                    break;
                case 'white':
                    $themeColour = '145,145,145';
                    break;
            }
            View::share('globalThemeColour', $themeColour);
        }

        View::share('globalUser', $this->user);
    }

    /**
     * Removes the user from the cache, if no user is
     * provided the currently logged in user will be used.
     *
     * @param App\Models\User $user
     *
     * @return mixed
     */
    public function forgetUserCache(User $user = null)
    {
        if (!Auth::check()) {
            return false;
        }

        $user = $user == null ? Auth::user() : $user;

        return Cache::forget('users.'.$user->id.md5($user->email));
    }

    /**
     * Will require the currently logged in user to have the given
     * permission, if they don't have the permission the page
     * will terminate itself and end with a 403 code.
     *
     * @param string|array $permission
     *
     * @return mixed
     */
    protected function requires($permission)
    {
        foreach (func_get_args() as $node) {
            if (is_string($node)) {
                if (!$this->user->hasPermission($permission)) {
                    return abort(403);
                }
                continue;
            } elseif (is_array($node)) {
                foreach ($node as $perm) {
                    $this->requires($perm);
                }
            }
        }
    }
}
