<?php

namespace App\Http\Controllers;

use App\Models\File;
use App\Models\User;
use App\Models\Plugin;
use App\Models\Invoice;
use App\Models\Organization;
use Illuminate\Http\Request;
use App\Emails\PluginFileAddEmail;
use App\Http\Requests\FileRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;
use Illuminate\Database\Eloquent\Collection;
use App\Http\Controllers\Abstracts\Controller;
use Illuminate\Pagination\LengthAwarePaginator;

class DeveloperController extends Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->requires('development.see');
    }

    public function plugins()
    {
        $plugins = [];

        foreach ($this->user->organizations as $org) {
            foreach ($org->plugins as $plugin) {
                $plugins[] = $plugin;
            }
        }

        return view('developer.plugins', compact('plugins'));
    }

    public function pluginUpdate($organization, $plugin)
    {
        $this->convertAndCheckValues($organization, $plugin);

        if ($plugin->availability == 0 && $plugin->user_id != $this->user->id) {
            return abort(403);
        }

        return view('developer.plugin-update', compact('plugin', 'organization'));
    }

    public function pluginFileUpload(FileRequest $request)
    {
        if ($request->hasFile('fileUpload') && $request->file('fileUpload')->isValid()) {
            if ($request->file('fileUpload')->guessClientExtension() == 'bin') {
                $plugin = Plugin::with('organization.users')->find($request->get('plugin_id'));

                if ($plugin != null) {
                    if (!$this->user->hasAccesssTo($plugin)) {
                        return abort(403);
                    }

                    $versionCheck = File::where('version', $request->get('version'))
                                        ->where('plugin_id', $plugin->id)->first();
                    if ($versionCheck == null) {
                        $time = \Carbon\Carbon::now();
                        $name = $time->format('d-m-y').'_'.str_slug($plugin->name.' '.$request->version).'.jar';

                        $file = $request->file('fileUpload')->move(storage_path('files'), $name);

                        File::create([
                            'plugin_id' => $plugin->id,
                            'user_id' => $this->user->id,
                            'slug' => str_slug($request->get('version')),
                            'version' => $request->get('version'),
                            'changes' => $request->get('changes'),
                            'file' => $name,
                        ]);

                        $plugin->version = $request->get('version');
                        $plugin->save();

                        Cache::forget('developer.plugin-files.'.$plugin->slug.$plugin->organization->id);

                        foreach ($plugin->organization->users as $user) {
                            if ($user->settings['opt_in_change']) {
                                (new PluginFileAddEmail())->withData([
                                    'version' => $request->get('version'),
                                    'plugin' => $plugin,
                                    'organization' => $plugin->organization,
                                ])->sendTo($user);
                            }
                        }

                        $this->forgetUserCache();

                        flash()->success('The file was uploaded!', 'The file was updated successfully!');

                        // Logs the action to the database
                        logAction('PLUGIN_FILE_UPLOAD', $this->user->name.' uploaded a new file to '.$plugin->name)
                            ->beforeAndAfter(null, [
                                'name' => $name,
                                'version' => $request->get('version'),
                                'changes' => $request->get('changes'),
                            ]
                        );

                        return redirect()->action('DeveloperController@pluginFiles', ['organization' => $plugin->organization->slug, 'plugin' => $plugin->slug]);
                    } else {
                        flash()->warning('Version already exists!', 'The given version name already exists, please pick another version number.')
                               ->withCloseButton();
                    }
                } else {
                    flash()->error('Invalid plugin ID given', 'The plugin given for the upload was not found! Please try again, if the error presists, contact a server manager.')
                           ->withCloseButton();
                }
            } else {
                flash()->warning('Unsupported file type', 'You attempted to upload an unsupported file type as a plugin file update, please only upload .jar files.')
                       ->withCloseButton()
                       ->withTimer(7600);
            }
        } else {
            flash()->warning('Failed to upload file!', 'The file you tried to upload got lost on it\'s way here.. This might be due to a interal server error, please try again. If the error presists, contact a server manager.')
                   ->withCloseButton();
        }

        return redirect()->back();
    }

    public function pluginFiles($organization, $plugin)
    {
        $this->convertAndCheckValues($organization, $plugin);

        $files = new LengthAwarePaginator($plugin->files, $plugin->files->count(), 10, null, [
            'path' => action('DeveloperController@pluginFiles', [
                'organization' => $organization->slug,
                'plugin' => $plugin->slug,
            ]),
        ]);

        return view('developer.plugins-files', compact('organization', 'plugin', 'files'));
    }

    public function pluginFileEdit(Request $request, $organization, $plugin, $version)
    {
        $this->convertAndCheckValues($organization, $plugin);

        $version = $plugin->files->where('slug', $version)->first();

        if ($version == null) {
            abort(404);
        }

        if ($request->has('plugin_id', 'version_id', 'organization_id', 'changes') && $request->get('version_id') == $version->id) {
            logAction('PLUGIN_FILE_UPDATE', $this->user->name.' made changes to '.$version->file)
                ->beforeAndAfter([
                    'changes' => $version->changes,
                    ], [
                    'changes' => $request->get('changes'),
                ]
            );

            $version->update($request->only('changes'));

            $this->forgetUserCache();

            flash()->success('File was updated', 'The file version "'.$version->version.'" was updated successfully!')
                   ->withCloseButton()
                   ->withTimer(4800);
        }

        return view('developer.plugin-file-edit', compact('organization', 'plugin', 'version'));
    }

    public function pluginEdit(Request $request, $organization, $plugin)
    {
        $this->convertAndCheckValues($organization, $plugin);
        $statuses = $this->pluginStatuses;

        if ($request->has('plugin_id', 'user_id', 'organization_id', 'status', 'description')) {
            if ($plugin->id == $request->get('plugin_id')) {
                $inputs = $request->only('description');

                if ($request->has('name') && $plugin->user_id == $this->user->id) {
                    $inputs['name'] = $request->get('name');
                    $inputs['slug'] = str_slug($inputs['name']);
                }

                if (array_key_exists($request->get('status'), $statuses)) {
                    $inputs['status'] = intval($request->get('status'));
                }

                if ($request->has('availability') && $plugin->user_id == $this->user->id) {
                    $inputs['availability'] = $request->get('availability') == 0 ? 0 : 1;
                }

                $loggerFields = [];
                foreach ($inputs as $key => $value) {
                    $loggerFields[$key] = $plugin->{$key};
                }

                logAction('PLUGIN_UPDATE', $this->user->name.' made changes to '.$plugin->name)
                    ->beforeAndAfter($loggerFields, $inputs);

                $plugin->update($inputs);
                $this->forgetUserCache();

                Cache::forget('plugins.'.md5($organization->slug).md5($plugin->slug));
                Cache::forget('developer.plugin-files.'.$plugin->slug.$organization->id);
                foreach (Plugin::with('organization.users')->where('id', $plugin->id)->get() as $users) {
                    foreach ($users->organization->users as $user) {
                        $this->forgetUserCache($user);
                    }
                }

                flash()->success('Plugin was updated', 'The plugin was updated successfully!')
                       ->withCloseButton()
                       ->withTimer(5700);

                return redirect()->action('DeveloperController@pluginEdit', [
                    'organization' => $organization->slug,
                    'plugin' => $plugin->slug,
                ]);
            } else {
                flash()->error('Invalid Plugin ID', 'The given plugin ID doesn\'t match our records, please try again or contact us about this issue.')
                       ->withCloseButton();
            }
        }

        return view('developer.plugins-edit', compact('organization', 'plugin', 'statuses'));
    }

    public function organizations()
    {
        $organizations = [];

        foreach ($this->user->organizations as $organization) {
            $organizations[] = $organization;
        }

        return view('developer.organizations', compact('organizations'));
    }

    public function organizationsMembers($organization)
    {
        $this->convertAndCheckOrganization($organization);

        $members = Cache::remember('organization-users.'.$organization->id, 1, function () use ($organization) {
            return Collection::make(DB::table('users')->leftJoin('organization_users', 'id', '=', 'users_id')
                                     ->where('organization_users.organization_id', $organization->id)->get());
        });

        $all = Cache::remember('organization-users.all-users', 1, function () {
            return User::with('group')->orderBy('groups_id')->get();
        });

        return view('developer.organization-members', compact('organization', 'members', 'all'));
    }

    public function organizationsActionMember(Request $request, $organization, $id, $action)
    {
        $this->convertAndCheckOrganization($organization);
        $user = User::findOrFail($id);
        $action = mb_strtolower($action);

        if ($organization->user_id != $this->user->id) {
            $this->requires('development.plugin.members.modify');
        }

        if (!$this->user->hasAccesssTo($organization) || !in_array($action, ['add', 'remove'])) {
            return abort(403);
        }

        if ($request->has('form_action', 'form_user', 'form_organization')) {
            if ($user->id != $request->get('form_user') || $organization->slug != $request->get('form_organization')) {
                return abort(403);
            }

            $action = $request->get('form_action');
            $form_organization = $request->get('form_organization');
            $this->convertAndCheckOrganization($form_organization);

            if ($form_organization->id != $organization->id || !$this->user->hasAccesssTo($form_organization) || !in_array($action, ['add', 'remove'])) {
                return abort(403);
            }

            if ($action == 'add') {
                if (DB::table('organization_users')->where('organization_id', $organization->id)->where('users_id', $user->id)->count() == 0) {
                    DB::table('organization_users')->insert([
                        'organization_id' => $organization->id,
                        'users_id' => $user->id,
                    ]);
                }

                flash()->success($user->name.' was added!', $user->name.' was added to '.$organization->name.' successfully!')
                       ->withCloseButton()
                       ->withTimer(5000);

                logAction('ORGANIZATION_USER_ADD', $user->name.' was added to '.$organization->name);

                if (isset($user->settings['opt_in_plugin']) && $user->settings['opt_in_plugin']) {
                    (new \App\Emails\UserAddToOrganization())
                                    ->withData(['organization' => $organization])
                                    ->sendTo($user);
                }
            } else {
                DB::table('organization_users')->where('organization_id', $organization->id)
                                               ->where('users_id', $user->id)
                                               ->delete();
                flash()->success($user->name.' was removed!', $user->name.' was removed from '.$organization->name.' successfully!')
                       ->withCloseButton()
                       ->withTimer(5000);

                logAction('ORGANIZATION_USER_REMOVE', $user->name.' was removed from '.$organization->name);
            }

            Cache::forget('organization-users.'.$organization->id);
            $this->forgetUserCache($user);

            return redirect()->action('DeveloperController@organizationsMembers', ['organization' => $organization->slug]);
        }

        return view('developer.organization-action-member', compact('organization', 'user', 'action'));
    }

    public function organizationCreate(Request $request)
    {
        $this->requires('development.organization.create');

        if ($request->has('name')) {
            $this->validate($request, [
                'name' => 'required|min:3|max:64',
            ]);

            $name = trim($request->get('name'));
            $slug = str_slug($name);

            if (organization::where('slug', $slug)->first() == null) {
                $organization = organization::create([
                    'user_id' => $this->user->id,
                    'name' => $name,
                    'slug' => $slug,
                ]);

                DB::table('organization_users')->insert([
                    'organization_id' => $organization->id,
                    'users_id' => $this->user->id,
                ]);

                $this->forgetUserCache();
                flash()->success('The organization was created!', 'Your organization was created successfully! You can now start adding people to it.')
                       ->withCloseButton()
                       ->withTimer(8500);

                logAction('ORGANIZATION_CREATE', $this->user->name.' created a new Organization ('.$organization->name.')');

                return redirect()->action('DeveloperController@organizationsMembers', ['organization' => $slug]);
            }

            return view('developer.organization-create')->withErrors(['The given name is already taken!']);
        }

        return view('developer.organization-create');
    }

    public function organizationPluginCreate(Request $request, $organization)
    {
        $this->requires('development.plugin.create');

        $this->convertAndCheckOrganization($organization);
        $statuses = $this->pluginStatuses;

        if ($request->has('name', 'availability', 'status', 'progress', 'description', 'organization_id')) {
            $this->validate($request, [
                'name' => 'required|min:3',
                'availability' => 'required|integer|digits_between:0,1',
                'status' => 'required|integer|in:0,1,2,9',
                'progress' => 'required|integer|digits_between:0,100',
                'description' => 'required|min:16',
            ]);

            if ($request->get('organization_id') != $organization->id || !$this->user->hasAccesssTo($organization)) {
                flash()->error('Insufficient Access', 'You attempted to create a new plugin under a organization that you don\'t have access to.')
                       ->withCloseButton();

                return view('developer.organization-plugin-create', compact('organization', 'statuses'));
            }

            $slug = str_slug($request->get('name'));

            if (Plugin::where('slug', $slug)->where('organization_id', $organization->id)->first() != null) {
                return redirect()->action('DeveloperController@organizationPluginCreate', ['organization' => $organization->slug])
                                 ->withInput($request->all())->withErrors(['message' => 'The given plugin name already exists!']);
            }

            $inputs = $request->only('name', 'availability', 'status', 'progress', 'description');
            $inputs['organization_id'] = $organization->id;
            $inputs['user_id'] = $this->user->id;
            $inputs['version'] = 'None';
            $inputs['slug'] = $slug;

            $plugin = Plugin::create($inputs);

            Cache::forget('organizations.'.md5($organization->slug));
            Cache::forget('plugins.'.md5($organization->slug).md5($plugin->slug));
            $this->forgetUserCache();

            flash()->success('The plugin was created!', '"'.$inputs['name'].'" was created successfully! You can use the Development Tools to add members or edit the plugin later.')
                   ->withCloseButton()
                   ->withTimer(12500);

            logAction('PLUGIN_CREATE', $this->user->name.' created a new plugin ('.$plugin->name.')')
                    ->beforeAndAfter(null, $inputs);

            return redirect()->action('PluginController@show', [
                'organization' => $organization->slug,
                'plugin' => $slug,
            ]);
        }

        return view('developer.organization-plugin-create', compact('organization', 'statuses'));
    }

    public function invoice(Request $request)
    {
        $page = $request->get('page', 1);

        $invoices = Cache::remember('developer.invoices.'.$this->user->id.'.'.$page, 15, function () {
            return Invoice::with('sender')
                          ->with('receiver')
                          ->where('developer_id', $this->user->id)
                          ->orderBy('created_at', 'desc')
                          ->paginate(10);
        });

        return view('developer.invoice-all', compact('invoices'));
    }

    public function invoiceCreate()
    {
        $users = [];

        foreach ($this->user->organizations as $organization) {
            foreach ($organization->users as $user) {
                if (!isset($users[$user->id]) && $user->id != $this->user->id) {
                    $users[$user->id] = $user->name;
                }
            }
        }

        return view('developer.invoice-create', compact('users'));
    }

    public function invoiceSave(Request $request)
    {
        $this->validate($request, [
            'user' => 'required|numeric',
            'item_name_1' => 'required|min:3',
            'item_quantity_1' => 'required',
            'item_price_1' => 'required',
        ]);

        $receiver = User::findOrFail($request->get('user'));

        $total = 0;
        $items = [];
        $currentIndex = 1;
        while (true) {
            if (!$request->has('item_name_'.$currentIndex)) {
                break;
            }

            $items[$currentIndex] = [
                'name' => $request->get('item_name_'.$currentIndex),
                'quantity' => intval($request->get('item_quantity_'.$currentIndex)),
                'cost' => doubleval($request->get('item_price_'.$currentIndex)),
            ];
            $total = $total + ($items[$currentIndex]['quantity'] * $items[$currentIndex]['cost']);

            ++$currentIndex;
        }

        $receipt = '0000'.$receiver->id;
        $receipt = mb_substr($receipt, mb_strlen($receipt) - 4).'-';

        $invoice = Invoice::create([
            'developer_id' => $this->user->id,
            'user_id' => $receiver->id,
            'status' => 0,
            'receipt' => str_random(32),
            'token' => null,
            'items' => $items,
            'total' => $total,
        ]);

        $idFiller = '000000'.$invoice->id;
        $receipt .= mb_substr($idFiller, mb_strlen($idFiller) - 6);

        $invoice->receipt = $receipt;
        $invoice->token = sha1($receipt).str_random(32).$invoice->user_id;
        $invoice->save();

        // Deletes development invoices
        $currentIndex = 1;
        while (true) {
            if (!Cache::has('developer.invoices.'.$this->user->id.'.'.$currentIndex)) {
                break;
            }

            Cache::forget('developer.invoices.'.$this->user->id.'.'.$currentIndex++);
        }

        // Deletes normal invoices
        $currentIndex = 1;
        while (true) {
            if (!Cache::has('invoices.'.$this->user->id.'-'.$currentIndex)) {
                break;
            }

            Cache::forget('invoices.'.$this->user->id.'-'.$currentIndex++);
        }

        (new \App\Emails\InvoiceEmail())->withData([
            'invoice' => $invoice,
        ])->sendTo($receiver);

        return redirect()->action('DeveloperController@invoice');
    }

    protected function convertAndCheckValues(&$organization, &$plugin)
    {
        $this->convertAndCheckOrganization($organization);
        $this->convertAndCheckPlugin($organization, $plugin);
    }

    protected function convertAndCheckOrganization(&$organization)
    {
        if (!$this->user->organizations->contains('slug', $organization)) {
            abort(404);
        }
        $organization = $this->user->organizations->where('slug', $organization)->first();
    }

    protected function convertAndCheckPlugin($organization, &$plugin)
    {
        $plugin = Cache::remember('developer.plugin-files.'.$plugin.$organization->id, 1, function () use ($plugin, $organization) {
            return Plugin::with('files.author')->where('slug', $plugin)
                                               ->where('organization_id', $organization->id)
                                               ->first();
        });

        if ($plugin == null) {
            abort(404);
        }
    }
}
