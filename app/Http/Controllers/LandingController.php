<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\Http\Controllers\Abstracts\Controller;

class LandingController extends Controller
{
    /**
     * Displays the landing page.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('landing');
    }

    /**
     * Stores the contact form request in the
     * database, and sets our flash message.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|min:2',
            'email' => 'required|email',
            'subject' => 'required|min:3',
            'message' => 'required|min:16',
        ]);

        Contact::create([
            'category_id' => 0,
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'address' => $request->server('REMOTE_ADDR'),
            'read' => [],
            'subject' => $request->get('subject'),
            'body' => $request->get('message'),
        ]);

        return Response::json([
            'status' => 'success',
        ], 200);
    }
}
