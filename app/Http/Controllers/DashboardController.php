<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Cache;
use App\Http\Controllers\Abstracts\Controller;

class DashboardController extends Controller
{
    /**
     * Displays our dashboard to our user.
     *
     * @return Illuminate\Support\Facades\View
     */
    public function index()
    {
        $server = Cache::remember('server-status', 1, function () {
            return \App\Models\Server::latest(1)->first();
        });

        $news = Cache::remember('news.latest', 10, function () {
            return \App\Models\News::latest(3);
        });

        return view('pages.dashboard', compact('server', 'news'));
    }
}
