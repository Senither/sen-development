<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\Faq;
use App\Models\Contact;
use App\Models\FaqCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;
use App\Http\Controllers\Abstracts\Controller;

class SupportController extends Controller
{
    public function faq()
    {
        $categories = Cache::remember('support.categories', 12 * 60, function () {
            return FaqCategory::with(['questions' => function ($query) {
                $query->where('awnser', '!=', 'null');
            }])->get();
        });

        $latest = Cache::remember('support.categories.latest', 12 * 60, function () {
            return DB::table('faq')->orderBy('id', 'desc')->where('awnser', '!=', 'null')->take(3)->get();
        });

        return view('support.faq', compact('categories', 'latest'));
    }

    public function formFaq(Request $request)
    {
        if ($request->has('question', 'category')) {
            $input = $request->only('question', 'category');
            $input['requester'] = $this->user->id;

            if (FaqCategory::find($input['category']) == null) {
                flash()->warning('Invalid category!', 'Invalid category id was given.')
                       ->withTimer(6000);

                return redirect()->action('SupportController@faq')->withErrors(['message' => 'The given category must be valid!']);
            }
            $input['category_id'] = $input['category'];

            Faq::create($input);

            flash()->info('Your question was sent!', 'Your question was sent off to our support center, we will try and awnser your question as quickly as possible!')
                   ->withTimer(12000)
                   ->withCloseButton();
        }

        return redirect()->action('SupportController@faq');
    }

    public function suggestion()
    {
        $categories = Cache::remember('support.suggestions', 24 * 60, function () {
            return \App\Models\SuggestionCategory::all();
        });

        return view('support.suggestion', compact('categories'));
    }

    public function formSuggestion(Request $request)
    {
        $this->validate($request, [
            'category_id' => 'required|numeric',
            'suggestion' => 'required|min:8',
        ]);

        DB::table('suggestions')->insert([
            'category_id' => $request->get('category_id'),
            'requster' => $this->user->id,
            'suggestion' => $request->get('suggestion'),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        flash()->info('Suggestion was created', 'Suggestion was created successfully!');

        return redirect()->action('SupportController@suggestion');
    }

    /**
     * Displays the contact page to user.
     *
     * @return View
     */
    public function contact()
    {
        $categories = Cache::remember('support.contact', 720, function () {
            return \App\Models\SuggestionCategory::all();
        });

        return view('support.contact');
    }

    /**
     * Handels the post contact form request, if the validation goes
     * trough the contact message will be saved to the database,
     * the user will be redirected back to the contact form
     * and a success flash message will be displayed.
     *
     * @param Request $request
     *
     * @return Redirect
     */
    public function formContact(Request $request)
    {
        $this->validate($request, [
            'category_id' => 'required|numeric',
            'subject' => 'required|min:3',
            'message' => 'required|min:32',
        ]);

        Contact::create([
            'category_id' => $request->get('category_id'),
            'name' => $this->user->name,
            'email' => $this->user->email,
            'address' => $request->server('REMOTE_ADDR'),
            'read' => [],
            'subject' => $request->get('subject'),
            'body' => $request->get('message'),
        ]);

        flash()->success('Mail was sent!', 'Your contact mail was sent to us successfully, we will get back to you as soon as possible!')
               ->withTimer(9200)
               ->withCloseButton();

        return redirect()->action('SupportController@contact');
    }
}
