<?php

namespace App\Http\Controllers;

use App\Models\File;
use App\Models\Plugin;
use App\Models\Organization;
use Illuminate\Support\Facades\Cache;
use App\Http\Controllers\Abstracts\Controller;
use Illuminate\Pagination\LengthAwarePaginator;

class PluginController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($organization, $plugin)
    {
        $plugin = $this->plugin($organization, $plugin);
        $version = $plugin->files->isEmpty() ? null : $plugin->files[0];
        $files = $this->filesFrom($plugin);

        return view('pages.plugin', compact('plugin', 'version', 'files', 'users'));
    }

    /**
     * Displays the given plugin version for the selected
     * plugin if the user has access to use it.
     *
     * @param string $organization
     * @param string $plugin
     * @param string $version
     *
     * @return \Illuminate\Http\Response
     */
    public function showVersion($organization, $plugin, $version)
    {
        $version = $this->pluginWithVersion($organization, $plugin, $version);

        if (!($version instanceof File)) {
            return $version;
        }

        $files = $this->filesFrom($plugin, $version);

        return view('pages.plugin-version', compact('plugin', 'version', 'files'));
    }

    /**
     * Handels the download process of the plugin of the user
     * has access to the given plugin file.
     *
     * @param string $organization
     * @param string $plugin
     * @param string $version
     *
     * @return \Illuminate\Http\Response
     */
    public function download($organization, $plugin, $version)
    {
        $version = $this->pluginWithVersion($organization, $plugin, $version);

        if (!($version instanceof File)) {
            abort(404);
        }

        $file = storage_path('files').DIRECTORY_SEPARATOR.$version->file;
        if (!file_exists($file)) {
            abort(404);
        }

        $attachment = str_replace(' ', '', $plugin->name).'-'.str_replace(' ', '-', $version->version).'.jar';

        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename="'.$attachment.'"');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: '.filesize($file));

        return readfile($file);
    }

    protected function filesFrom($plugin, $version = null)
    {
        $arguments = [
            'organization' => $plugin->organization->slug,
            'plugin' => $plugin->slug,
        ];

        $method = 'show';

        if ($version != null) {
            $method              .= 'Version';
            $arguments['version'] = $version->slug;
        }

        return new LengthAwarePaginator($plugin->files, $plugin->files->count(), 8, null, [
            'path' => action('PluginController@'.$method, $arguments),
        ]);
    }

    /**
     * Gets the plugin object using eloquent egar-loading, as-well
     * as the version of the currently selected file.
     *
     * @param string $organization
     * @param string $plugin
     * @param string $version
     *
     * @return App\Models\Plugin
     */
    protected function pluginWithVersion($organization, &$plugin, $version)
    {
        $plugin = $this->plugin($organization, $plugin);

        if ($version == 'latest') {
            $version = $plugin->files[0];
        } else {
            $version = $plugin->files->where('slug', $version)->first();

            if ($version == null) {
                return redirect()->action('PluginController@show', [
                    'organization' => $plugin->organization->slug,
                    'plugin' => $plugin->slug,
                ]);
            }
        }

        return $version;
    }

    /**
     * Gets the plugin object using eloquent egar-loading.
     *
     * @param string $organization
     * @param string $plugin
     *
     * @return App\Models\Plugin
     */
    protected function plugin($organization, $plugin)
    {
        $organization = Cache::remember('organizations.'.md5($organization), 10, function () use ($organization) {
            return Organization::where('slug', $organization)->firstOrFail();
        });

        $plugin = Cache::remember('plugins.'.md5($organization->slug).md5($plugin), 5, function () use ($plugin, $organization) {
            return Plugin::with('organization.users.group')
                         ->with('files')
                         ->where('slug', $plugin)
                         ->where('organization_id', $organization->id)
                         ->first();
        });

        if ($plugin == null || $plugin->organization->slug !== $organization->slug) {
            abort(404);
        }

        if (!$this->user->hasAccesssTo($plugin)) {
            abort(403);
        }

        return $plugin;
    }
}
