<?php

namespace App\Http\Controllers;

use App\Models\Faq;
use App\Models\File;
use App\Models\News;
use App\Models\User;
use App\Models\Group;
use App\Models\Theme;
use App\Models\Logger;
use App\Models\Plugin;
use App\Models\Contact;
use App\Models\Invoice;
use App\Models\Permission;
use App\Emails\SignUpEmail;
use App\Models\FaqCategory;
use App\Models\Organization;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;
use Illuminate\Database\Eloquent\Collection;
use App\Http\Controllers\Abstracts\AdminController;

class ManagementController extends AdminController
{
    /**
     * Constructs our controller and sets our
     * permission requirement for a user to
     * be able to user the controller.
     */
    public function __construct()
    {
        parent::__construct();

        $this->requires('management.see');
    }

    public function news(Request $request)
    {
        $this->requires('management.news.see');

        $page = $request->get('page', 1);
        $news = Cache::remember('management.news-all.page.'.$page, 10, function () {
            return News::with('author')->paginate(10);
        });

        return view('management.news', compact('news'));
    }

    public function newsEdit($id, $slug)
    {
        $this->requires('management.news.see');

        $article = Cache::remember('news.edit-article.'.$id, 5, function () use ($id) {
            return News::findOrFail($id);
        });

        return view('management.news-edit', compact('article'));
    }

    public function newsSave(Request $request, $id, $slug)
    {
        $this->requires('management.news.see');

        $this->validate($request, [
            'article' => 'required|integer',
            'title' => 'required|min:3',
            'description' => 'required|min:8',
            'body' => 'required|min:32',
        ]);

        $article = News::findOrFail($request->get('article'));

        if ($article->id != $id) {
            abort(404);
        }

        logAction('MANAGEMENT_NEWS_UPDATE', $this->user->name.' updated the news article with an ID of '.$article->id)
            ->beforeAndAfter([
                'title' => $article->title,
                'description' => $article->description,
                'body' => $article->body,
            ], $request->only('title', 'description', 'body'));

        $article->update($request->only('title', 'description', 'body'));

        Cache::forget('news.edit-article.'.$article->id);

        flash()->success('The news article was updated!', 'The news article with an ID of "'.$article->id.'" was updated successfully!')
               ->withCloseButton()
               ->withTimer(5700);

        return redirect()->action('ManagementController@newsEdit', ['id' => $article->id, 'slug' => str_slug($article->title)]);
    }

    public function newsCreate(Request $request)
    {
        $this->requires('management.news.see');

        if ($request->has('title', 'description', 'body')) {
            $this->validate($request, [
                'title' => 'required|min:3',
                'description' => 'required|min:8',
                'body' => 'required|min:32',
            ]);

            $input = $request->only('title', 'description', 'body');
            $input['user_id'] = $this->user->id;

            $article = News::create($input);

            flash()->success('Your news article was created!', 'Your news article has been created successfully!')
                   ->withCloseButton()
                   ->withTimer(7500);

            $currentCachePage = 1;
            while (true) {
                if (!Cache::has('management.news-all.page.'.$currentCachePage)) {
                    break;
                }

                Cache::forget('news.all.'.$currentCachePage);
                Cache::forget('management.news-all.page.'.$currentCachePage++);
            }

            logAction('MANAGEMENT_NEWS_CREATE', $this->user->name.' has created a new News Article!')
                ->beforeAndAfter([], $input);

            return redirect()->action('NewsController@show', ['id' => $article->id, 'slug' => str_slug($article->title)]);
        }

        return view('management.news-create');
    }

    public function users(Request $request)
    {
        $this->requires('management.user.see');

        $page = $request->get('page', 1);

        $users = Cache::remember('management.users-page.'.$page, 15, function () {
            return User::with('group')->orderBy('groups_id', 'asc')
                                      ->orderBy('name', 'asc')
                                      ->paginate(10);
        });

        return view('management.users', compact('users'));
    }

    public function user($id)
    {
        $this->requires('management.user.edit');

        $user = User::findOrFail($id);

        $themes = Theme::all();
        $groups = Group::all();

        return view('management.users-view', compact('user', 'themes', 'groups'));
    }

    public function updateUser(Request $request, $id)
    {
        $this->requires('management.user.edit');

        $this->validate($request, [
            'name' => 'required|min:3',
            'themes_id' => 'required|numeric',
            'email' => 'required|email',
            'groups_id' => 'required|numeric',
            'minecraft' => 'min:2|max:32',
            'website' => 'min:2|url',
        ]);

        $user = User::findOrFail($id);
        $group = $user->group->id;

        logAction('MANAGEMENT_USER_UPDATE', $this->user->name.' updated '.$user->name.'s account')
            ->beforeAndAfter([
                'name' => $user->name,
                'email' => $user->email,
                'themes_id' => $user->themes_id,
                'groups_id' => $user->groups_id,
                'minecraft' => $user->minecraft,
                'website' => $user->website,
            ], $request->only('name', 'email', 'themes_id', 'groups_id', 'minecraft', 'website')
        );

        $groupObj = Group::findOrFail($request->groups_id);

        $user->update($request->only('name', 'email', 'themes_id', 'groups_id', 'minecraft', 'website'));

        if ($group != $groupObj->id) {
            flash()->info('User group was changed!', $user->name.' is now a member of the "'.$groupObj->name.'" group!')
                   ->withCloseButton()
                   ->withTimer(6200);
        }

        flash()->success('User was updated!', $user->name.' was updated and saved successfully!')
               ->withCloseButton()
               ->withTimer(5800);

        $pageNumber = 1;
        while (true) {
            if (!Cache::has('management.users-page.'.$pageNumber)) {
                break;
            }

            Cache::forget('management.users-page.'.$pageNumber++);
        }
        $this->forgetUserCache($user);

        return redirect()->action('ManagementController@user', ['id' => $user->id, 'user' => str_slug($user->name)]);
    }

    public function userCreate(Request $request)
    {
        $this->requires('management.user.invite');

        if ($request->has('name', 'email')) {
            $user = User::where('email', $request->get('email'))->first();

            if ($user == null) {
                $input = $request->only('name', 'email');
                $input['activation_token'] = str_random(rand(32, 128));
                $input['settings'] = [
                    'opt_in_plugin' => true,
                    'opt_in_change' => true,
                ];
                $input['groups_id'] = Group::client()->id;
                $user = User::create($input);

                (new SignUpEmail())->sendTo($user);

                $pageNumber = 1;
                while (true) {
                    if (!Cache::has('management.users-page.'.$pageNumber)) {
                        break;
                    }

                    Cache::forget('management.users-page.'.$pageNumber++);
                }

                flash()->success('User was create!', $user->name.' was created and contacted with an inviation email!')
                      ->withCloseButton()
                      ->withTimer(5800);

                logAction('MANAGEMENT_USER_INVITE', $this->user->name.' sent a user invite to '.$user->email);

                return redirect()->action('ManagementController@user', ['id' => $user->id, 'name' => str_slug($user->name)]);
            }

            return redirect()->back()->withErrors(['message' => 'The email is already taken']);
        }

        return view('management.users-create');
    }

    public function groups()
    {
        $this->requires('management.group.see');

        $groups = Cache::remember('management.groups', 720, function () {
            return Group::orderBy('level', 'desc')->get();
        });

        return view('management.groups', compact('groups'));
    }

    public function group($id)
    {
        $this->requires('management.group.edit');

        $group = Cache::remember('management.groups-group.'.$id, 2, function () use ($id) {
            return Group::with('permissions')->findOrFail($id);
        });

        $permissions = Cache::remember('management.groups-permissions', 120, function () {
            return $permissions = Permission::all();
        });

        return view('management.group-edit', compact('group', 'permissions'));
    }

    public function saveGroup(Request $request)
    {
        $this->requires('management.group.edit');

        $this->validate($request, [
            'name' => 'required|min:3',
            'level' => 'required|numeric',
            'group' => 'required|numeric',
        ]);

        $group = Group::findOrFail($request->get('group'));
        $group->name = $request->get('name');
        $group->level = $request->get('level');
        $group->default = $request->has('default');

        logAction('MANAGEMENT_GROUP_UPDATE', $this->user->name.' updated '.$group->name)
            ->beforeAndAfter([
                'name' => $group->name,
                'level' => $group->level,
                'default' => $group->default,
            ], [
                'name' => $request->get('name'),
                'level' => $request->get('level'),
                'default' => $request->has('default'),
            ]
        );

        $group->save();

        if ($request->has('default')) {
            DB::table('groups')->where('id', '!=', $group->id)->update(['default' => 0]);
        }

        Cache::forget('management.groups');
        Cache::forget('management.groups-group.'.$group->id);

        return redirect()->back();
    }

    public function updateGroupPermission($group, $permission, $action)
    {
        $group = Group::findOrFail($group);
        $permission = Permission::findOrFail($permission);
        $runCleanup = false;

        if ($action == 'add') {
            DB::table('groups_permissions')->insert([
                'group_id' => $group->id,
                'permission_id' => $permission->id,
            ]);

            $runCleanup = true;
            logAction('MANAGEMENT_PERMISSION_ADD', $this->user->name.' added the '.$permission->node.' permissions node to '.$group->name);
        } elseif ($action == 'remove') {
            DB::table('groups_permissions')->where('group_id', $group->id)
                                           ->where('permission_id', $permission->id)
                                           ->delete();

            $runCleanup = true;
            logAction('MANAGEMENT_PERMISSION_REMOVE', $this->user->name.' removed the permission node '.$permission->node.' from '.$group->name);
        }

        if ($runCleanup) {
            foreach (User::where('groups_id', $group->id)->get() as $user) {
                $this->forgetUserCache($user);
            }
        }

        Cache::forget('management.groups-group.'.$group->id);

        return redirect()->back();
    }

    public function plugins(Request $request)
    {
        $this->requires('management.plugin.see');

        $page = $request->get('page', 1);

        $plugins = Cache::remember('management.plugins-page.'.$page, 15, function () {
            return Plugin::with('organization')->orderBy('name', 'asc')->paginate(10);
        });

        return view('management.plugins', compact('plugins'));
    }

    public function plugin($id, $slug)
    {
        $this->requires('management.plugin.edit');

        $plugin = Plugin::with('organization')->with('author')->where('slug', $slug)->first();
        if ($plugin == null) {
            return abort(404);
        }

        $statuses = $this->pluginStatuses;

        return view('management.plugin-edit', compact('plugin', 'statuses'));
    }

    public function pluginSave(Request $request, $id, $slug)
    {
        $this->requires('management.plugin.edit');

        $this->validate($request, [
            'plugin_id' => 'required|in:'.$id,
            'user_id' => 'required|integer',
            'name' => 'required|min:3',
            'status' => 'required|integer',
            'availability' => 'required|integer',
            'description' => 'required|min:12',
        ]);

        $plugin = Plugin::with('organization')->findOrFail($request->id);

        $inputs = $request->only('description');
        $inputs['name'] = $request->get('name');
        $inputs['slug'] = str_slug($inputs['name']);
        $inputs['status'] = intval($request->get('status'));
        $inputs['availability'] = $request->get('availability') == 0 ? 0 : 1;

        $loggerFields = [];
        foreach ($inputs as $key => $value) {
            $loggerFields[$key] = $plugin->{$key};
        }

        logAction('MANAGEMENT_PLUGIN_UPDATE', $this->user->name.' made changes to '.$plugin->name)
            ->beforeAndAfter($loggerFields, $inputs);

        $plugin->update($inputs);
        $this->forgetUserCache();

        Cache::forget('developer.plugin-files.'.$plugin->slug.$plugin->organization->id);
        foreach (Plugin::with('organization.users')->where('id', $plugin->id)->get() as $users) {
            foreach ($users->organization->users as $user) {
                $this->forgetUserCache($user);
            }
        }

        flash()->success('Plugin was updated', 'The plugin was updated successfully!')
               ->withCloseButton()
               ->withTimer(5700);

        return redirect()->action('ManagementController@plugin', ['id' => $plugin->id, 'slug' => $plugin->slug]);
    }

    public function files(Request $request)
    {
        $this->requires('management.file.see');

        $page = $request->has('page') ? $request->get('page') : 1;
        $files = Cache::remember('management.files.'.$page, 1, function () {
            return File::with('plugin.organization')->paginate(10);
        });

        return view('management.files', compact('files'));
    }

    public function fileEdit(Request $request, $id, $slug)
    {
        $this->requires('management.file.edit');

        $file = File::with('plugin.organization')->with('author')->findOrFail($id);

        if ($request->has('file_id', 'user_id', 'plugin_id', 'organization_id', 'changes')) {
            $this->validate($request, [
                'file_id' => 'required|in:'.$id,
                'user_id' => 'required|in:'.$file->user_id,
                'plugin_id' => 'required|in:'.$file->plugin->id,
                'organization_id' => 'required|in:'.$file->plugin->organization->id,
                'changes' => 'required|min:6',
            ]);

            $file->update($request->only('changes'));

            flash()->success('File was updated', 'The file version "'.$file->version.'" for "'.$file->plugin->name.'" was updated successfully!')
                   ->withCloseButton()
                   ->withTimer(6500);

            Cache::forget('plugins.'.md5($file->plugin->organization->slug).md5($file->plugin->slug));

            return redirect()->action('ManagementController@fileEdit', ['id' => $id, 'slug' => $slug]);
        }

        return view('management.files-edit', compact('file'));
    }

    public function organization(Request $request)
    {
        $this->requires('management.organization.see');

        $page = $request->get('page', 1);

        $organizations = Cache::remember('management.organizations-page.'.$page, 15, function () {
            return Organization::with('users')->with('plugins')->paginate(10);
        });

        return view('management.organization', compact('organizations'));
    }

    public function organizationsMembers($organization)
    {
        $this->requires('management.organization.edit');

        $organization = organization::where('slug', $organization)->first();

        if ($organization == null) {
            abort(404);
        }

        $members = Cache::remember('organization-users.'.$organization->id, 1, function () use ($organization) {
            return Collection::make(DB::table('users')->leftJoin('organization_users', 'id', '=', 'users_id')
                                     ->where('organization_users.organization_id', $organization->id)->get());
        });

        $all = Cache::remember('organization-users.all-users', 1, function () {
            return User::with('group')->orderBy('groups_id')->get();
        });

        return view('management.organization-members', compact('organization', 'members', 'all'));
    }

    public function organizationsActionMember(Request $request, $organization, $id, $action)
    {
        $this->requires('management.organization.edit');

        $organization = organization::where('slug', $organization)->first();

        if ($organization == null) {
            abort(404);
        }

        $user = User::findOrFail($id);
        $action = mb_strtolower($action);

        if ($organization->user_id != $this->user->id) {
            $this->requires('development.plugin.members.modify');
        }

        if (!$this->user->hasAccesssTo($organization) || !in_array($action, ['add', 'remove'])) {
            return abort(403);
        }

        if ($request->has('form_action', 'form_user', 'form_organization')) {
            if ($user->id != $request->get('form_user') || $organization->slug != $request->get('form_organization')) {
                return abort(403);
            }

            $action = $request->get('form_action');
            $form_organization = $request->get('form_organization');
            $form_organization = organization::where('slug', $form_organization)->first();

            if ($form_organization->id != $organization->id || !$this->user->hasAccesssTo($form_organization) || !in_array($action, ['add', 'remove'])) {
                return abort(403);
            }

            if ($action == 'add') {
                if (DB::table('organization_users')->where('organization_id', $organization->id)->where('users_id', $user->id)->count() == 0) {
                    DB::table('organization_users')->insert([
                        'organization_id' => $organization->id,
                        'users_id' => $user->id,
                    ]);
                }

                flash()->success($user->name.' was added!', $user->name.' was added to '.$organization->name.' successfully!')
                       ->withCloseButton()
                       ->withTimer(5000);

                if (isset($user->settings['opt_in_plugin']) && $user->settings['opt_in_plugin']) {
                    (new \App\Emails\UserAddToOrganization())
                                    ->withData(['organization' => $organization])
                                    ->sendTo($user);
                }
            } else {
                DB::table('organization_users')->where('organization_id', $organization->id)
                                               ->where('users_id', $user->id)
                                               ->delete();
                flash()->success($user->name.' was removed!', $user->name.' was removed from '.$organization->name.' successfully!')
                       ->withCloseButton()
                       ->withTimer(5000);
            }

            Cache::forget('organization-users.'.$organization->id);
            $this->forgetUserCache($user);

            return redirect()->action('ManagementController@organizationsMembers', ['organization' => $organization->slug]);
        }

        return view('management.organization-action-member', compact('organization', 'user', 'action'));
    }

    public function questions(Request $request)
    {
        $this->requires('management.faq.see');

        $page = $request->get('page', 1);

        $unawnsered = Cache::remember('management.questions-unawnsered-page.'.$page, 30, function () {
            return Collection::make(DB::table('faq')->leftJoin('users', 'users.id', '=', 'faq.requester')
                                   ->join('faq_categories', 'faq_categories.id', '=', 'faq.category_id')
                                   ->select('faq.*', 'users.name', 'faq_categories.name as category')
                                   ->where('awnser', null)
                                   ->get());
        });

        $awnsered = Cache::remember('management.questions-awnsered-page.'.$page, 30, function () {
            return FaqCategory::with('questions')->get();
        });

        return view('management.questions', compact('awnsered', 'unawnsered'));
    }

    public function deleteQuestion($id)
    {
        $this->requires('management.faq.delete');

        if (DB::table('faq')->where('id', $id)->delete()) {
            flash()->success('Question was deleted!', 'Question with the ID of '.$id.' was removed successfully!')
                   ->withCloseButton()
                   ->withTimer(6200);

            logAction('MANAGEMENT_QUESTION_REMOVE', $this->user->name.' removed the question with an ID of '.$id);
        }

        return redirect()->action('ManagementController@questions');
    }

    public function editQuestion(Request $request, $id)
    {
        $this->requires('management.faq.edit');

        if ($request->has('question')) {
            $input = $request->only('question', 'awnser', 'category_id');
            $input['requester'] = 0;

            if (mb_strlen(trim($input['awnser'])) == 0) {
                $input['awnser'] = null;
            }

            logAction('MANAGEMENT_QUESTION_EDIT', $this->user->name.' updated the question with an ID of '.$id)
                ->beforeAndAfter(DB::table('faq')->select('question', 'awnser', 'category_id', 'requester')->where('id', $id)->first(), $input);

            DB::table('faq')->where('id', $id)
                            ->update($input);

            flash()->success('Question was updated!', 'The question "'.$input['question'].'" was updated successfully!')
                   ->withCloseButton()
                   ->withTimer(6200);

            Cache::forget('management.questions-unawnsered-page.1');

            $pageNumber = 1;
            while (true) {
                if (!Cache::has('management.questions-awnsered-page.'.$pageNumber)) {
                    break;
                }

                Cache::forget('management.questions-awnsered-page.'.$pageNumber++);
            }

            return redirect()->action('ManagementController@questions');
        }

        $question = DB::table('faq')->where('id', $id)->first();

        if ($question == null) {
            return redirect()->action('ManagementController@questions');
        }

        return view('management.question-edit', compact('question'));
    }

    public function messages(Request $request)
    {
        $this->requires('management.message.see');

        $page = $request->get('page', 1);
        $messages = Cache::remember('management.messages-page.'.$page, 1, function () {
            return Contact::with('category')->paginate(10);
        });

        return view('management.messages', compact('messages'));
    }

    public function messageShow($id)
    {
        $this->requires('management.message.see');

        $message = Cache::remember('management.message.'.$id, 1, function () use ($id) {
            return Contact::findOrFail($id);
        });

        if (!in_array($this->user->id, $message->read)) {
            $read = $message->read;
            $read[count($read)] = $this->user->id;
            $message->read = $read;
            $message->save();
        }

        return view('management.message-show', compact('message'));
    }

    public function suggestions(Request $request)
    {
        $this->requires('management.suggestion.see');

        $suggestions = Cache::remember('management.suggestions', 1, function () {
            return DB::table('suggestions')
                ->select('suggestions.*', 'users.name', 'suggestion_categories.name AS category')
                ->leftJoin('users', 'suggestions.requster', '=', 'users.id')
                ->leftJoin('suggestion_categories', 'suggestions.category_id', '=', 'suggestion_categories.id')
                ->orderBy('suggestions.created_at', 'desc')->get();
        });

        return view('management.suggestions', compact('suggestions'));
    }

    public function suggestionDelete($id)
    {
        $this->requires('management.suggestion.delete');

        DB::table('suggestions')->where('id', $id)->delete();
        Cache::forget('management.suggestions');

        return redirect()->action('ManagementController@suggestions');
    }

    public function logger(Request $request)
    {
        $this->requires('management.logs.see');

        $page = $request->get('page', 1);
        $logs = Cache::remember('management.logs-page.'.$page, 1, function () {
            return Logger::with('user')->paginate(10);
        });

        return view('management.logs', compact('logs'));
    }

    public function loggerShow($id)
    {
        $this->requires('management.logs.see');

        $log = Cache::remember('management.logs-view.'.$id, 120, function () use ($id) {
            return Logger::with('user.group')->findOrFail($id);
        });

        return view('management.logs-view', compact('log'));
    }

    public function invoices(Request $request)
    {
        $this->requires('management.invoice.see');

        $page = $request->get('page', 1);
        $invoices = Cache::remember('management.invoice-page.'.$page, 5, function () {
            return Invoice::orderBy('status', 'asc')
                           ->orderBy('created_at', 'desc')
                           ->paginate(10);
        });

        return view('management.invoice-all', compact('invoices'));
    }
}
