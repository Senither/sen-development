<?php

namespace App\Http\Controllers;

use Auth;
use Carbon\Carbon;
use App\Models\User;
use App\Models\Theme;
use Illuminate\Http\Request;
use App\Emails\PasswordResetEmail;
use App\Http\Requests\UserRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;
use App\Http\Traits\UserLogicAndAuthenticate;
use App\Http\Controllers\Abstracts\Controller;

class UserController extends Controller
{
    use UserLogicAndAuthenticate;

    /**
     * Constructs our global layout variables by calling the parent constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->createMiddleware();
    }

    /**
     * The field we should use to repecent the username.
     *
     * @var string
     */
    protected $loginUsername = 'email';

    /**
     * The following methods will be ignored by the login check so
     * that we can visit them without having to login first.
     *
     * @var array
     */
    protected $ignoreMethods = ['forgot', 'saveForgot', 'activate'];

    public function authenticated(Request $request, User $user)
    {
        $this->forgetUserCache($user);

        $user->remember_token = null;
        $user->save();

        return redirect()->intended('client/dashboard');
    }

    /**
     * Handel password resets getter and post requests.
     *
     * @param Illuminate\Http\Request $request
     * @param string                  $token
     *
     * @return mixed
     */
    public function forgot(Request $request, $token = null)
    {
        if ($token == null) {
            // Post request was sent
            if ($request->has('email')) {
                $user = User::where('email', '=', $request->get('email'))->first();
                if ($user != null) {
                    $token = str_random(128);
                    DB::table('password_resets')->insert([
                        'email' => $user->email,
                        'token' => $token,
                        'created_at' => Carbon::now(),
                    ]);

                    (new PasswordResetEmail())
                                    ->withData(['token' => $token])
                                    ->sendTo($user);

                    return redirect()->action('UserController@login')
                                     ->with(
                                        'success_message',
                                        'A password reset request email have been sent to '.$user->email.'!'
                    );
                }

                return redirect()->back()->withErrors([
                    'failedMessage' => 'These credentials do not match our records.',
                ]);
            }

            return view('user.forgot');
        }

        $row = DB::table('password_resets')->where('token', '=', $token)->first();
        if ($row == null) {
            abort(404);
        }

        $user = User::where('email', '=', $row->email)->first();
        if ($user == null) {
            abort('404');
        }

        return view('user.forgot-form', compact('user', 'token'));
    }

    /**
     * Saves the password request once it has been sent.
     *
     * @param Illuminate\Http\Request $request
     *
     * @return mixed
     */
    public function saveForgot(UserRequest $request)
    {
        DB::table('password_resets')->where('email', '=', $request->email)->delete();
        $user = User::where('email', '=', $request->get('email'))->first();

        $user->activation_token = null;
        $user->password = $request->get('password');
        $user->save();

        return redirect()->action('UserController@login')->with('success_message', 'Your password was changed successfully!');
    }

    public function activate(Request $request, $token)
    {
        $user = User::where('activation_token', $token)->first();

        if ($user == null) {
            abort(404);
        }

        if ($request->has('name', 'password', 'password_again')) {
            $validate = Validator::make($request->all(), [
                'name' => 'required|min:3',
                'password' => 'required|min:6',
                'password_again' => 'same:password',
                'token_id' => 'required',
            ]);

            if ($validate->fails()) {
                return redirect()->back()->withErrors($validate->errors()->all());
            }

            $user->name = $request->get('name');
            $user->password = $request->get('password');
            $user->activation_token = null;
            $user->save();

            Auth::attempt([
                'email' => $user->email,
                'password' => $request->get('password'),
            ]);

            flash()->success('Welcome '.Auth::user()->name.'!', 'You have logged in successfully!')
                   ->withCloseButton()
                   ->withTimer(4800);

            return redirect()->action('DashboardController@index');
        }

        return view('user.activate-account', compact('user', 'token'));
    }

    /**
     * Displays the users settings to them, and allows them to edit them.
     *
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Support\Facades\View
     */
    public function settings(Request $request)
    {
        $themes = Cache::remember('user.themes', 24 * 60, function () {
            return Theme::all();
        });

        if (count($request->all()) >= 6 && $request->has('name', 'theme')) {
            $input = $request->all();
            $errors = [];

            // Overwrites the name
            $this->user->name = $input['name'];

            // Overwrites the theme if it is valid
            if (is_numeric($input['theme']) && $themes->contains('id', $input['theme'])) {
                $this->user->themes_id = $input['theme'];
            }

            // Sets the users minecraft name if valid
            if (isset($input['minecraft']) && mb_strlen(trim($input['minecraft'])) > 0) {
                $this->user->minecraft = $input['minecraft'];
            } else {
                $this->user->minecraft = null;
            }

            // Sets the users website if valid
            if (isset($input['website']) && mb_strlen(trim($input['website'])) > 0) {
                $this->user->website = $input['website'];
            } else {
                $this->user->website = null;
            }

            // Set opt-in email settings
            $this->user->settings()->set('opt_in_plugin', isset($input['opt_in_plugin']));
            $this->user->settings()->set('opt_in_change', isset($input['opt_in_change']));

            // Sets a new password if
            $password = null;
            if (isset($input['password']) && isset($input['password_again']) && mb_strlen(trim($input['password'])) > 0) {
                if (mb_strlen(trim($input['password'])) > 5) {
                    if (trim($input['password']) == $input['password_again']) {
                        $this->user->password = trim($input['password']);
                        $password = true;
                    } else {
                        $errors[] = 'Password and password again must match.';
                    }
                } else {
                    $errors[] = 'Your password must be 6 characters or longer.';
                }
            }

            $this->user->save();
            $this->forgetUserCache();

            if (!empty($errors)) {
                return redirect()->action('UserController@settings')
                                  ->withErrors($errors);
            }

            flash()->success('Settings was update', 'Your account settings was updated successfully!')
                   ->withCloseButton()
                   ->withTimer(5400);

            if ($password != null) {
                flash()->info('Password was changed!', 'Your password was changed successfully.')
                       ->withCloseButton()
                       ->withTimer(4800);
            }

            return redirect()->action('UserController@settings');
        }

        return view('user.settings', compact('themes'));
    }
}
