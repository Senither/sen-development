<?php

namespace App\Http\Controllers;

use App\Models\News;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use App\Http\Controllers\Abstracts\Controller;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $page = $request->get('page', 1);

        $news = Cache::remember('news.all.'.$page, 10, function () {
            $items = News::orderBy('id', 'desc')->get();

            return new \Illuminate\Pagination\LengthAwarePaginator($items, count($items), 8, null, [
                'path' => action('NewsController@index'),
            ]);
        });

        return view('news.all', compact('news'));
    }

    public function show($id, $slug)
    {
        $article = Cache::remember('news.article.'.$id, 5, function () use ($id) {
            return News::with('author')->findOrFail($id);
        });

        if (str_slug($article->title) != $slug) {
            return abort(404);
        }

        return view('news.view', compact('article'));
    }
}
