<?php

namespace App\Http\Controllers;

use App\Http\Payment;
use App\Models\Invoice;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use App\Http\Controllers\Abstracts\Controller;

class InvoiceController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $page = $request->get('page', 1);

        $invoices = Cache::remember('invoices.'.$this->user->id.'-'.$page, 2, function () {
            return Invoice::with('sender')
                          ->where('user_id', $this->user->id)
                          ->orderBy('status', 'asc')
                          ->orderBy('created_at', 'desc')
                          ->paginate(10);
        });

        return view('invoice.all', compact('invoices'));
    }

    public function show($receipt)
    {
        $invoice = $this->getInvoice($receipt);

        if (Input::has('payment') && Input::get('payment') == 'success' && Input::get('token', '') != $invoice->token) {
            return redirect()->action('InvoiceController@show', ['receipt' => $invoice->receipt]);
        }

        return view('invoice.show', compact('invoice'));
    }

    public function build(Request $request)
    {
        $message = 'Missing fields, you can\'t find any records with the propper fields';

        if ($request->has('invoice', 'token')) {
            $invoice = Invoice::where('receipt', $request->get('invoice'))->first();

            if ($invoice != null && $invoice->token == $request->token) {
                if ($this->user->hasAccesssTo($invoice) && $invoice->status == 0) {
                    $sessToken = str_random(64);

                    $payment = $this->buildPaymentObjectWith($invoice);
                    $payment->setOption('invoice', $invoice->receipt);
                    $payment->setOption('custom', [
                        'sess_token' => $sessToken,
                        'receipt' => $invoice->receipt,
                        'user' => $invoice->user_id,
                        'dev' => $invoice->developer_id,
                    ]);

                    foreach ($invoice->items as $item) {
                        $payment->addItem($item['name'], $item['quantity'], $item['cost']);
                    }

                    return Response::json([
                        'status' => 'success',
                        'data' => $payment->renderArray(),
                    ], 200);
                } else {
                    $message = 'You dont have permission to access the given invoice';
                }
            } else {
                $message = 'Failed to find any invoice matching the given id';
            }
        }

        return Response::json([
            'status' => 'error',
            'reason' => $message,
        ], 422);
    }

    public function confirm(Request $request, $receipt)
    {
        if (!$request->has('token')) {
            return redirect()->action('InvoiceController@show', ['receipt' => $receipt]);
        }

        $invoice = Invoice::where('receipt', $receipt)
                          ->where('token', $request->get('token'))
                          ->first();

        if ($invoice != null && $invoice->status == 0 && ($invoice->developer_id == $this->user->id || $this->user->hasPermission('management.invoice.others'))) {
            $invoice->status = 1;
            $invoice->token = null;
            $invoice->paid_at = \Carbon\Carbon::now();
            $invoice->save();

            Cache::forget('invoice.receipt.'.$receipt);

            $currentPageNumber = 1;
            $devInvoice = true;
            $norInvoice = true;
            while (true) {
                $devInvoice = Cache::has('developer.invoices.'.$this->user->id.'.'.$currentPageNumber);
                $norInvoice = Cache::has('developer.invoices.'.$this->user->id.'.'.$currentPageNumber);

                if ($devInvoice === false && $norInvoice === false) {
                    break;
                }

                Cache::forget('developer.invoices.'.$this->user->id.'.'.$currentPageNumber);
                cache::forget('invoices.'.$this->user->id.'-'.$currentPageNumber);

                ++$currentPageNumber;
            }
        }

        return redirect()->action('InvoiceController@show', ['receipt' => $receipt]);
    }

    /**
     * Builds the Payment object with the given return and cancel return urls.
     *
     * @param string $return
     * @param string $cancel_return
     *
     * @return Payment
     */
    protected function buildPaymentObjectWith(Invoice $invoice)
    {
        $payment = new Payment('iamphoenix.me@gmail.com');

        $payment->setButtonImage(asset('assets/images/empty-image.png'));

        $payment->setReturnUrl('client/invoice/'.$invoice->receipt.'?payment=success&token='.$invoice->token);
        $payment->setCancelReturnUrl('client/invoice/'.$invoice->receipt);

        return $payment;
    }

    /**
     * Gets the given invoice that match the receipt.
     *
     * @param string $receipt
     *
     * @return Invoice
     */
    protected function getInvoice($receipt)
    {
        $invoice = Cache::remember('invoice.receipt.'.$receipt, 15, function () use ($receipt) {
            return Invoice::with('sender')
                          ->with('receiver')
                          ->where('receipt', $receipt)->first();
        });

        if ($invoice == null) {
            abort(404);
        }

        if (!$this->user->hasAccesssTo($invoice)) {
            abort(403);
        }

        return $invoice;
    }
}
