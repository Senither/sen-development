<?php

namespace App\Http;

use App\Models\User;
use App\Models\Logger as LogModel;
use Illuminate\Support\Facades\Auth;

class Logger
{
    private $user = true;
    private $user_id = 0;
    private $action = null;
    private $message = null;

    private $old = null;
    private $new = null;

    private $run = false;

    public function __construct()
    {
        $this->user_id = Auth::check() ? Auth::user()->id : 0;
    }

    public function __destruct()
    {
        if (!$this->run) {
            $this->save();
        }
    }

    public function useUser($user)
    {
        if (is_bool($user)) {
            $this->user = $user;
        } elseif ($user instanceof User) {
            $this->user = true;
            $this->user_id = $user->id;
        }

        return $this;
    }

    public function action($action)
    {
        $this->action = $action;

        return $this;
    }

    public function message($message)
    {
        $this->message = $message;

        return $this;
    }

    public function beforeAndAfter($before, $after)
    {
        $this->old = $before;
        $this->new = $after;

        return $this;
    }

    public function save()
    {
        $user = ($this->user) ? $this->user_id : 0;

        if ($this->action == null || $this->message == null) {
            return false;
        }

        $this->run = true;

        return LogModel::create([
            'user_id' => $user,
            'action' => $this->action,
            'message' => $this->message,
            'new' => $this->new,
            'old' => $this->old,
            'address' => $_SERVER['REMOTE_ADDR'],
        ]);
    }
}
