<?php

// Event::listen('illuminate.query', function ($query) use (&$queries) {
//     echo '<pre class="language-sql" rel="sql"><code class="language-sql">' . $query . '</code></pre>';
// });

// |-| \\

Route::get('/', ['as' => 'home', 'uses' => 'LandingController@index']);
Route::post('/save', ['uses' => 'LandingController@store']);

// Client site
Route::group(['prefix' => 'client'], function () {
    // Default route to always return to the dashboard
    Route::any('/', ['middleware' => 'auth', function () {
        return redirect('client/dashboard');
    }]);

    // Logged in routes
    Route::get('dashboard', ['middleware' => 'auth', 'uses' => 'DashboardController@index']);

    // Organization routes
    Route::get('organization/{organization}', ['middleware' => 'auth', 'uses' => 'OrganizationController@show']);

    // Plugin routes
    Route::get('plugin/{organization}/{plugin}', ['middleware' => 'auth', 'uses' => 'PluginController@show']);
    Route::get('plugin/{organization}/{plugin}/{version}', ['middleware' => 'auth', 'uses' => 'PluginController@showVersion']);
    Route::get('plugin/{organization}/{plugin}/{version}/download', ['middleware' => 'auth', 'uses' => 'PluginController@download']);

    // Users routes
    Route::match(['get', 'post'], 'login', 'UserController@login');
    Route::match(['get', 'post'], 'user/forgot-password', 'UserController@forgot');
    Route::match(['get', 'post'], 'user/forgot/{token?}', 'UserController@forgot');
    Route::match(['get', 'post'], 'user/settings', 'UserController@settings');
    Route::post('user/forgot/{token}/save', 'UserController@saveForgot');
    Route::get('user/logout', 'UserController@logout');

    Route::match(['get', 'post'], 'user/activate-account/{token}', 'UserController@activate');

    // Invoice routes
    Route::post('invoice', ['middleware' => 'auth', 'uses' => 'InvoiceController@build']);
    Route::get('invoice/all', ['middleware' => 'auth', 'uses' => 'InvoiceController@index']);
    Route::get('invoice/{receipt}', ['middleware' => 'auth', 'uses' => 'InvoiceController@show']);
    Route::post('invoice/update-invoice/{receipt}', ['middleware' => 'auth', 'uses' => 'InvoiceController@confirm']);

    // Support routes
    Route::get('support/faq', ['middleware' => 'auth', 'uses' => 'SupportController@faq']);
    Route::post('support/faq', ['middleware' => 'auth', 'uses' => 'SupportController@formFaq']);
    Route::get('support/contact', ['middleware' => 'auth', 'uses' => 'SupportController@contact']);
    Route::post('support/contact', ['middleware' => 'auth', 'uses' => 'SupportController@formContact']);
    Route::get('support/suggestion', ['middleware' => 'auth', 'uses' => 'SupportController@suggestion']);
    Route::post('support/suggestion', ['middleware' => 'auth', 'uses' => 'SupportController@formSuggestion']);
    Route::get('news/all', ['middleware' => 'auth', 'uses' => 'NewsController@index']);
    Route::get('news/{id}/{slug}', ['middleware' => 'auth', 'uses' => 'NewsController@show']);

    // Developer Tools routes
    Route::get('developer-tools/plugins', ['middleware' => 'auth', 'uses' => 'DeveloperController@plugins']);
    Route::get('developer-tools/plugins/{organization}/{plugin}/files', ['middleware' => 'auth', 'uses' => 'DeveloperController@pluginFiles']);
    Route::match(['get', 'post'], 'developer-tools/plugins/{organization}/{plugin}/files/{file}', ['middleware' => 'auth', 'uses' => 'DeveloperController@pluginFileEdit']);
    Route::get('developer-tools/plugins/{organization}/{plugin}/add-file', ['middleware' => 'auth', 'uses' => 'DeveloperController@pluginUpdate']);
    Route::post('developer-tools/plugins/{organization}/{plugin}/add-file/upload-file', ['middleware' => 'auth', 'uses' => 'DeveloperController@pluginFileUpload']);
    Route::match(['get', 'post'], 'developer-tools/plugins/{organization}/{plugin}/edit', ['middleware' => 'auth', 'uses' => 'DeveloperController@pluginEdit']);

    Route::get('developer-tools/organizations', ['middleware' => 'auth', 'uses' => 'DeveloperController@organizations']);
    Route::get('developer-tools/organizations/{organization}/members', ['middleware' => 'auth', 'uses' => 'DeveloperController@organizationsMembers']);
    Route::match(['get', 'post'], 'developer-tools/organizations/{organization}/members/{id}/{action}', ['middleware' => 'auth', 'uses' => 'DeveloperController@organizationsActionMember']);
    Route::match(['get', 'post'], 'developer-tools/organizations/create', ['middleware' => 'auth', 'uses' => 'DeveloperController@organizationCreate']);
    Route::match(['get', 'post'], 'developer-tools/organizations/{organization}/plugin-create', ['middleware' => 'auth', 'uses' => 'DeveloperController@organizationPluginCreate']);

    Route::get('developer-tools/invoices', ['middleware' => 'auth', 'uses' => 'DeveloperController@invoice']);
    Route::get('developer-tools/invoices/new', ['middleware' => 'auth', 'uses' => 'DeveloperController@invoiceCreate']);
    Route::post('developer-tools/invoices/new/save', ['middleware' => 'auth', 'uses' => 'DeveloperController@invoiceSave']);

    // Management routes
    Route::get('management/news', ['middleware' => 'auth', 'uses' => 'ManagementController@news']);
    Route::get('management/news/{id}/{slug}/edit', ['middleware' => 'auth', 'uses' => 'ManagementController@newsEdit']);
    Route::post('management/news/{id}/{slug}/save', ['middleware' => 'auth', 'uses' => 'ManagementController@newsSave']);
    Route::match(['get', 'post'], 'management/news/create', ['middleware' => 'auth', 'uses' => 'ManagementController@newsCreate']);

    Route::get('management/users', ['middleware' => 'auth', 'uses' => 'ManagementController@users']);
    Route::get('management/users/{id}/{user}', ['middleware' => 'auth', 'uses' => 'ManagementController@user']);
    Route::post('management/users/{id}/{user}', ['middleware' => 'auth', 'uses' => 'ManagementController@updateUser']);
    Route::match(['get', 'post'], 'management/users/invite', ['middleware' => 'auth', 'uses' => 'ManagementController@userCreate']);

    Route::get('management/groups', ['middleware' => 'auth', 'uses' => 'ManagementController@groups']);
    Route::get('management/groups/{id}/{slug?}', ['middleware' => 'auth', 'uses' => 'ManagementController@group']);
    Route::post('management/groups/{id}/{slug?}', ['middleware' => 'auth', 'uses' => 'ManagementController@saveGroup']);
    Route::get('management/groups/{group}/{permission}/{action}', ['middleware' => 'auth', 'uses' => 'ManagementController@updateGroupPermission']);

    Route::get('management/plugins', ['middleware' => 'auth', 'uses' => 'ManagementController@plugins']);
    Route::get('management/plugins/{id}/{slug}', ['middleware' => 'auth', 'uses' => 'ManagementController@plugin']);
    Route::post('management/plugins/{id}/{slug}/save', ['middleware' => 'auth', 'uses' => 'ManagementController@pluginSave']);

    Route::get('management/files', ['middleware' => 'auth', 'uses' => 'ManagementController@files']);
    Route::match(['get', 'post'], 'management/files/{id}/{slug}/edit', ['middleware' => 'auth', 'uses' => 'ManagementController@fileEdit']);

    Route::get('management/organizations', ['middleware' => 'auth', 'uses' => 'ManagementController@organization']);
    Route::get('management/organizations/{organization}/members', ['middleware' => 'auth', 'uses' => 'ManagementController@organizationsMembers']);
    Route::match(['get', 'post'], 'management/organizations/{organization}/members/{id}/{action}', ['middleware' => 'auth', 'uses' => 'ManagementController@organizationsActionMember']);

    Route::get('management/questions', ['middleware' => 'auth', 'uses' => 'ManagementController@questions']);
    Route::get('management/questions/delete/{id}', ['middleware' => 'auth', 'uses' => 'ManagementController@deleteQuestion']);
    Route::match(['get', 'post'], 'management/questions/edit/{id}', ['middleware' => 'auth', 'uses' => 'ManagementController@editQuestion']);

    Route::get('management/messages', ['middleware' => 'auth', 'uses' => 'ManagementController@messages']);
    Route::get('management/messages/{id}', ['middleware' => 'auth', 'uses' => 'ManagementController@messageShow']);

    Route::get('management/suggestions', ['middleware' => 'auth', 'uses' => 'ManagementController@suggestions']);
    Route::get('management/suggestions/delete/{id}', ['middleware' => 'auth', 'uses' => 'ManagementController@suggestionDelete']);

    Route::get('management/logs', ['middleware' => 'auth', 'uses' => 'ManagementController@logger']);
    Route::get('management/logs/view/{id}', ['middleware' => 'auth', 'uses' => 'ManagementController@loggerShow']);

    Route::get('management/invoices', ['middleware' => 'auth', 'uses' => 'ManagementController@invoices']);
});
