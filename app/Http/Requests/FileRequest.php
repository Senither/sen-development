<?php

namespace App\Http\Requests;

class FileRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fileUpload' => 'required',
            'plugin_id' => 'required',
            'organization_id' => 'required',
            'version' => 'required|min:3',
            'changes' => 'required|min:16',
        ];
    }
}
