<?php

namespace App\Http;

class Payment
{
    /**
     * Determens if the payment gateway should go to
     * PayPals sandbox API or the public API.
     *
     * @var bool
     */
    protected $sandbox = false;

    /**
     * Reprecent the payments to generate.
     *
     * @var PaymentItem Array
     */
    protected $items = [];

    /**
     * The email what the payment should be sent to.
     *
     * @var string
     */
    protected $business = null;

    /**
     * The type of currency the payment should be paid in.
     *
     * @var string
     */
    protected $currency = 'USD';

    /**
     * The submit button image that should be used by the form.
     *
     * @var string
     */
    protected $image = 'http://www.paypal.com/en_US/i/btn/x-click-but01.gif';

    protected $urls = [
        'return' => null,
        'cancel_return' => null,
    ];

    /**
     * Some different paypal options you can overwrite.
     *
     * @var array
     */
    protected $options = [
        'invoice' => null,
        'custom' => null,
        'image_url' => null,
        'cn' => 'This is payment to SenDevelopment',
    ];

    public function __construct($business = null, $currency = null)
    {
        $this->sandbox = config('payment.sandbox', false);
        $this->business = ($business == null) ?: $business;
        $this->currency = ($currency == null) ?: $currency;

        $url = config('payment.url_base');

        foreach ($this->urls as $name => $nullValue) {
            $this->urls[$name] = $url;
        }
    }

    /**
     * Sets a new value for an option key.
     *
     * @param string $key
     * @param mixed  $value
     */
    public function setOption($key, $value)
    {
        if (array_key_exists($key, $this->options)) {
            if (is_array($value)) {
                $value = urlencode(http_build_query($value));
            }
            $this->options[$key] = $value;
        }
    }

    /**
     * Gets the option value for the given key.
     *
     * @param string $key
     *
     * @return mixed
     */
    public function getOption($key)
    {
        if (isset($this->options[$key])) {
            return $this->options[$key];
        }

        return;
    }

    public function setReturnUrl($path)
    {
        $this->urls['return'] .= $path;
    }

    public function getReturnUrl()
    {
        return $this->urls['return'];
    }

    public function setCancelReturnUrl($path)
    {
        $this->urls['cancel_return'] .= $path;
    }

    public function getCancelReturnUrl()
    {
        return $this->urls['cancel_return'];
    }

    /**
     * Sets a new button image for the form.
     *
     * @param string $image
     */
    public function setButtonImage($image)
    {
        $this->image = $image;
    }

    /**
     * Adds a new item to the items cache, allowing the
     * class to build the item into a form input later.
     *
     * @param string $name
     * @param int    $quantity
     * @param float  $cost
     */
    public function addItem($name, $quantity, $cost)
    {
        $this->items[] = new PaymentItem($name, $quantity, $cost);
    }

    /**
     * Generate the HTML form for the request with the given options.
     *
     * @return string
     */
    public function render()
    {
        return $this->buildForm(ture, function ($payment, $index) {
            $item = [
                'name' => '<input type="hidden" name="item_name_'.$index.'" value="'.$payment->getName().'">',
                'cost' => '<input type="hidden" name="amount_'.$index.'" value="'.$payment->getCost().'">',
                'quantity' => '<input type="hidden" name="quantity_'.$index.'" value="'.$payment->getQuantity().'">',
            ];

            return $item;
        });
    }

    public function renderArray()
    {
        return $this->buildForm(false, function ($payment, $index) {
            $item = [
                'name' => '<input type="hidden" name="item_name_'.$index.'" value="'.$payment->getName().'">',
                'cost' => '<input type="hidden" name="amount_'.$index.'" value="'.$payment->getCost().'">',
                'quantity' => '<input type="hidden" name="quantity_'.$index.'" value="'.$payment->getQuantity().'">',
            ];

            return $item;
        });
    }

    public function toArray()
    {
        $array = [];

        foreach ($this->items as $index => $item) {
            $array[$index] = $item->toArray();
        }

        return $array;
    }

    public function toJson()
    {
        return json_encode($this->toArray());
    }

    public function __toString()
    {
        return $this->toJson();
    }

    /**
     * Generate the overview form data and all of the options that has been set.
     *
     * @param Closure $clouser
     *
     * @return string
     */
    protected function buildForm($implode, \Closure $clouser)
    {
        $html[] = '<form action="https://www'.($this->sandbox ? '.sandbox' : null).'.paypal.com/cgi-bin/webscr" method="post" id="paypal_invoice_form">';

        $html[] = '<input type="hidden" name="cmd" value="_cart">';
        $html[] = '<input type="hidden" name="upload" value="1">';
        $html[] = '<input type="hidden" name="business" value="'.$this->business.'">';

        // Builts the Options
        foreach ($this->options as $name => $value) {
            if ($value == null) {
                continue;
            }

            $html[] = '<input type="hidden" name="'.$name.'" value="'.$value.'">';
        }

        // Builts the URLS
        foreach ($this->urls as $name => $value) {
            if ($value == null) {
                continue;
            }

            $html[] = '<input type="hidden" name="'.$name.'" value="'.$value.'">';
        }

        // Builts the items
        foreach ($this->items as $index => $item) {
            $item = $clouser($item, $index + 1);
            $html[] = implode("\r\n", $item);
        }

        $html[] = '<input type="image" src="'.$this->image.'" name="submit" alt="Make payments with PayPal - it\'s fast, free and secure!">';
        $html[] = '</form>';

        return ($implode) ? implode("\r\n", $html) : $html;
    }
}

class PaymentItem
{
    private $name;
    private $quantity;
    private $cost;

    public function __construct($name, $quantity, $cost)
    {
        $this->name = $name;
        $this->quantity = $quantity;
        $this->cost = $cost;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getQuantity()
    {
        return $this->quantity;
    }

    public function getCost()
    {
        return $this->cost;
    }

    public function getSubtotal()
    {
        return $this->getCost() * $this->getQuantity();
    }

    public function toArray()
    {
        return [
            'name' => $this->getName(),
            'quantity' => $this->getQuantity(),
            'cost' => $this->getCost(),
            'subtotal' => $this->getSubtotal(),
        ];
    }
}
