$( document ).ready(function() {

    // Slimscroll
    $('.slimscroll').slimscroll({
        allowPageScroll: true
    });

    // Wow
    new WOW().init();

    // Smooth scroll
    $('a[href^="#"]').on('click',function (e) {
        e.preventDefault();

        var target = this.hash;
        var $target = $(target);
        var scrollTo = $target.offset().top

        $('html, body').stop().animate({
            'scrollTop': scrollTo
        }, 1000, 'easeInOutExpo');
    });

    $(window).scroll(function() {
        var scroll = $(window).scrollTop();

        if (scroll >= 60) {
            $(".navbar").addClass("whiteHeader");
        } else {
            $(".navbar").removeClass("whiteHeader");
        }
    });

    // Tabs
    [].slice.call( document.querySelectorAll( '.tabs' ) ).forEach( function( el ) {
        new CBPFWTabs( el );
    });

    // Form action
    var $button = $('#form-submit');
    var $canRun = true;

    $('#form-submit').on('click', function () {
        if (!$canRun) {
            return;
        }
        $button.addClass('disabled');
        $button.html('<i class="fa fa-spinner fa-spin"></i> Saving..');

        var formData = {
            name: $('input#name').val(),
            email: $('input#email').val(),
            subject: $('input#subject').val(),
            message: $('textarea#message').val()
        };

        $.ajax({
            type: 'POST',
            url:  '/save',
            data: formData,
            success: function (data) {
                $('#form-container').animate({
                    width: "toggle",
                    height: "toggle",
                }, {
                    duration: 1200,
                    specialEasing: {
                    width: "linear",
                    height: "easeOutBounce"
                }, complete: function() {
                    $( this ).after( "<div id='toggle-after-form-submit'><h2 class='text-center'>Thank you for your email!</h2>" +
                                     "<p class='text-center'>We will try and get back to you as soon as possible ;)</p></div>");
                    $('#toggle-after-form-submit').hide().toggle('fold');
                }});

                $canRun = false;
            },
            error: function (data) {
                $button.removeClass('disabled');
                $button.html('Send Message');

                var formErrorsField = $('#form-errors');
                formErrorsField.html('<ul class="alert alert-warning">');

                $.each(data.responseJSON, function (object, messages) {
                    $.each(messages, function (index, message) {
                        $('<li>' + message + '</li>').appendTo('ul.alert');
                    });
                });

                formErrorsField.hide().fadeIn(1500);
            }
        });
    });
});
