SenDevelopment To-Do List
=========================

The following items is a list of items that needs to be finished by the end of the deadline for Sen Development!
Link: [sen-dev.com](http://sen-dev.com/)

---

### Dashboard
 * ~~Finish Server Stats~~
 * Add a news feed(Sort of like a blog)
 * Add a table of plugins that the logged in user has access to

### Plugins
>There is currently no things that needs to be finished here

### Organizations
>There is currently no things that needs to be finished here

### My Settings
 * Allow the user to see their own invoices somehow
    * Make a **Pay Now** button that is linked to paypal if the status of the invoice is *Pending*
 * Make opt-in email settings false by default

### Support
##### FAQ
>There is currently no things that needs to be finished here

##### Suggestions
>There is currently no things that needs to be finished here

##### Contact
>There is currently no things that needs to be finished here

### Development Tools
 * ~~Add new permission checks for every page so that normal users can't see theses pages.~~

##### Plugins
 * ~~Allowing users to edit already uploaded files~~
 * ~~Add a edit plugin page.~~
 * ~~Add secureity checks~~

##### Organizations
 * ~~Make the organizations page~~
 * ~~Add a **Create new Organization** button with page functionality~~
 * ~~Make a edit page functionallity, allowing the user to~~
    * ~~Add and remove members, make a confirm page to make sure the user should be removed before carring out the action.~~
 * ~~Make a **Create new plugin** button with page functionality~~
 * ~~Add a owner ID so that there will always be an user in the organization.~~

##### Invoices
 * Make the invoices page
 * Allow developers to make, and send out an invoice
 * Allow developers to see their own invoies and their status

### Management
 * Add new permission checks for every page so that normal users or developers can be given access to some pages, but not others
    * Users
    * Groups
    * Plugins
    * Files
    * Organizations
    * Questions
    * Invoices
    * Logs

##### Users
>There is currently no things that needs to be finished here

##### Groups
>There is currently no things that needs to be finished here

##### Plugins
 * ~~Add a edit plugins functionality~~
 * ~~Add a padinator at the bottom of the page~~

##### Files
 * ~~Add a edit files functionality~~
 * ~~Add a padinator at the bottom of the page~~

##### Organizations
 * ~~Add a edit functionality so that admins can edit organizations~~

##### Questions
>There is currently no things that needs to be finished here

##### Invoices
 * Make the invoices page
 * Allow admins to see all the invoices, this needs a status of some sort to see if the invoice has been paid for or not

##### Logs
 * ~~Make the log page~~
 * ~~Allow admins to view the page, and no one else~~
 * ~~Make a log state, eg: **USER_FILE_EDIT** for when a user edits a file~~
 * ~~Make a before and after view so you can see what has been changed~~
