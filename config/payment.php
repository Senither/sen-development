<?php

/**
 * The configuration fot the Payment for PayPal, created by Alexis Tan.
 *
 * @version  1.0
 */
return [

    /*
    |--------------------------------------------------------------------------
    | Sandbox Mode
    |--------------------------------------------------------------------------
    |
    | Sets the API requests to sandbox mode if it is set to true, you should
    | only enable this while testing, otherwise the payments won't be sent
    | to your paypal email at the end of the transaction.
    |
    */
    'sandbox' => true,

    /*
    |--------------------------------------------------------------------------
    | URL Base Path
    |--------------------------------------------------------------------------
    |
    | The base path for the return and cancel return urls sent to PayPal.
    |
    */
    'url_base' => 'http://sen-dev.dev/',
];
